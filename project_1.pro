TEMPLATE = subdirs
SUBDIRS += libs/utils
SUBDIRS += libs/dbutils
SUBDIRS += libs/fplot
SUBDIRS += libs/okexapi
SUBDIRS += libs/telegrambot
SUBDIRS += libs/series
SUBDIRS += libs/seriesgui

SUBDIRS += apps/project_1

CODECFORTR  = UTF-8
CODECFORSRC = UTF-8
CONFIG+= ordered
CONFIG+= warn_on
