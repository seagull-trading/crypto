QT       += core network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib
DEFINES+= TelegramBot_LIBRARY
TARGET = $$qtLibraryTarget(telegrambot)

DESTDIR = ../../output
LIBS += -L../../output

CONFIG(debug,debug|release) {
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}

HEADERS += \
    networking.h \
    qttelegrambot.h \
    telegrambot_global.h   \
    types/audio.h \
    types/callbackquery.h \
    types/chat.h \
    types/contact.h \
    types/document.h \
    types/file.h \
    types/location.h \
    types/message.h \
    types/photosize.h \
    types/reply/forcereply.h \
    types/reply/genericreply.h \
    types/reply/inlinekeyboardmarkup.h \
    types/reply/replykeyboardhide.h \
    types/reply/replykeyboardmarkup.h \
    types/reply/replykeyboardremove.h \
    types/sticker.h \
    types/update.h \
    types/user.h \
    types/video.h \
    types/voice.h

SOURCES += \
    networking.cpp \
    qttelegrambot.cpp \
    types/audio.cpp \
    types/callbackquery.cpp \
    types/chat.cpp \
    types/contact.cpp \
    types/document.cpp \
    types/location.cpp \
    types/message.cpp \
    types/photosize.cpp \
    types/sticker.cpp \
    types/update.cpp \
    types/user.cpp \
    types/video.cpp \
    types/voice.cpp
