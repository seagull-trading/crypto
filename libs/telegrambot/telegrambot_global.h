#ifndef TELEGRAMBOT_GLOBAL_H
#define TELEGRAMBOT_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(TelegramBot_LIBRARY)
#  define TelegramBot_EXPORT Q_DECL_EXPORT
#else
#  define TelegramBot_EXPORT Q_DECL_IMPORT
#endif
#endif
