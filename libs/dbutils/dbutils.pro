QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$qtLibraryTarget(dbutils)

TEMPLATE = lib
DEFINES+= DBUtils_LIBRARY

DESTDIR = ../../output
LIBS += -L../../output
INCLUDEPATH += ../../libs/utils
INCLUDEPATH += ../../libs/series

CONFIG(debug,debug|release) {
    LIBS += -lutilsd -lseriesd
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    LIBS += -lutils -lseries
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}

HEADERS += \
    dbutils.h \
    dbutils_global.h 

SOURCES += \
    dbutils.cpp


