#include <QDebug>
#include <QSqlError>
#include "candlesloader.h"
#include "properties.h"
#include "dbutils.h"
#include "series.h"

bool ConnectToDatabase(QSqlDatabase& db, QString host, int port, QString dbname, QString usr, QString pwd)
{
    db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(host);
    db.setPort(port);
    db.setDatabaseName(dbname);
    db.setUserName(usr);
    db.setPassword(pwd);
    bool ok = db.open();
    if(!ok) qDebug() << Q_FUNC_INFO
                     << "Connection error:"
                     <<  db.lastError().text();
    return ok;
}

bool OpenDatabase(QSqlDatabase& db)
{
    int port;
    QString host, dbname, usr, pwd;
    Properties p("settings/database.ini");
    p.load();
    host   = p.get("host").toString();
    port   = p.get("port").toInt();
    dbname = p.get("name").toString();
    usr    = p.get("usr").toString();
    pwd    = p.getPassword("pwd");
    return ConnectToDatabase(db, host,port,dbname,usr,pwd);
}

void CreateAllCryptoBinaries()
{
    QSqlDatabase dataBase;
    if(!OpenDatabase(dataBase)) return;
    CandlesLoader loader(dataBase);
    loader.load("eth","usdt");
    loader.load("eth","btc");
    loader.load("btc","usdt");
    loader.load("btc","usdc");
    loader.load("ltc","usdt");
    loader.load("etc","usdt");    
    loader.load("eos","usdt");
    loader.load("ada","usdt");
    loader.load("xrp","usdt");
    loader.load("trx","usdt");
}
