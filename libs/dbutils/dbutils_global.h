#ifndef DBUTILS_GLOBAL_H
#define DBUTILS_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(DBUtils_LIBRARY)
#  define DBUtils_EXPORT Q_DECL_EXPORT
#else
#  define DBUtils_EXPORT Q_DECL_IMPORT
#endif
#endif
