#ifndef DBUTILS_H
#define DBUTILS_H
#include <QSqlDatabase>
#include "dbutils_global.h"
bool DBUtils_EXPORT OpenDatabase(QSqlDatabase& db);


// в папке series создать все бинарные файлы загрузив их из БД
// (table_names = "eth_usdt_1m", "xrp_usdt_1d", ...)
void DBUtils_EXPORT CreateCryptoBinaries(QStringList table_names);
void DBUtils_EXPORT CreateAllCryptoBinaries();

#endif // DBUTILS_H
