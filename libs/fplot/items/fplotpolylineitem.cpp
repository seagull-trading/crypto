#include "finplotwidget.h"
#include "fplotpolylineitem.h"

FPlotPolyLineItem::FPlotPolyLineItem(QObject* parent)
    : FPlotItem(parent)
{
}

void FPlotPolyLineItem::draw(FinPlotWidget* w, QPainter* p)
{
    p->setPen(mPen);
    for(int i = 1; i < mPoints.count(); i++)
    {
        QPointF p1 = w->coordToPixel(mPoints[i-1]);
        QPointF p2 = w->coordToPixel(mPoints[i]);
        p->drawLine(p1,p2);
    }
}

void FPlotPolyLineItem::setPen(QPen p)
{
    mPen = p;
}

void FPlotPolyLineItem::addPoint(QPointF p)
{
    addPoint(p.x(), p.y());
}

void FPlotPolyLineItem::addPoint(double x, double y)
{
    mPoints << QPointF(x,y);
}

void FPlotPolyLineItem::addRect(double xx1, double yy1, double xx2, double yy2)
{
    double x1 = qMin(xx1,xx2), x2 = qMax(xx1,xx2);
    double y1 = qMin(yy1,yy2), y2 = qMax(yy1,yy2);
    addPoint(x1,y1);
    addPoint(x2,y1);
    addPoint(x2,y2);
    addPoint(x1,y2);
    addPoint(x1,y1);
}

QPen FPlotPolyLineItem::pen()
{
    return mPen;
}
