#ifndef FPLOTITEM_H
#define FPLOTITEM_H

#include <QObject>
#include <QPainter>
#include "fplot_global.h"

class FinPlotWidget;
class FPLOT_EXPORT FPlotItem : public QObject
{
    Q_OBJECT
public:
    explicit FPlotItem(QObject *parent = nullptr);
    virtual void draw(FinPlotWidget* w, QPainter*) = 0;
    bool visible();
    void setVisible(bool);
private:
    bool mVisible = true;

};

#endif // FPLOTITEM_H
