#include <QDebug>
#include "finplotwidget.h"
#include "fplotlineitem.h"

FPlotLineItem::FPlotLineItem(QObject* parent)
    : FPlotItem(parent)
{
}

void FPlotLineItem::setPen(QPen p)
{
    mPen = p;
}

void FPlotLineItem::setLine(QLineF l)
{
    mLine = l;
}

void FPlotLineItem::setLine(double x1, double y1, double x2, double y2)
{
    mLine = QLineF(x1,y1,x2,y2);
}

void FPlotLineItem::setLine(QPointF p1, QPointF p2)
{
    mLine = QLineF(p1.x(),p1.y(),p2.x(),p2.y());
}

QPen FPlotLineItem::pen()
{
    return mPen;
}

void FPlotLineItem::draw(FinPlotWidget* w, QPainter * p)
{
    p->setPen(mPen);
    QPointF p1 = w->coordToPixel(mLine.p1());
    QPointF p2 = w->coordToPixel(mLine.p2());
    p->drawLine(p1,p2);
}
