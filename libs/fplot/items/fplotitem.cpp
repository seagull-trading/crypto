#include "fplotitem.h"

FPlotItem::FPlotItem(QObject *parent) : QObject(parent)
{

}

bool FPlotItem::visible()
{
    return mVisible;
}

void FPlotItem::setVisible(bool v)
{
    mVisible = v;
}
