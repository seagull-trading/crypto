#ifndef FPLOTLINEITEM_H
#define FPLOTLINEITEM_H

#include "fplotitem.h"

class FPLOT_EXPORT FPlotLineItem : public FPlotItem
{
public:
    FPlotLineItem(QObject* parent = nullptr);
    void draw(FinPlotWidget*, QPainter*) override;
    void setPen(QPen);
    void setLine(QLineF);
    void setLine(double x1, double y1, double x2, double y2);
    void setLine(QPointF p1, QPointF p2);
    QPen pen();
private:
    QPen mPen = QPen(Qt::black);
    QLineF mLine;

};

#endif // FPLOTLINEITEM_H
