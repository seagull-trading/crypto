#ifndef FPLOTPOLYLINEITEM_H
#define FPLOTPOLYLINEITEM_H


#include "fplotitem.h"

class FPLOT_EXPORT FPlotPolyLineItem : public FPlotItem
{
public:
    FPlotPolyLineItem(QObject* parent = nullptr);
    void draw(FinPlotWidget*, QPainter*) override;
    void setPen(QPen);
    void addPoint(QPointF);
    void addPoint(double x, double y);
    void addRect(double x1, double y1, double x2, double y2);

    QPen pen();
private:
    QPen mPen = QPen(Qt::black);
    QVector<QPointF> mPoints;

};
#endif // FPLOTPOLYLINEITEM_H
