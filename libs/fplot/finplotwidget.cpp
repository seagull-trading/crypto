#include <QDebug>
#include "utils.h"
#include "fplotitem.h"
#include "finplotwidget.h"
#include "properties.h"

FinPlotWidget::FinPlotWidget(QWidget *parent) :
    QCustomPlot(parent)
{
    props = new Properties();
    setFocusPolicy(Qt::ClickFocus); // чтобы работали keyPressEvent
    rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
    setInteraction(QCP::iRangeDrag, true);
    setInteraction(QCP::iRangeZoom, true);
    setInteraction(QCP::iSelectPlottables, true);
    setInteraction(QCP::iSelectItems, true);
    setInteraction(QCP::iMultiSelect, false);
    setLocale(QLocale(QLocale::Russian, QLocale::Russia));
    setDateAxis(true);
    xAxis->setVisible(true);
    yAxis2->setVisible(true);
    yAxis->setVisible(false);
    connect(yAxis, SIGNAL(rangeChanged(QCPRange)), yAxis2, SLOT(setRange(QCPRange)));
}


void FinPlotWidget::setDateAxis(bool d)
{
    if(d)
    {
        QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
        dateTicker->setDateTimeSpec(Qt::UTC);
        dateTicker->setDateTimeFormat("hh:mm\ndd.MM.yyyy");
        xAxis->setTicker(dateTicker);
        IsDateAxis = true;
        drawDayNight = true;
    }else{
        QSharedPointer<QCPAxisTicker> numTicker(new QCPAxisTicker);
        numTicker->setTickCount(10);
        xAxis->setTicker(numTicker);
        IsDateAxis = false;
    }
}

bool FinPlotWidget::isDateAxis()
{
    return IsDateAxis;
}

void FinPlotWidget::showMonthsInfo(bool b)
{
    mShowMonthsInfo = b;
}

void FinPlotWidget::clearUserItems()
{
    mUserItems.clear();
    repaint();
}

void FinPlotWidget::addUserItem(FPlotItem* it)
{
    if(mUserItems.contains(it)) return;
    mUserItems << it;
    repaint();
}

void FinPlotWidget::removeUserItem(FPlotItem* it)
{
    if(!mUserItems.contains(it)) return;
    mUserItems.removeAll(it);
    repaint();
}

int FinPlotWidget::daySectionHeight()
{
    return QFontMetrics(daySectionFont).height();
}

void FinPlotWidget::init(QString path)
{
    props->setFileName(path+"/finwidget.ini");
    props->load();
}

void FinPlotWidget::reservGraphs(int cnt)
{
    while(cnt >= graphCount()) addGraph();
}

FinPlotWidget::~FinPlotWidget()
{
    props->set("closed", QDateTime::currentDateTime());
    props->save();
    delete rubberBand;
    delete props;
}

void FinPlotWidget::mousePressEvent(QMouseEvent *e)
{
    mouseType = fmtDefault;
    rubberPos = e->pos();
    if(e->button() == Qt::LeftButton)
    {
        if(QApplication::keyboardModifiers().testFlag(Qt::ControlModifier))
        {
            startPos = e->pos();
            mouseType = fmtLine;
        }
    }else if(e->button() == Qt::RightButton)
    {
        rubberBand->setGeometry(QRect(rubberPos, QSize()));
        rubberBand->show();
    }
    QCustomPlot::mousePressEvent(e);
}

QString FinPlotWidget::defaultCursorInfo()
{
    if(isDateAxis())
    {
        QDateTime t = QDateTime::fromTime_t((int)xAxis->pixelToCoord(currentPos.x()),Qt::UTC);
        double y = yAxis2->pixelToCoord(currentPos.y());
        return t.toString("dd/MM/yyyy hh:mm") + "\n" + QString::number(y,'f',9);
    }else{
        double x = xAxis->pixelToCoord(currentPos.x());
        double y = yAxis2->pixelToCoord(currentPos.y());
        return "x=" + QString::number(x,'f') + "\ny="  + QString::number(y,'f',9);
    }
}

void FinPlotWidget::mouseMoveEvent(QMouseEvent *e)
{
    currentPos = e->pos();
    setCursorInfo(defaultCursorInfo());
    rubberBand->setGeometry(QRect(rubberPos, e->pos()).normalized());
    repaint();
    if(mouseType == fmtDefault)
        QCustomPlot::mouseMoveEvent(e);
}

void FinPlotWidget::mouseReleaseEvent(QMouseEvent* e)
{
    if(rubberBand->isVisible())
    {
        if(e->pos().x() < rubberPos.x() || e->pos().y() < rubberPos.y())
        {
            myRescaleAxes();
        }else{
            QRect zoomRect = rubberBand->geometry();
            int xp1,yp1,xp2,yp2;
            zoomRect.getCoords(&xp1,&yp1,&xp2,&yp2);
            double x1 = xAxis->pixelToCoord(xp1);
            double x2 = xAxis->pixelToCoord(xp2);
            xAxis->setRange(x1,x2);
            yAxis2->setRange(yAxis->pixelToCoord(yp1),yAxis->pixelToCoord(yp2));
            yAxis->setRange(yAxis->pixelToCoord(yp1),yAxis->pixelToCoord(yp2));
        }
        rubberBand->hide();
        replot();
    }
    if(mouseType == fmtLine)
    {
        emit onAddLine(pixelToCoord(startPos), pixelToCoord(e->pos()));
    }
    mouseType = fmtDefault;
    QCustomPlot::mouseReleaseEvent(e);
}

void FinPlotWidget::keyReleaseEvent(QKeyEvent* e)
{
    if(e->key() == Qt::Key_Delete)
    {
        QList<QCPAbstractItem*> tmp = selectedItems();
        for(int i = 0; i < tmp.count(); i++)
            removeItem(tmp[i]);
        replot();
    }
    QCustomPlot::keyReleaseEvent(e);
}

void FinPlotWidget::paintEvent(QPaintEvent *e)
{
    QCustomPlot::paintEvent(e);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    if(QApplication::keyboardModifiers().testFlag(Qt::ControlModifier))
    {
        p.setPen(props->getPen("cursorLinePen", QPen(Qt::black)));
        p.drawLine(currentPos.x(),0,currentPos.x(),rect().height());
        p.drawLine(0,currentPos.y(),rect().width(),currentPos.y());
        p.setPen(props->getPen("cursorTextPen", QPen(Qt::black)));
        p.setFont(props->getFont("cursorFont", QFont("Consolas",10)));
        QStringList tmp = cursorInfo.split("\n");
        int h = p.fontMetrics().height();
        for(int i = 0; i < tmp.count(); i++)
            p.drawText(currentPos + QPoint(5, h*(i - tmp.count() + 1) - 5), tmp[i]);
    }
    if(mouseType == fmtLine)
    {
        p.setPen(props->getPen("supportLinePen",QPen(Qt::darkMagenta)));
        p.drawLine(startPos, currentPos);
        p.drawLine(startPos - QPoint(10,0), startPos + QPoint(10,0));
        p.drawLine(startPos - QPoint(0,10), startPos + QPoint(0,10));
        p.drawLine(currentPos - QPoint(10,0), currentPos + QPoint(10,0));
        p.drawLine(currentPos - QPoint(0,10), currentPos + QPoint(0,10));
        double dy = yAxis->pixelToCoord(currentPos.y()) - yAxis->pixelToCoord(startPos.y());
        double bid = qMin(yAxis->pixelToCoord(startPos.y()),yAxis->pixelToCoord(currentPos.y()));
        double ask = qMax(yAxis->pixelToCoord(startPos.y()),yAxis->pixelToCoord(currentPos.y()));
        double delta = ask - bid;
        p.drawText(currentPos + QPointF(32,p.fontMetrics().height()),
                   tr("Δy=%1 %2 %3").arg(QString::number(dy,'f',9))
                   .arg(myRound(delta*100/ask,2)).arg("%"));
    }
    if(drawDayNight)
        drawingDayNight(&p);
    if(mShowMonthsInfo)
        drawMonthInfo(&p);

    drawUserItems(&p);

    emit onDraw(&p);
}

QString FinPlotWidget::myRound(double d, int aftcomma)
{
    QString str = QString::number(d,'f');
    int ind = str.indexOf(".");
    if(ind == -1) return str;
    return str.mid(0,ind+aftcomma+1);
}

QColor FinPlotWidget::monthColorMix(QColor c, QDateTime)
{
    QColor color = c;
    return color;
}

void FinPlotWidget::wheelEvent(QWheelEvent* e)
{
    if(e->modifiers().testFlag(Qt::ControlModifier))
    {
        QCPAxis* a = xAxis;
        double wheelSteps = e->delta()/120.0;
        double factor = qPow(a->axisRect()->rangeZoomFactor(a->orientation()), wheelSteps);
        a->scaleRange(factor, a->pixelToCoord(e->pos().x()));
        replot();
    }else if(e->modifiers().testFlag(Qt::AltModifier))
    {
        QCPAxis* a = yAxis;
        double wheelSteps = e->delta()/120.0;
        double factor = qPow(a->axisRect()->rangeZoomFactor(a->orientation()), wheelSteps);
        a->scaleRange(factor, a->pixelToCoord(e->pos().y()));
        replot();
    }else QCustomPlot::wheelEvent(e);
}


void FinPlotWidget::setPen(int index, QPen pen, QPen selPen)
{
    if(selPen == QPen()) selPen = pen;
    reservGraphs(index);
    graph(index)->setPen(pen);
    //graph(index)->setSelectedPen(selPen);
    auto decor = new QCPSelectionDecorator();
    decor->setPen(selPen);
    graph(index)->setSelectionDecorator(decor);

}

void FinPlotWidget::setCursorInfo(QString str)
{
    cursorInfo = str;
}

QPointF FinPlotWidget::pixelToCoord(QPoint p)
{
    return QPointF(xAxis->pixelToCoord(p.x()), yAxis2->pixelToCoord(p.y()));
}

double FinPlotWidget::xToCoord(double x_pixel)
{
    return xAxis->pixelToCoord(x_pixel);
}

double FinPlotWidget::xToPixel(double x_value)
{
    return xAxis->coordToPixel(x_value);
}

QPoint FinPlotWidget::coordToPixel(QPointF p)
{
    return QPoint(xAxis->coordToPixel(p.x()),yAxis2->coordToPixel(p.y()));
}

double FinPlotWidget::yToCoord(double x_pixel)
{
    return yAxis2->pixelToCoord(x_pixel);
}

double FinPlotWidget::yToPixel(double x_value)
{
    return yAxis2->coordToPixel(x_value);
}

QRectF FinPlotWidget::visibleRect()
{
    return QRectF(QPointF(xAxis->range().lower,yAxis2->range().lower),
                  QPointF(xAxis->range().upper,yAxis2->range().upper));
}

QRect FinPlotWidget::visibleRectPixel()
{
    return QRectF(coordToPixel(QPointF(xAxis->range().lower,yAxis2->range().lower)),
                  coordToPixel(QPointF(xAxis->range().upper,yAxis2->range().upper))).toRect();
}

void FinPlotWidget::myRescaleAxes()
{
    rescaleAxes(true);
    QCPRange ry = yAxis2->range();
    double dy = ry.upper - ry.lower;
    ry.upper+=dy;
    ry.lower-=dy;
    yAxis2->setRange(ry);
    yAxis->setRange(ry);
}

void FinPlotWidget::setMargin(double left, double top, double right, double bottom)
{
    marginLeft = left;
    marginTop = top;
    marginRight = right;
    marginBottom = bottom;
}

void FinPlotWidget::addValue(int index, double x, double y)
{
    reservGraphs(index);
    graph(index)->addData(x, y);
}

void FinPlotWidget::setGraphVisible(int index, bool vis)
{
    reservGraphs(index);
    graph(index)->setVisible(vis);
}


void FinPlotWidget::drawingDayNight(QPainter* p)
{
    int alpha = 80;
    double x1 = xAxis->range().lower;
    double x2 = xAxis->range().upper;
    double y1 = yAxis2->range().lower;
    double y2 = yAxis2->range().upper;
    p->setPen(Qt::black);
    p->setBrush(Qt::NoBrush);
    QRect r = QRect(coordToPixel(QPointF(x1,y1)),coordToPixel(QPointF(x2,y2)));
    p->drawRect(r);
    p->setClipRect(r);
    p->setClipping(true);
    p->setPen(Qt::NoPen);
    p->setFont(daySectionFont);
    QDateTime tm_beg, tm_end;
    tm_beg = QDateTime::fromTime_t(x1 - 3600,Qt::UTC);
    tm_beg.setTime(QTime(tm_beg.time().hour(),0,0));
    tm_end = QDateTime::fromTime_t(x2 + 3600,Qt::UTC);
    tm_end.setTime(QTime(tm_end.time().hour(),0,0));
    int tbeg = tm_beg.toTime_t();
    int tend = tm_end.toTime_t();
    int dsh = daySectionHeight();
    int step = 3600 / 2;

    for(int t = tbeg; t < tend; t+= step)
    {
        QDateTime dt = QDateTime::fromTime_t(t,Qt::UTC);
        QTime tt = dt.time();
        double hh = tt.hour() + double(tt.minute())/60;
        double v = (hh < 12) ? (127*hh)/12 : (127 - (127*(hh-12))/12);
        if(v < 0) v = 0;
        if(v > 127) v = 127;
        QColor color = monthColorMix(QColor(127+v,127+v,127+v,alpha), dt);
        p->setBrush(QBrush(color));
        p->drawRect(QRectF(coordToPixel(QPointF(t,y1)), coordToPixel(QPointF(t+step,y2))));
    }
    return;
    int t1 = QDateTime(tm_beg.date().addDays(-1)).toTime_t();
    int t2 = QDateTime(tm_end.date().addDays( 1)).toTime_t();
    int dx = qAbs(coordToPixel(QPointF(t1,y1)).x() - coordToPixel(QPointF(t1+86400,y1)).x());
    //    auto vr = visibleRectPixel();
    int digw = p->fontMetrics().horizontalAdvance("000");
    if(dx > digw)
    {
        p->setBrush(Qt::NoBrush);
        int step = 84600;
        for(int t = t1; t < t2; t+= step)
        {
            p->setPen(Qt::black);
            QRectF r = QRectF(coordToPixel(QPointF(t,y1)),
                              coordToPixel(QPointF(t+step,y1)) - QPointF(0,dsh));
            p->drawRect(r);
            QString str;
            QDateTime tm = QDateTime::fromTime_t(t,Qt::UTC);
            if(r.width() < 2*digw)
            {
                str = tm.toString("dd");
            }else{
                str = tm.toString("dd MMM");
            }
            p->drawText(r,  Qt::AlignCenter, str);
        }
    }else{
        //        p->setBrush(Qt::NoBrush);
        //        QDateTime tt = QDateTime(QDate(tm_beg.date().year(), tm_beg.date().month(), 1), QTime(), Qt::UTC);
        //        tt = tt.addMonths(-1);
        //        while(tt < tm_end)
        //        {
        //            int t1 = tt.toTime_t();
        //            int t2 = tt.addMonths(1).toTime_t();
        //            QRectF r = QRectF(coordToPixel(QPointF(t1,y1)),
        //                              coordToPixel(QPointF(t2,y1)) - QPointF(0,dsh));
        //            p->setPen(Qt::black);
        //            p->drawRect(r);
        //            p->drawText(r,  Qt::AlignCenter, QDateTime::fromTime_t(t1, Qt::UTC).toString("MMMM"));
        //            p->drawLine(r.left(),vr.top(),r.left(),vr.bottom());
        //            tt = tt.addMonths(1);
        //        }
    }
    p->setClipping(false);
}

void FinPlotWidget::drawMonthInfo(QPainter* p)
{
    double x1 = xAxis->range().lower;
    double x2 = xAxis->range().upper;
    double y1 = yAxis2->range().lower;
    double y2 = yAxis2->range().upper;
    p->setPen(Qt::black);
    p->setBrush(Qt::NoBrush);
    QRect r = QRect(coordToPixel(QPointF(x1,y1)),
                    coordToPixel(QPointF(x2,y2)));
    p->setClipRect(r);
    //p->setClipping(true);
    p->setPen(Qt::NoPen);
    p->setFont(monthInfoFont);
    QDateTime d1, d2;
    QDateTime t1 = QDateTime::fromTime_t(x1,Qt::UTC).addDays(-60);
    QDateTime t2 = QDateTime::fromTime_t(x2,Qt::UTC).addDays( 60);
    d1.setDate(QDate(t1.date().year(),t1.date().month(),1));
    d1.setTimeSpec(Qt::UTC);
    d2.setDate(QDate(t2.date().year(),t2.date().month(),1));
    d2.setTimeSpec(Qt::UTC);
    int tbeg = d1.toTime_t();
    int tend = d2.toTime_t();
    if(tbeg < 1 || tend < 1) return;

    int hh = p->fontMetrics().height();
    int t = tbeg;
    int t0 = tbeg;
    QDateTime tm0;
    while(t < tend)
    {
        QDateTime tm = QDateTime::fromTime_t(t,Qt::UTC);
        int len = tm.date().month() == 1 ? 3 : 1;
        p->setPen(QPen(Qt::darkGray,len));
        p->drawLine(coordToPixel(QPointF(t,y1)),coordToPixel(QPointF(t,y2)));

        int yy = tm.date().year();
        int mm = tm.date().month();
        if(mm < 12) mm++; else { mm = 1; yy++; }
        tm.setDate(QDate(yy,mm,1));
        tm.setTime(QTime(0,0));
        t0 = t;
        t = tm.toTime_t();

        QRect rr = QRect(coordToPixel(QPointF(t0,y2)),
                         coordToPixel(QPointF( t,y2)) + QPoint(0,hh));
        bool on_draw = true;
        QString str = tm0.date().toString("MMMM");
        if(rr.width() < p->fontMetrics().horizontalAdvance(str))
        {
            str = tm0.date().toString("MMM");
            if(rr.width() < p->fontMetrics().horizontalAdvance(str))
                on_draw = false;
        }
        if(on_draw)
            p->drawText(rr, Qt::AlignCenter, str);

        tm0 = tm;
    }
    //p->setClipping(false);
}

void FinPlotWidget::drawUserItems(QPainter* p)
{
    for(auto it : mUserItems)
        if(it->visible())
            it->draw(this,p);
}
