#ifndef FINPLOTWIDGET_H
#define FINPLOTWIDGET_H
#include <QList>
#include <QWidget>
#include <QWheelEvent>
#include <QRubberBand>

#include "fplot_global.h"
#include "src/qcustomplot.h"

class Properties;
class FPlotItem;
class FPLOT_EXPORT FinPlotWidget : public QCustomPlot
{
    Q_OBJECT

public:
    enum FinMouseType { fmtDefault,  fmtLine };
    bool drawDayNight = false;
    FinPlotWidget(QWidget *parent = 0);
    void init(QString path);
    void reservGraphs(int cnt);
    void addValue(int index, double x, double y);
    void setGraphVisible(int index, bool vis);
    ~FinPlotWidget();

    void setPen(int index, QPen pen, QPen selPen = QPen());
    void setCursorInfo(QString);

    // преобразование координат
    QPointF pixelToCoord(QPoint);
    double xToCoord(double x_pixel);
    double xToPixel(double x_value);
    QPoint coordToPixel(QPointF);
    double yToCoord(double x_pixel);
    double yToPixel(double x_value);

    // видимое окно в координатах сцены
    QRectF visibleRect();

    // видимое окно в координатах виджета
    QRect visibleRectPixel();

    int daySectionHeight();

    void myRescaleAxes();
    void setMargin(double left, double top, double right, double bottom);
    void setDateAxis(bool);
    bool isDateAxis();
    void showMonthsInfo(bool);

    void clearUserItems();
    void addUserItem(FPlotItem*);    // добавляет в список
    void removeUserItem(FPlotItem*); // удаляет из списка
private:
    QVector<FPlotItem*> mUserItems;

    bool mShowMonthsInfo = true;
    double marginLeft = 0, marginTop = 0, marginRight = 0, marginBottom = 0;
    QFont daySectionFont = QFont("Tahoma", 9);
    QFont monthInfoFont = QFont("Tahoma", 8);
    bool IsDateAxis = true;
    Properties* props;
    FinMouseType mouseType = fmtDefault;
    QString cursorInfo;
    QPoint rubberPos;
    QPoint startPos;
    QPoint currentPos;
    QRubberBand* rubberBand;
    QString defaultCursorInfo();
    void drawingDayNight(QPainter *p);
    QString myRound(double d, int aftcomma);
    QColor monthColorMix(QColor c, QDateTime d);
    void drawMonthInfo(QPainter*);
    void drawUserItems(QPainter*);
signals:
    void onDraw(QPainter*);
    void onAddLine(QPointF p1, QPointF p2);

protected:
    void paintEvent(QPaintEvent*);
    void wheelEvent(QWheelEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent*);
    void keyReleaseEvent(QKeyEvent *);
};

#endif // FINPLOTWIDGET_H
