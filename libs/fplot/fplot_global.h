#ifndef FMAPSCENE_GLOBAL_H
#define FMAPSCENE_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(FPlot_LIBRARY)
#  define FPLOT_EXPORT Q_DECL_EXPORT
#else
#  define FPLOT_EXPORT Q_DECL_IMPORT
#endif
#endif
