QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$qtLibraryTarget(fplot)

TEMPLATE = lib

DEFINES+= FPlot_LIBRARY QCUSTOMPLOT_COMPILE_LIBRARY

DESTDIR=../../output
LIBS += -L../../output


SOURCES += \
    items/fplotitem.cpp \
    items/fplotlineitem.cpp \
    items/fplotpolylineitem.cpp \
    src/qcustomplot.cpp \
    finplotwidget.cpp


HEADERS  += \
    fplot_global.h \
    items/fplotitem.h \
    items/fplotlineitem.h \
    items/fplotpolylineitem.h \
    src/qcustomplot.h \
    finplotwidget.h


INCLUDEPATH += src items
INCLUDEPATH +=../../libs/utils

Debug{
    LIBS += -L../../libs -lutilsd 
    MOC_DIR = ../../tmp/build/MOC_debug/$${TARGET}
    OBJECTS_DIR = ../../tmp/build/Obj_debug/$${TARGET}
}

Release{
    LIBS += -L../../libs -lutils
    MOC_DIR = ../../tmp/build/MOC_release/$${TARGET}
    OBJECTS_DIR = ../../tmp/build/Obj_release/$${TARGET}
}


