#include <QFontDialog>
#include "widgetfont.h"
#include "ui_widgetfont.h"

WidgetFont::WidgetFont(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetFont)
{
    ui->setupUi(this);
    ui->font->home(true);
    setFontu(font());
}

QString WidgetFont::fontName(QFont fnt)
{
    return QString("%1 %2").arg(fnt.family())
            .arg(fnt.pointSize());
}

void WidgetFont::setFontu(QFont fnt)
{
    ui->font->setText(fontName(fnt));
    ui->font->setProperty("font", fnt);
    ui->font->setCursorPosition(0);
}

void WidgetFont::setFontu(QString str)
{
    QFont fnt;
    fnt.fromString(str);
    setFontu(fnt);
}

QFont WidgetFont::getFontu()
{
    return ui->font->property("font").value<QFont>();
}

WidgetFont::~WidgetFont()
{
    delete ui;
}

bool WidgetFont::fontDialog(QFont& fnt)
{
    bool ok;
    QFont new_font = QFontDialog::getFont(&ok, fnt, this);
    if(!ok) return false;
    fnt = new_font;
    return true;
}

void WidgetFont::on_setFont_clicked()
{
    QFont fnt = ui->font->property("font").value<QFont>();
    if(!fontDialog(fnt)) return;
    setFontu(fnt);
    emit changeFont(fnt);
}

