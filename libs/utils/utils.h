#ifndef UTILS_H
#define UTILS_H
#include <QDebug>
#include <QDateTime>
#include <QTextEdit>
#include "utils_global.h"
#define qDeb qDebug() << Q_FUNC_INFO

void Utils_EXPORT ShowMessage(QString str);
void Utils_EXPORT SetLogFilename(QString fn);
void Utils_EXPORT AddLog(QString str);
QString Utils_EXPORT AppDir(QString fn);

int Utils_EXPORT CopyFile(QString srcFileName, QString dstFileName);
bool Utils_EXPORT FileExists(QString fileName);
QDateTime Utils_EXPORT FileCreateDate(QString fileName);

bool Utils_EXPORT DirExists(QString dir);
long Utils_EXPORT DirSize(const QString & str);

void Utils_EXPORT AddStringToFile(QString fileName, QString str);
bool Utils_EXPORT SaveStringToFile(QString fileName, QString str);
bool Utils_EXPORT SaveBuffToFile(QString fileName, char* buf, int sz);
bool Utils_EXPORT LoadStringFromFile(QString fileName, QString& str);

int  Utils_EXPORT SplitString(QString str, QString rsd, QMap<int, QString> & words, bool skipEmpty);
bool Utils_EXPORT checkHHMM(QString hhmm, int tp);
QByteArray Utils_EXPORT LoadArrayFromFile(QString fn);
QString Utils_EXPORT LoadStringFromFile(QString fn);
bool Utils_EXPORT LoadStringListFromFile(QString fn, QStringList& tmp, bool trim);
bool Utils_EXPORT removeDir(const QString &dirName, bool fileOnly = false);
int Utils_EXPORT makeDirectoryList(QString path, QStringList& lst);
int Utils_EXPORT makeFileList(QString path, QStringList& lst);
int Utils_EXPORT makeFileListExt(QString path, QStringList& lst, QString ext);
QString Utils_EXPORT GetFilePath(QString fileName);
QString Utils_EXPORT GetFileName(QString fileName, bool removeExt);
QString Utils_EXPORT GetFileExt(QString fileName);
QString Utils_EXPORT SetFileExt(QString fileName, QString ext);
QString Utils_EXPORT RemoveFileExt(QString fileName);
void Utils_EXPORT setTextEditCharFormat(QTextEdit* textEdit, int start, int len, QTextCharFormat tcf);
void Utils_EXPORT setTextEditUndeline(QTextEdit* textEdit, int start, int len, QColor c, QTextCharFormat::UnderlineStyle style);
QDateTime Utils_EXPORT DateTimeFromString(QString str, QDateTime def = QDateTime());
QString Utils_EXPORT DateTimeToString(QDateTime tm);

#endif // UTILS_H
