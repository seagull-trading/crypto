#ifndef WIDGETCALENDAR_H
#define WIDGETCALENDAR_H

#include <QDate>
#include <QWidget>
#include <QLineEdit>

namespace Ui {
class WidgetCalendar;
}

class WidgetCalendar : public QWidget
{
  Q_OBJECT

public:
  QDate Result;
  explicit WidgetCalendar(QWidget *parent = 0);
  void setMinimumDate(QDate d);
  void setMaximumDate(QDate d);
  void setLineEdit(QLineEdit*);
  void setDate(QDate d);
  ~WidgetCalendar();

  void setFormat(QString frm);
private slots:
  void on_calendarWidget_activated(const QDate &date);

private:
  QString format;
  QLineEdit* lineEdit;
  Ui::WidgetCalendar *ui;
};

#endif // WIDGETCALENDAR_H
