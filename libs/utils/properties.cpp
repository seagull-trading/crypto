﻿#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QByteArray>
#include <QTextCodec>
#include <QColor>
#include <QString>
#include "d3des.h"
#include "properties.h"

#define PasswordAlfavid    QString("1234567890-=_+QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm")

QVariant GetPropertiesFromFile(QString fn, QString grp, QString key, QVariant def)
{
    QVariant val;
    Properties* p = new Properties(fn);
    p->load();
    if(!grp.isEmpty()) p->beginGroup(grp);
    val = p->get(key, def);
    if(!grp.isEmpty()) p->endGroup();
    delete p;
    return val;
}

void SetPropertiesToFile(QString fn, QString grp, QString key, QVariant val)
{
    Properties* p = new Properties(fn);
    if(!grp.isEmpty()) p->beginGroup(grp);
    p->set(key,val);
    if(!grp.isEmpty()) p->endGroup();
    p->save();
    delete p;
}

Properties::Properties()
{
    currGroup = "Main";
}

Properties::Properties(QString fn)
{
    Version = "1.0";
    currGroup = "Main";
    iniFileName = fn;
}

void Properties::setFileName(QString fn)
{
    iniFileName = fn;
}

bool Properties::contains(QString name)
{
    return data[currGroup].contains(name);
}

bool Properties::containsGroup(QString grp)
{
    return data.contains(grp);
}

void Properties::beginGroup(QString grp)
{
    currGroup = grp;
}

void Properties::endGroup()
{
    currGroup = "Main";
}

QVariant Properties::get(QString name, QVariant def)
{
    if(!data[currGroup].contains(name)) return def;
    return data[currGroup][name];
}

QVariant Properties::getv(QString grp, QString name, QVariant def)
{
    if(!data[grp].contains(name)) return def;
    return data[grp][name];
}

QStringList Properties::getGroups()
{
    return data.keys();
}

void Properties::clear(bool rmfile)
{
    data.clear();
    if(rmfile) QFile(iniFileName).remove();
}

void Properties::set(QString name, QVariant val)
{
    data[currGroup][name] = val;
}

void Properties::setv(QString grp, QString name, QVariant val)
{
    data[grp][name] = val;
}

void Properties::setSplitter(QSplitter* s, QString name)
{
    if(name.isEmpty()) name = s->objectName();
    set(name, s->saveState());
}

void Properties::getSplitter(QSplitter* s, QString name)
{
    if(name.isEmpty()) name = s->objectName();
    s->restoreState(get(name, s->saveState()).toByteArray());
}

void Properties::save(bool clr)
{
    QSettings* settings = new QSettings(iniFileName, QSettings::IniFormat);
    settings->setIniCodec(QTextCodec::codecForName("UTF-8"));
    if(clr) settings->clear();
    QList<QString> grp = data.keys();
    for(int i = 0; i < grp.count(); i++)
    {
        settings->beginGroup(grp[i]);
        QList<QString> tmp = data[grp[i]].keys();
        for(int j = 0; j < tmp.count(); j++)
            settings->setValue(tmp[j], data[grp[i]][tmp[j]]);
        settings->endGroup();
    }
    delete settings;
}

int Properties::load()
{
    QSettings settings(iniFileName, QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    QStringList tmp = settings.childGroups();
    int cnt = 0;
    for(int i = 0; i < tmp.count(); i++)
    {
        settings.beginGroup(tmp[i]);
        QStringList keys = settings.childKeys();
        for(int j = 0; j < keys.count(); j++)
            data[tmp[i]][keys[j]] = settings.value(keys[j]);
        settings.endGroup();
        cnt+= keys.count();
    }
    return cnt;
}


// строку например "secret" преобразует в строку с зашифрованным паролем
QString Properties::passwordToString(QString pwd, QString key)
{
    return DesEncode(pwd,key);
}
// строку с зашифрованные паролем преобразует в пароль ("secret")
QString Properties::stringToPassword(QString str, QString key)
{
    return DesDecode(str,key);
}

QString Properties::fileName()
{
    return iniFileName;
}

QList<QPointF> Properties::getPointList(QString name)
{
    QList<QPointF> tmp;
    QList<QVariant> q = get(name).toList();
    for(int i = 0; i < q.count(); i++)
        tmp << q[i].toPointF();
    return tmp;
}

void Properties::setPointList(QString name, QList<QPointF> pnt)
{
    QList<QVariant> q;
    for(int i = 0; i < pnt.count(); i++)
        q << pnt[i];
    set(name, q);
}

// зашифровать и сохранить пароль
void Properties::setPassword(QString name, QString pwd)
{
    set(name,passwordToString(pwd));
}
// зашифровать и сохранить пароль
void Properties::setpwd(QString name, QString pwd, QString key)
{
    set(name,passwordToString(pwd,key));
}
// расшифровать и вернуть пароль
QString Properties::getpwd(QString name, QString key)
{
    return stringToPassword(get(name).toString(),key);
}
// расшифровать и вернуть пароль
QString Properties::getPassword(QString name)
{
    return stringToPassword(get(name).toString());
}

QColor Properties::getColor(QString name, QColor def)
{
    QColor c;
    QString str = get(name,def.name()).toString().trimmed();
    if(str.isEmpty()) return def;
    c.setNamedColor(str);
    return c;
}

QColor Properties::getColorv(QString grp, QString name, QColor def)
{
    QString old = currGroup;
    beginGroup(grp);
    QColor color = getColor(name,def);
    endGroup();
    beginGroup(old);
    return color;
}

void Properties::setColor(QString name, QColor c)
{
    set(name,c.isValid() ? c.name() : "");
}

void Properties::setColorv(QString grp, QString name, QColor c)
{
    setv(grp, name,c.isValid() ? c.name() : "");
}

void Properties::setFont(QString name, QFont fnt)
{
    set(name,fnt.toString());
}

QFont Properties::getFont(QString name, QFont def)
{
    QFont fnt(def);
    QString str = def.toString();
    str = get(name,str).toString();
    if(fnt.fromString(str))
        return fnt;
    return def;
}

void Properties::setBrush(QString name, QBrush b)
{
    set(name, b);
}

void Properties::setPen(QString name, QPen b)
{
    set(name, b);
}

QBrush Properties::getBrush(QString name, QBrush b)
{
    if(!data.contains(name)) return b;
    QVariant val = get(name, val);
    return val.value<QBrush>();
}

QPolygonF Properties::getPolygonF(QString name, QPolygonF def)
{
    return get(name, def).value<QPolygonF>();
}

void Properties::setPolygonF(QString name, QPolygonF p)
{
    set(name, p);
}

QPen Properties::getPen(QString name, QPen b)
{
    if(!data.contains(name)) return b;
    QVariant val = get(name, val);
    return val.value<QPen>();
}

