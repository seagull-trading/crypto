#include "properties.h"
#include "dblogindialog.h"
#include "ui_dblogindialog.h"

DbLoginDialog::DbLoginDialog(QString fn, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DbLoginDialog)
{
    fileName = fn;
    ui->setupUi(this);
    loadProps();
}

DbLoginDialog::~DbLoginDialog()
{
    delete ui;
}

QString DbLoginDialog::userName()
{
    return ui->usr->text();
}

QString DbLoginDialog::password()
{
    return ui->pwd->text();
}

QString DbLoginDialog::host()
{
    QString str = ui->host->text();
    int ind = str.indexOf(":");
    if(ind == -1) return str;
    return str.mid(0,ind);
}

int DbLoginDialog::port(int def)
{
    QRegExp rx("[:]([0-9]+)");
    return ui->host->text().indexOf(rx) == -1 ? def : rx.cap(1).toInt();
}

QString DbLoginDialog::dbName()
{
    return ui->dbname->text();
}

void DbLoginDialog::loadProps()
{
    if(fileName.isEmpty()) return;
    Properties p(fileName);
    p.load();
    p.beginGroup("connection");
    ui->usr->setText(p.get("usr").toString());
    ui->pwd->setText(p.getPassword("pwd"));
    ui->host->setText(p.get("host").toString());
    ui->dbname->setText(p.get("dbname").toString());
    p.endGroup();
}

void DbLoginDialog::saveProps()
{
    if(fileName.isEmpty()) return;
    Properties p(fileName);
    p.beginGroup("connection");
    p.set("usr",ui->usr->text());
    p.setPassword("pwd",ui->pwd->text());
    p.set("host",ui->host->text());
    p.set("dbname",ui->dbname->text());
    p.endGroup();
    p.save();
}

void DbLoginDialog::showText(QColor c, QString str)
{
    QPalette pal = ui->inf->palette();
    pal.setColor(QPalette::WindowText, c);
    ui->inf->setText(str);
    ui->inf->setPalette(pal);
}

void DbLoginDialog::showInfo(QString str)
{
    showText(Qt::black, str);
}

void DbLoginDialog::showError(QString str)
{
    showText(Qt::red, str);
}

void DbLoginDialog::on_start_clicked()
{
    OK = true;
    close();
}

void DbLoginDialog::on_cancel_clicked()
{
    OK = false;
    close();
}
