#ifndef DIALOGMESSAGE_H
#define DIALOGMESSAGE_H

#include <QDialog>
#include <QColor>
#include <QFont>
#include <QTimer>
#include "utils_global.h"
//#include "GUIElements/include/positioneddialog.h"
namespace Ui {
class DialogMessage;
}
void Utils_EXPORT EnableTimeMessages(bool);
void Utils_EXPORT ShowTimeMessage(QString str, int timeout, QSize sz = QSize(320,200), QString title = "", QString sst = "");
void Utils_EXPORT ShowStandartTimeMessage(QString str, int timeout = 1000);

int Utils_EXPORT ShowMessageDialog(int page, QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowOK(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowOK(QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowERR(QString msg, QColor c = Qt::red, QFont f = QFont());
int Utils_EXPORT ShowERROR(QString msg, QColor c = Qt::red, QFont f = QFont());
int Utils_EXPORT ShowOKC(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowSave(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowYN(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowYNC(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowPRINT(QString tit, QString msg, QColor c = QColor(), QFont f = QFont());
int Utils_EXPORT ShowUSRDLG(QString tit, QString msg, QStringList btn, QColor col, QFont f);


// stylename - имя стиля из DialogMessageStyles
void Utils_EXPORT LoadStyledMessages(QString path);
int Utils_EXPORT ShowStyledMessage(int page, QString name, QString tit, QString msg);
int Utils_EXPORT ShowStyledOK(QString name, QString tit, QString msg);
int Utils_EXPORT ShowStyledOK(QString name, QString msg);

class Utils_EXPORT DialogMessage : public QDialog
{
    Q_OBJECT

public:
    int buttonNum; // порядковый номер нажатой клавиши
    QTimer timerClose;
    explicit DialogMessage(QWidget *parent = 0);
    ~DialogMessage();
    void setPage(int);
    void setTitle(QString);
    void setText(QString);
    void setFont(QFont);
    void setSize(int w, int h, bool toCenter = true);
    void setBtnUsr(int num, QString txt);
    void setTimeout(int ms);
    void hideButtons();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_8_clicked();
    void on_btnUsr1_clicked();
    void on_btnUsr2_clicked();
    void on_btnUsr3_clicked();
    void on_btnUsr4_clicked();

    void timeCloseUpdate();
    void on_btnYes_clicked();
    void on_btnNo_clicked();
    void on_btnCancel_clicked();

private:
    Ui::DialogMessage *ui;
    int timeOut;
};

#endif // DIALOGMESSAGE_H
