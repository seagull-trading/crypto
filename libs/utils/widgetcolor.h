#ifndef WIDGETCOLOR_H
#define WIDGETCOLOR_H

#include <QWidget>
#include "utils_global.h"

namespace Ui {
class WidgetColor;
}

class Utils_EXPORT WidgetColor : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetColor(QWidget *parent = 0);
    void setColor(QColor c);
    QColor getColor();
    ~WidgetColor();

private slots:
    void on_pushButton_clicked();

private:
    QColor Color;
    Ui::WidgetColor *ui;
signals:
    void colorChanged();
};

#endif // WIDGETCOLOR_H
