#include "logger.h"

#include <QDir>
#include <QFile>
#include <QDebug>
#include <QDateTime>
#include <QTextStream>

Logger Utils_EXPORT appLog;

Logger::Logger(QObject* parent)
    : QObject(parent)
{
    IsInit = false;
    IsQDeb = false;
    IsTranslateLat = false;
    clearLogPathTimer.setInterval(3600*24*1000);
    connect(&clearLogPathTimer,SIGNAL(timeout()),this,SLOT(clearLogTimerUpdate()));
    rusLat.clear();
    rusLat["Ё"] = "E";    rusLat["Й"] = "Y";    rusLat["Ц"] = "C";
    rusLat["У"] = "U";    rusLat["К"] = "K";    rusLat["Е"] = "E";
    rusLat["Н"] = "N";    rusLat["Г"] = "G";    rusLat["Ш"] = "SH";
    rusLat["Щ"] = "SH";   rusLat["З"] = "Z";    rusLat["Х"] = "H";
    rusLat["Ъ"] = "";     rusLat["Ф"] = "F";    rusLat["Ы"] = "Y";
    rusLat["В"] = "V";    rusLat["А"] = "A";    rusLat["П"] = "P";
    rusLat["Р"] = "R";    rusLat["О"] = "O";    rusLat["Л"] = "L";
    rusLat["Д"] = "D";    rusLat["Ж"] = "J";    rusLat["Э"] = "E";
    rusLat["Я"] = "YA";   rusLat["Ч"] = "CH";   rusLat["С"] = "S";
    rusLat["М"] = "M";    rusLat["И"] = "I";    rusLat["Т"] = "T";
    rusLat["Ь"] = "";     rusLat["Б"] = "B";    rusLat["Ю"] = "U";
    QStringList keys = rusLat.keys();
    for(int i = 0; i < keys.count(); i++)
        rusLat[ keys[i].toLower() ] = rusLat[ keys[i] ].toLower();
}

Logger::~Logger()
{
    clearLogPathTimer.stop();
}

void Logger::init(QString app, QString path, bool qdeb)
{
    AppName = app;
    Path = path;
    clearLogPathTimer.start();
    IsInit = true;
    setConsoleDebug(qdeb);
}

void Logger::setLogFileSuffix(QString suff)
{
    LogFileSuffix = suff;
}

void Logger::setConsoleDebug(bool qdeb)
{
    IsQDeb = qdeb;
}

void Logger::setTranslateLat(bool lat)
{
    IsTranslateLat = lat;
}

void Logger::addString(QString str)
{
    if(!IsInit) return;
    QString fileName = currentFileName();
    bool exists = QFile::exists(fileName);
    QFile mFile(fileName);
    if(!mFile.open(QFile::Append | QFile::Text))
    {
        qDebug() << "Could not open log file for writing: " << fileName;
        return;
    }
    QTextStream out(&mFile);
    if(!exists)
    {
        // для совместимости с процессами adp
        out << QDateTime::currentDateTimeUtc().toTime_t();
        out << "\n";
        out << "\nLogger version: " + LOGGER_VER + " (" + LOGGER_VER_DATE + ")\n";
    }
    out << transString(str);
    mFile.close();
}

QString Logger::transString(QString str)
{
    if(!IsTranslateLat) return str;
    QString dst;
    for(int i = 0; i < str.length(); i++)
    {
        QString ch = str.mid(i,1);
        dst.append(rusLat.contains(ch) ? rusLat[ch] : ch);
    }
    return dst;
}

void Logger::addLine(QString line)
{
    if(IsQDeb)
    {
        if(IsTranslateLat)
            qDebug() << transString(line).toLocal8Bit().data();
        else
            qDebug() << line.toUtf8().data();
    }
    addString("\n[" + QDateTime::currentDateTimeUtc().toString("dd.MM.yyyy hh:mm:ss.zzz") + "] " + line);
}

void Logger::addLines(QStringList tmp, QString prefix)
{
    for(int i = 0; i < tmp.count(); i++)
    {
        if(IsQDeb)
        {
            if(IsTranslateLat)
                qDebug() << transString(tmp[i]).toLocal8Bit().data();
            else
                qDebug() << tmp[i].toUtf8().data();
        }
        addString("\n" + prefix + tmp[i]);
    }
}

QString Logger::currentFileName()
{
    return Path + "/" + AppName + "_" + QDateTime::currentDateTimeUtc().toString("yyyyMMdd") + LogFileSuffix + ".log";
}

void Logger::clearOld(int days)
{
    if(!IsInit) return;
    QDir dir(Path);
    QStringList filters;
    filters << "*.log";
    QFileInfoList tmp = dir.entryInfoList(filters, QDir::Files | QDir::NoDotAndDotDot);
    QDateTime mind = QDateTime::currentDateTimeUtc().addDays(-days);
    addLine("Удаление старых log файлов до даты: " + mind.toString("dd.MM.yyyy"));
    addLine("Папка с лог файлами: " + Path);
    for(int i = 0; i < tmp.count(); i++)
    {
        QString fileName = tmp[i].fileName();
        if(fileName.mid(0, AppName.length()) != AppName) continue; // чистим только свои
        QRegExp rx("([0-9]{4})([0-9]{2})([0-9]{2})");
        if(fileName.indexOf(rx) == -1) continue;
        QDateTime d = QDateTime(QDate(rx.cap(1).toInt(),rx.cap(2).toInt(),rx.cap(3).toInt()), QTime());
        if(!d.isValid()) continue;
        if(d < mind)
        {
            if(QFile::remove(Path + "/" + fileName))
                addLine("  удаление " + fileName + " - успешно");
            else
                addLine("  удаление " + fileName + " - ошибка (невозможно удалить файл)");
        }
    }
}

void Logger::clearLogTimerUpdate()
{
    clearOld();
}

