﻿#ifndef PROPERTIES_H
#define PROPERTIES_H

#include <QHash>
#include <QFont>
#include <QString>
#include <QPointF>
#include <QColor>
#include <QPen>
#include <QBrush>
#include <QVariant>
#include <QSettings>
#include <QStringList>
#include <QApplication>
#include <QSplitter>

#include "utils_global.h"

//#define DATA_PATH      QString("data.adp")
//
// !
// ! Все функции в этом классе должны работать через set() и get()
// !
QVariant Utils_EXPORT GetPropertiesFromFile(QString fn, QString grp, QString key, QVariant def = QVariant());
void Utils_EXPORT SetPropertiesToFile(QString fn, QString grp, QString key, QVariant val);

class Utils_EXPORT Properties
{
public:
    QString Version;
    QString iniFileName;
    Properties();
    Properties(QString fn);
    void setFileName(QString fn);

    QVariant get(QString name, QVariant def = QVariant());
    QVariant getv(QString grp, QString name, QVariant def = QVariant());
    QStringList getGroups();

    void clear(bool rmfile = true);
    void set(QString name, QVariant val);
    void setv(QString grp, QString name, QVariant val);
    void setSplitter(QSplitter*, QString name = "");
    void getSplitter(QSplitter*, QString name = "");
    bool contains(QString name);
    bool containsGroup(QString grp);
    void beginGroup(QString grp);
    void endGroup();
    QString getPassword(QString name);
    QColor getColor(QString name, QColor def = QColor());
    QColor getColorv(QString grp, QString name, QColor def = QColor());
    void setColor(QString name, QColor c);
    void setColorv(QString grp, QString name, QColor c);
    void setPassword(QString name, QString pwd);
    void setpwd(QString name, QString pwd, QString key);
    QString getpwd(QString name, QString key);

    void setFont(QString name, QFont fnt);
    QFont getFont(QString name, QFont def);
    void setBrush(QString name, QBrush b);
    QBrush getBrush(QString name, QBrush b = QBrush());

    QPolygonF getPolygonF(QString name, QPolygonF def = QPolygonF());
    void setPolygonF(QString name, QPolygonF p);
    void save(bool clr = false);
    int load();
    QString fileName();

    QList<QPointF> getPointList(QString name);
    void setPointList(QString name, QList<QPointF> pnt);

    QPen getPen(QString name, QPen b);
    void setPen(QString name, QPen b);

    QHash<QString, QHash<QString,QVariant> > data; // data["group1"]["value1"]=...;
private:

    QString currGroup;
    QString passwordToString(QString pwd, QString key = "sec12ret");
    QString stringToPassword(QString str, QString key = "sec12ret");
};

extern Properties AdpProp;


#endif // PROPERTIES_H
