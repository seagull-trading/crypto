#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include "utils.h"

QString ApplicationLogFileName;

void ShowMessage(QString str)
{
    QMessageBox msgBox;
    msgBox.setText(str);
    msgBox.exec();
}

// вернет имя файла дополненное слева текущим катологом
QString AppDir(QString fn)
{
    if(!fn.isEmpty() && fn.mid(0,1) == "/") fn = fn.mid(1);
    return QDir::currentPath() + "/" + fn;
}

int CopyFile(QString srcFileName, QString dstFileName)
{
    QFile sourceFile( srcFileName );
    QFile destFile( dstFileName );
    bool success = true;
    success &= sourceFile.open( QFile::ReadOnly );
    success &= destFile.open( QFile::WriteOnly | QFile::Truncate );
    success &= destFile.write( sourceFile.readAll() ) >= 0;
    sourceFile.close();
    destFile.close();
    return success;
}

bool Utils_EXPORT FileExists(QString fileName)
{
    QFile fp(fileName);
    return fp.exists();
}

QDateTime Utils_EXPORT FileCreateDate(QString fileName)
{
    return QFileInfo(fileName).birthTime();
}

bool Utils_EXPORT DirExists(QString dir)
{
    QDir d(dir);
    return d.exists();
}

long Utils_EXPORT DirSize(const QString & str)
{
    long sizex = 0;
    QFileInfo str_info(str);
    if (str_info.isDir())
    {
        QDir dir(str);
        QFileInfoList list = dir.entryInfoList(QDir::Files | QDir::Dirs |  QDir::Hidden | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        for (int i = 0; i < list.size(); ++i)
        {
            QFileInfo fileInfo = list.at(i);
            if(fileInfo.isDir()) sizex += DirSize(fileInfo.absoluteFilePath());
            else sizex += fileInfo.size();
        }
    }
    return sizex;
}

void AddStringToFile(QString fileName, QString str)
{
    QFile mFile(fileName);
    if(!mFile.open(QFile::Append | QFile::Text))
    {
        qDebug() << "Could not open log file for writing: " << fileName;
        return;
    }
    QTextStream out(&mFile);
    out << str;
    mFile.close();
}
bool SaveStringToFile(QString fileName, QString str)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    QTextStream out(&file);
    out << str;
    file.close();
    return true;
}

bool SaveBuffToFile(QString fileName, char* buf, int sz)
{
    FILE* fp;
    if((fp = fopen(fileName.toLocal8Bit().data(),"wb")) == NULL) return false;
    for(int i = 0; i < sz; i++)
        fwrite(&buf[i],1,1,fp);
    fclose(fp);
    return true;
}


bool LoadStringFromFile(QString fileName, QString& str)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream in(&file);
    str = "";
    while (!in.atEnd())
        str.append(in.readLine() + "\n");
    file.close();
    return true;
}
void SetLogFilename(QString fn)
{
    ApplicationLogFileName = fn;
}

// Запись в лог файл
void AddLog(QString str)
{
    if(ApplicationLogFileName.isEmpty()) return;
    AddStringToFile(ApplicationLogFileName,
                    "\n[" + QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss") + "] " + str);
}

// разобьет строку на подстроки через разделители и вернёт подстроки с указателями на начало в words
//        01234567
// str = "AAA BB CCCCC" rsz = " "
// вернёт: words[0]="AAA", words[4]="BB", words[7]="CCCCC"
int SplitString(QString str, QString rsd, QMap<int, QString> & words, bool skipEmpty)
{
    words.clear();
    if(rsd.isEmpty()) return 0;
    QString w, s = str;// + rsd.mid(0,1);
    int k = 0;
    bool flag = false;
    for(int i = 0; i < s.length(); i++)
    {
        flag = false;
        for(int j = 0; j < rsd.length(); j++)
            if(s.mid(i,1) == rsd.mid(j,1)) flag = true;
        if(flag)
        {
            if(!skipEmpty || !w.isEmpty())
            {
                words[k] = w;
            }
            w = "";
            k = i+1;
        }else{
            w.append(s.mid(i,1));
        }
    }
    if(!flag)
    {
        if(!skipEmpty || !w.isEmpty())
            words[k] = w;
    }
    return words.count();
}

bool checkHHMM(QString hhmm, int tp)
{
    bool ok;
    if(hhmm.length() != 4) return false;
    int hh, mm;
    hh = hhmm.mid(0,2).toInt(&ok);
    if(!ok) return false;
    mm = hhmm.mid(2,2).toInt(&ok);
    if(!ok) return false;
    if(tp) return true;
    if(hh < 0 || hh > 23) return false;
    if(mm < 0 || mm > 59) return false;
    return true;
}

// загрузить строку из файла
QByteArray LoadArrayFromFile(QString fn)
{
    QFile file(fn);
    if(file.open(QIODevice::ReadOnly))
    {
        QByteArray buf = file.readAll();
        file.close();
        return buf;
    }
    return QByteArray();
}

// загрузить строку из файла (кодировка файла UTF8) !!!
QString LoadStringFromFile(QString fn)
{
    return QString(LoadArrayFromFile(fn));
}

// загрузит StringList из файла
bool LoadStringListFromFile(QString fn, QStringList& tmp, bool trim)
{
    QFile file(fn);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    tmp.clear();
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readLine();
        tmp.append(trim ? line.trimmed() : line);
    }
    file.close();
    return true;
}

// удалить папку вместе с подпапками
// если fileOnly = true  - удалятся только файлы а все директории остаются
//                 false - удаляется всё полностью
bool Utils_EXPORT removeDir(const QString &dirName, bool fileOnly)
{
    bool result = true;
    QDir dir(dirName);
    if(dir.exists(dirName))
    {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  |
                                                    QDir::AllDirs | QDir::Files, QDir::DirsFirst))
        {
            if(info.isDir())
            {
                if(!fileOnly) result = removeDir(info.absoluteFilePath());
            }else{
                result = QFile::remove(info.absoluteFilePath());
            }
            if(!result) return result;
        }
        result = dir.rmdir(dirName);
    }
    return result;
}
// вернет список директорий в QStringList
// не найдет директорию, если её имя начинается с символа "."
int Utils_EXPORT makeDirectoryList(QString path, QStringList& lst)
{
    QDir dir(path);
    QFileInfoList tmp = dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot);
    lst.clear();
    for(int i = 0; i < tmp.count(); i++)
    {
        if(!tmp[i].baseName().isEmpty())
            lst.append(tmp[i].baseName());
    }
    return lst.count();
}
// вернет список файлов из папки path в QStringList
int Utils_EXPORT makeFileList(QString path, QStringList& lst)
{
    QDir dir(path);
    QFileInfoList tmp = dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot);
    lst.clear();
    for(int i = 0; i < tmp.count(); i++)
    {
        if(!tmp[i].fileName().isEmpty())
            lst.append(tmp[i].fileName());
    }
    return lst.count();
}

// вернёт только файлы с расширением ext
int Utils_EXPORT makeFileListExt(QString path, QStringList& lst, QString ext)
{
    QDir dir(path);
    QFileInfoList tmp = dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot);
    lst.clear();
    for(int i = 0; i < tmp.count(); i++)
    {
        if(!tmp[i].fileName().isEmpty())
        {
            if(GetFileExt(tmp[i].fileName()).toUpper() == ext.toUpper())
                lst.append(tmp[i].fileName());
        }
    }
    return lst.count();
}

// вернёт имя папки (например: "C:/SRCQ/data")
QString Utils_EXPORT GetFilePath(QString fileName)
{
    QFileInfo fi(fileName);
    return fi.dir().path();
}
// вернёт имя файла (например: "armpf.ini")
QString Utils_EXPORT GetFileName(QString fileName, bool removeExt)
{
    QFileInfo fi(fileName);
    if(!removeExt) return fi.fileName();
    int ind = fi.fileName().lastIndexOf(".");
    if(ind == -1) return fi.fileName();
    return fi.fileName().mid(0,ind);
}
// вернёт расширение файла (например: "ini")
QString Utils_EXPORT GetFileExt(QString fileName)
{
    int ind = fileName.lastIndexOf(".");
    if(ind == -1) return QString();
    return fileName.mid(ind+1);
}

// ext = "txt" или "ini"
QString Utils_EXPORT SetFileExt(QString fileName, QString ext)
{
    int ind = fileName.lastIndexOf(".");
    if(ind == -1) return fileName + "." + ext;
    return fileName.mid(0,ind) + "." + ext;
}

// ext = "txt" или "ini"
QString Utils_EXPORT RemoveFileExt(QString fileName)
{
    int ind = fileName.lastIndexOf(".");
    if(ind == -1) return fileName;
    return fileName.mid(0,ind);
}

// подчеркнуть или выделить цветом текст (или убрать подчеркиванние)
// len = -1 подчеркнет весь текст
void Utils_EXPORT setTextEditCharFormat(QTextEdit* textEdit, int start, int len, QTextCharFormat tcf)
{
    if(textEdit->toPlainText().isEmpty()) return;
    if(len < 0) len = textEdit->toPlainText().length();
    QTextCursor c = textEdit->textCursor();
    QTextCharFormat tcf0 = textEdit->textCursor().charFormat();
    int pos = c.position();
    c.setPosition(start);
    c.setPosition(start + len,QTextCursor::KeepAnchor);
    c.setCharFormat(tcf);
    textEdit->setTextCursor(c);
    c.setPosition(pos);
    textEdit->setTextCursor(c);
    c.setCharFormat(tcf0);
}

// подчеркнуть/убрать подчёркивание
// len = -1 подчеркнет весь текст
void Utils_EXPORT setTextEditUndeline(QTextEdit* textEdit, int start, int len, QColor c, QTextCharFormat::UnderlineStyle style)
{
    if(textEdit->toPlainText().isEmpty()) return;
    QTextCharFormat tcf;
    if(c.isValid()) tcf.setUnderlineColor(c);
    tcf.setUnderlineStyle(style);
    setTextEditCharFormat(textEdit,start,len,tcf);
}

// 2021-10-20 12:30
// 2021-10-21
QDateTime DateTimeFromString(QString str, QDateTime def)
{
    if(str.isEmpty()) return def;
    QRegExp rx("(\\d+)");
    QVector<int> list;
    int pos = 0;
    while ((pos = rx.indexIn(str, pos)) != -1) {
        list << rx.cap(1).toInt();
        pos += rx.matchedLength();
    }
    QDateTime result;
    if(list.count() < 1) return def;
    int n = list.count();
    int yy = (n > 0) ? list[0] : QDate::currentDate().year();
    int mon = (n > 1) ? list[1] : QDate::currentDate().month();
    int dd = (n > 2) ? list[2] : QDate::currentDate().day();
    int hh = (n > 3) ? list[3] : 0;
    int mm = (n > 4) ? list[4] : 0;
    int ss = (n > 5) ? list[5] : 0;
    result.setDate(QDate(yy,mon,dd));
    result.setTime(QTime(hh,mm,ss));
    result.setTimeSpec(Qt::UTC);
    return result;
}

QString DateTimeToString(QDateTime tm)
{
    return tm.toString("yyyy-MM-dd hh:mm:ss");
}
