#include "widgetcalendar.h"
#include "ui_widgetcalendar.h"

WidgetCalendar::WidgetCalendar(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::WidgetCalendar)
{
  ui->setupUi(this);
  lineEdit = 0;
}

void WidgetCalendar::setMinimumDate(QDate d)
{
  ui->calendarWidget->setMinimumDate(d);
}

void WidgetCalendar::setMaximumDate(QDate d)
{
  ui->calendarWidget->setMaximumDate(d);
}

void WidgetCalendar::setLineEdit(QLineEdit* ed)
{
  lineEdit = ed;
}

void WidgetCalendar::setFormat(QString frm)
{
  format = frm;
}

void WidgetCalendar::setDate(QDate d)
{
  if(d.isValid())
    ui->calendarWidget->setCurrentPage(d.year(),d.month());
}

WidgetCalendar::~WidgetCalendar()
{
  delete ui;
}

void WidgetCalendar::on_calendarWidget_activated(const QDate &date)
{
  Result = date;
  if(lineEdit)
  {
    QDateTime t;
    t.setDate(date);
    t.setTime(QTime(12,0,0));
    lineEdit->setText(t.toString(format));
  }
  close();
}
