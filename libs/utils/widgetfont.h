#ifndef WIDGETFONT_H
#define WIDGETFONT_H
#include <QFont>
#include <QWidget>
#include "utils_global.h"

namespace Ui {
class WidgetFont;
}

class Utils_EXPORT WidgetFont : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetFont(QWidget *parent = 0);
    void setFontu(QFont);
    void setFontu(QString);
    QFont getFontu();
    ~WidgetFont();

private slots:
    void on_setFont_clicked();

private:
    Ui::WidgetFont *ui;
    QString fontName(QFont);
    bool fontDialog(QFont &font);
signals:
    void changeFont(QFont);
};

#endif // WIDGETFONT_H
