SOURCES +=\
     $$PWD/utils.cpp \
     $$PWD/dialogmessage.cpp \
     $$PWD/fprocess.cpp \
     $$PWD/dialogmemo.cpp \
     $$PWD/dialogwait.cpp \
     $$PWD/dialogmenu.cpp \
     $$PWD/d3des.cpp \
     $$PWD/fqtregexcheckerdialog.cpp \
     $$PWD/properties.cpp

HEADERS  += \
     $$PWD/utils.h \
     $$PWD/utils_global.h \
     $$PWD/dialogmessage.h \
     $$PWD/fprocess.h \
     $$PWD/dialogmemo.h \
     $$PWD/dialogwait.h \
     $$PWD/dialogmenu.h \
     $$PWD/d3des.h \
     $$PWD/fqtregexcheckerdialog.h \
     $$PWD/properties.h

FORMS    += \
     $$PWD/dialogmessage.ui \
     $$PWD/dialogmemo.ui \
     $$PWD/dialogwait.ui \
     $$PWD/dialogmenu.ui \
     $$PWD/fqtregexcheckerdialog.ui


