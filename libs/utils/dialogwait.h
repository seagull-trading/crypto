#ifndef DIALOGWAIT_H
#define DIALOGWAIT_H
#include <QLabel>
#include <QDialog>
#include "utils_global.h"

namespace Ui {
class DialogWait;
}

class Utils_EXPORT DialogWait : public QDialog
{
    Q_OBJECT

public:
    int onClose;  // 1 при нажатии на крестик(закрытие окна)
    explicit DialogWait(QWidget *parent = 0);
    void showProgress(int val = 0, int maxval = 0);
    void setText(QString str);
    void showCancelButton();
    QLabel* label();
    ~DialogWait();

private:
    Ui::DialogWait *ui;
protected:
    void closeEvent(QCloseEvent *);
private slots:
    void on_btnCancel_clicked();
};

#endif // DIALOGWAIT_H
