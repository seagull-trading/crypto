#include <QDebug>
#include <QWidget>
#include <QDateTime>
#include <QCalendarWidget>
#include "utils.h"
#include "widgetdatetime.h"
#include "widgetcalendar.h"
#include "ui_widgetdatetime.h"
#include "utils.h"
WidgetDateTime::WidgetDateTime(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetDateTime)
{
    Calendar = 0;
    timeShift = 0;
    defType = setDefaultValue;
    ui->setupUi(this);
    ui->btnNext->hide();
    ui->btnPrev->hide();
    format = "dd.MM.yyyy hh:mm";
    ui->lineEdit->setInputMask("99.99.9999 99:99");
    ui->btnDefault->hide();
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(dateTimeIsChanged()));
    showBtnUsr(false);
}

void WidgetDateTime::clear()
{
    ui->lineEdit->clear();
}

QDateTime WidgetDateTime::getDateTime()
{
    return stringToDateTime(ui->lineEdit->text(),format);
}

QDateTime WidgetDateTime::getUTC()
{
    QDateTime result = stringToDateTime(ui->lineEdit->text(), format);
    result.setTimeSpec(Qt::UTC);
    return result;
}

QDateTime WidgetDateTime::getDefault()
{
    return defVal;
}

QDate WidgetDateTime::getDate()
{
    return getDateTime().date();
}

QDateTime WidgetDateTime::stringToDateTime(QString str, QString format)
{
    QDateTime d;
    if(format.mid(0,6) == "yyMMdd") // специально для такого формата даты
        d = QDateTime::fromString("20" + str, "yy" + format);
    else
        d = QDateTime::fromString(str,format);
    return d;
}

QString WidgetDateTime::dateTimeToString(QDateTime d, QString format)
{
    return d.toString(format);
}

void WidgetDateTime::setDateTime(const QDateTime& d)
{
    ui->lineEdit->setText(dateTimeToString(d,format));
}

void WidgetDateTime::setFormat(QString frm, QString editMask)
{
    format = frm;
    ui->lineEdit->setInputMask(editMask);
}


void WidgetDateTime::showBtnCalendar(bool shw)
{
    ui->btnCalendar->setVisible(shw);
}

void WidgetDateTime::showBtnDefault(bool shw)
{
    ui->btnDefault->setVisible(shw);
}

void WidgetDateTime::showBtnClear(bool shw)
{
    ui->btnClear->setVisible(shw);
}

void WidgetDateTime::showBtnNextPrev(bool shw)
{
    ui->btnNext->setVisible(shw);
    ui->btnPrev->setVisible(shw);
}

void WidgetDateTime::showBtnUsr(bool shw)
{
    ui->btnUsr->setVisible(shw);
}

int WidgetDateTime::getMininumWidth()
{
    QFontMetrics fm(ui->lineEdit->font());
    int w = fm.width(format);
    w+= ui->btnNext->isVisible() ? ui->btnNext->width() : 0;
    w+= ui->btnPrev->isVisible() ? ui->btnPrev->width() : 0;
    w+= ui->btnDefault->isVisible() ? ui->btnDefault->width() : 0;
    w+= ui->btnCalendar->isVisible() ? ui->btnCalendar->width() : 0;
    w+= ui->btnClear->isVisible() ? ui->btnClear->width() : 0;
    w+= ui->btnUsr->isVisible() ? ui->btnUsr->width() : 0;
    return w ;
}

void WidgetDateTime::autoResize(int delta)
{
    QFontMetrics fm(ui->lineEdit->font());
    int w = fm.width(format) + delta;
    ui->lineEdit->setMinimumWidth(w);
    ui->lineEdit->setMaximumWidth(w);
    w = getMininumWidth() + delta;
    setMinimumWidth(w);
    setMaximumWidth(w);
}


void WidgetDateTime::setFocus()
{
    ui->lineEdit->setFocus();
}

void WidgetDateTime::setDefault(QDateTime dv)
{
    defVal = dv;
    ui->btnDefault->setToolTip(defVal.toString("dd.MM.yyyy hh:mm:ss"));
}

void WidgetDateTime::setTimeShift(double ts)
{
    timeShift = ts;
}

void WidgetDateTime::setDefaultType(WidgetDateTime::btnDefaultTypes t)
{
    defType = t;
}

QPushButton *WidgetDateTime::btnCalendar()
{
    return ui->btnCalendar;
}

QPushButton *WidgetDateTime::btnDefault()
{
    return ui->btnDefault;
}

QPushButton *WidgetDateTime::btnClear()
{
    return ui->btnClear;
}

QPushButton *WidgetDateTime::btnNext()
{
    return ui->btnNext;
}

QPushButton *WidgetDateTime::btnPrev()
{
    return ui->btnPrev;
}

QPushButton *WidgetDateTime::btnUsr()
{
    return ui->btnUsr;
}

void WidgetDateTime::refresh()
{
    on_lineEdit_textChanged(ui->lineEdit->text());
}

void WidgetDateTime::dateTimeIsChanged()
{
    QDateTime d = getDateTime();
    if(d.isValid())
        emit isChanged(d);
}

WidgetDateTime::~WidgetDateTime()
{
    delete ui;
}

QLineEdit *WidgetDateTime::lineEdit()
{
    return ui->lineEdit;
}

void WidgetDateTime::on_btnCalendar_clicked()
{
    if(Calendar) {
        delete Calendar;
        Calendar = 0;
    }
    Calendar = new WidgetCalendar();
    Calendar->setLineEdit(ui->lineEdit);
    Calendar->setFormat(format);
    Calendar->setMinimumDate(QDate(2000, 1, 1));
    Calendar->setMaximumDate(QDate(3000, 1, 1));
    QRect rect = Calendar->geometry();
    rect.setTopLeft(mapToGlobal(ui->lineEdit->geometry().bottomLeft()));
    Calendar->setGeometry(rect);
    Calendar->setWindowFlags(Qt::Popup);
    QDate d = stringToDateTime(ui->lineEdit->text(),format).date();
    if(d.year() < 2000) d = d.addYears(100);
    Calendar->setDate(d);
    Calendar->show();
}

void WidgetDateTime::on_btnClear_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit->setFocus();
}


void WidgetDateTime::on_lineEdit_textChanged(const QString &)
{
    if(!ui->lineEdit->text().isEmpty() && !getDateTime().isValid())
        ui->lineEdit->setStyleSheet("color: red;");
    else
        ui->lineEdit->setStyleSheet("");

}

void WidgetDateTime::wheelEvent(QWheelEvent* e)
{
    QDateTime d = getDateTime();
    if(!d.isValid()) return;
    if(e->delta() > 0)
    {
        setDateTime(d.addSecs(600));
    }else{
        setDateTime(d.addSecs(-600));
    }
}

void WidgetDateTime::on_btnDefault_clicked()
{
    switch(defType)
    {
    case setDefaultValue:
        setDateTime(defVal);
        break;
    case setCurrent:
        setDateTime(QDateTime::currentDateTime().addSecs(timeShift*3600));
        break;
    case setSignal:
        emit onDefaultPressed();
        break;
    }
    ui->lineEdit->setFocus();
}

void WidgetDateTime::on_btnPrev_clicked()
{
    QDateTime d = getDateTime();
    if(d.isValid())
        setDateTime(d.addDays(1));
    ui->lineEdit->setFocus();
}

void WidgetDateTime::on_btnNext_clicked()
{
    QDateTime d = getDateTime();
    if(d.isValid())
        setDateTime(d.addDays(-1));
    ui->lineEdit->setFocus();
}

void WidgetDateTime::on_btnUsr_clicked()
{
    emit btnUsrPressed();
}
