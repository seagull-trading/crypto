#include "utils.h"
#include "dialogmemo.h"
#include "ui_dialogmemo.h"
// окошечко с формой для ввода текста
bool ShowEditForm(QString& str, int page, QString fnt)
{
    bool ok = false;
    DialogMemo* dlg = new DialogMemo();
    dlg->setWindowFlags(Qt::Window);
    dlg->setWindowModality(Qt::ApplicationModal);
    dlg->setText(str);
    dlg->setPage(page);
    if(!fnt.isEmpty())
    {
        QFont font;
        if(font.fromString(fnt))
            dlg->setTextFont(font);
    }
    dlg->exec();
    if(dlg->ok)
    {
        str = dlg->getText();
        ok  = true;
    }
    delete dlg;
    return ok;
}

int ShowTextEdit(QString str, QString capt, int page, QString fnt, QSize sz, bool readOnly, bool asHtml)
{
    bool ok = false;
    DialogMemo* dlg = new DialogMemo();
    dlg->setWindowFlags(Qt::Window);
    dlg->setWindowTitle(capt);
    dlg->setWindowModality(Qt::ApplicationModal);
    if(asHtml) dlg->setHtml(str); else dlg->setText(str);
    dlg->setPage(page);
    dlg->textEdit()->setReadOnly(readOnly);
    if(!fnt.isEmpty())
    {
        QFont font;
        if(font.fromString(fnt))
            dlg->setTextFont(font);
    }
    if(!sz.isEmpty())
    {
        int x,y;
        dlg->show();
        x = dlg->geometry().x() + (dlg->geometry().width() - sz.width())/2;
        y = dlg->geometry().y() + (dlg->geometry().height() - sz.height())/2;
        dlg->setGeometry(x,y,sz.width(),sz.height());
    }
    dlg->exec();
    ok = dlg->ok;
    delete dlg;
    if(ok) return 0;
    return 1;
}

int ShowFileMemo(QString fn, QString capt, int page, QString fnt, QSize sz, bool readOnly)
{
    QString str = LoadStringFromFile(fn);
    if(str.isEmpty()) return -1;
    if(capt.isEmpty()) capt = str;
    return ShowTextEdit(str,capt,page,fnt,sz,readOnly);
}

// окошечко отображающее текст в QTextEdit
bool ShowMemo(QString str, int page)
{
    QString tmp = str;
    return ShowEditForm(str,page);
}

DialogMemo::DialogMemo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogMemo)
{
    ui->setupUi(this);
    btnNum = -1;
    ok = false;
}

void DialogMemo::setCaption(QString s)
{
    setWindowTitle(s);
}

void DialogMemo::setText(QString s)
{
    ui->textEdit->setText(s);
}

void DialogMemo::setHtml(QString s)
{
    ui->textEdit->setHtml(s);
}

void DialogMemo::setPage(int page)
{
    ui->stackedWidget->setCurrentIndex(page);
}

void DialogMemo::setTextFont(QFont fnt)
{
    ui->textEdit->setFont(fnt);
}

QTextEdit *DialogMemo::textEdit()
{
    return ui->textEdit;
}

QString DialogMemo::getText()
{
    return ui->textEdit->toPlainText();
}

DialogMemo::~DialogMemo()
{
    delete ui;
}


void DialogMemo::on_pushButton_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_2_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_3_clicked()
{
    ok = false;
    btnNum = 2;
    close();
}

void DialogMemo::on_pushButton_5_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_4_clicked()
{
    ok = false;
    btnNum = 2;
    close();
}

void DialogMemo::on_pushButton_6_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_7_clicked()
{
    ok = false;
    btnNum = 2;
    close();
}

void DialogMemo::on_pushButton_9_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_8_clicked()
{
    ok = true;
    btnNum = 2;
    close();
}

void DialogMemo::on_pushButton_11_clicked()
{
    ok = true;
    btnNum = 1;
    close();
}

void DialogMemo::on_pushButton_10_clicked()
{
    ok = false;
    btnNum = 2;
    close();
}
