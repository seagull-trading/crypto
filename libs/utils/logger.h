#ifndef LOGGER_H
#define LOGGER_H
#include <QHash>
#include <QTimer>
#include <QStringList>
#include "utils_global.h"

#define LOGGER_VER       QString("1.0.1")
#define LOGGER_VER_DATE  QString("11.10.2021")

class Utils_EXPORT Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = nullptr);
    ~Logger();
    void init(QString app, QString path, bool qdeb = false);
    void setLogFileSuffix(QString);
    void setConsoleDebug(bool);
    void setTranslateLat(bool lat);
    void addLine(QString line);
    void addLines(QStringList tmp, QString prefix = "");
    QString currentFileName();
    void clearOld(int days = 7);
private:
    bool IsInit = false, IsQDeb = false, IsTranslateLat = false;
    QTimer clearLogPathTimer;
    QString Path, AppName, LogFileSuffix;
    QHash<QString,QString> rusLat;
    void addString(QString);
    QString transString(QString);

private slots:
    void clearLogTimerUpdate();
};

extern Logger Utils_EXPORT appLog;

#endif // LOGGER_H
