#ifndef DIALOGMEMO_H
#define DIALOGMEMO_H

#include <QDialog>
#include <QTextEdit>
#include "utils_global.h"

bool Utils_EXPORT ShowEditForm(QString& str, int page = 0, QString fnt = "");
bool Utils_EXPORT ShowMemo(QString str, int page = 0);
int  Utils_EXPORT ShowTextEdit(QString str, QString capt, int page = 0, QString fnt = "", QSize sz = QSize(), bool readOnly = true, bool asHtml = false);
int  Utils_EXPORT ShowFileMemo(QString fn, QString capt, int page, QString fnt, QSize sz = QSize(), bool readOnly = true);

namespace Ui {
class DialogMemo;
}

class Utils_EXPORT DialogMemo : public QDialog
{
  Q_OBJECT
  
public:
  bool ok;
  int btnNum;
  explicit DialogMemo(QWidget *parent = 0);
  void setCaption(QString s);
  void setText(QString s);
  void setHtml(QString s);
  QString getText();
  void setPage(int page);
  void setTextFont(QFont);
  QTextEdit* textEdit();
  ~DialogMemo();
  
private slots:
  void on_pushButton_clicked();
  void on_pushButton_2_clicked();
  void on_pushButton_3_clicked();
  void on_pushButton_5_clicked();
  void on_pushButton_4_clicked();
  void on_pushButton_6_clicked();
  void on_pushButton_7_clicked();
  void on_pushButton_9_clicked();
  void on_pushButton_8_clicked();
  void on_pushButton_11_clicked();
  void on_pushButton_10_clicked();

private:
  Ui::DialogMemo *ui;
};

#endif // DIALOGMEMO_H
