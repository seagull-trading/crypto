QT       += core gui
TEMPLATE = lib
DEFINES += Utils_LIBRARY
QMAKE_CXXFLAGS += -std=c++11
TARGET = $$qtLibraryTarget(utils)
DESTDIR = ../../output

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

FORMS += \
    dblogindialog.ui \
    dialogmemo.ui \
    dialogmessage.ui \
    dialogwait.ui \
    widgetcalendar.ui \
    widgetcolor.ui \
    widgetdatetime.ui \
    widgetfont.ui

HEADERS += \
    d3des.h \
    dblogindialog.h \
    dialogmemo.h \
    dialogmessage.h \
    dialogwait.h \
    fileutils.h \
    logger.h \
    properties.h \
    utils.h \
    utils_global.h \
    widgetcalendar.h \
    widgetcolor.h \
    widgetdatetime.h \
    widgetfont.h

SOURCES += \
    d3des.cpp \
    dblogindialog.cpp \
    dialogmemo.cpp \
    dialogmessage.cpp \
    dialogwait.cpp \
    fileutils.cpp \
    logger.cpp \
    properties.cpp \
    utils.cpp \
    widgetcalendar.cpp \
    widgetcolor.cpp \
    widgetdatetime.cpp \
    widgetfont.cpp

RESOURCES += \
    utils_resource.qrc

LIBS += -L../../output
INCLUDEPATH += ../../libs/cutils

CONFIG(debug,debug|release){
    MOC_DIR = ../../tmp/build/MOC_debug/$${TARGET}
    OBJECTS_DIR = ../../tmp/build/Obj_debug/$${TARGET}
}
CONFIG(release,debug|release){
    MOC_DIR = ../../tmp/build/MOC_release/$${TARGET}
    OBJECTS_DIR = ../../tmp/build/Obj_release/$${TARGET}
}


