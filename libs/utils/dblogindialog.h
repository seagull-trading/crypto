#ifndef DBLOGINDIALOG_H
#define DBLOGINDIALOG_H

#include <QDialog>
#include "utils_global.h"

namespace Ui {
class DbLoginDialog;
}

class Utils_EXPORT DbLoginDialog : public QDialog
{
    Q_OBJECT

public:
    bool OK = true;
    explicit DbLoginDialog(QString fn, QWidget *parent = nullptr);
    ~DbLoginDialog();

    QString userName();
    QString password();
    QString host();
    int port(int def = 5342);
    QString dbName();

    void loadProps();
    void saveProps();
    void showText(QColor, QString);
    void showInfo(QString);
    void showError(QString);

private slots:
    void on_start_clicked();
    void on_cancel_clicked();

private:
    Ui::DbLoginDialog *ui;
    QString fileName;

};

#endif // DBLOGINDIALOG_H
