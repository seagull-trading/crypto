#ifndef UTILS_GLOBAL_H
#define UTILS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(Utils_LIBRARY)

#  define Utils_EXPORT Q_DECL_EXPORT

#else

#  define Utils_EXPORT Q_DECL_IMPORT

#endif


#endif // UTILS_GLOBAL_H
