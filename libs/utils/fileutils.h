#ifndef FILEUTILS_H
#define FILEUTILS_H
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QStringList>
#include <QTextEdit>
#include "utils_global.h"
class Utils_EXPORT CopyFolderProgress
{
public:
    CopyFolderProgress()
    {
        value = 0;
        valMax = 100;
        valMin = 0;
        logErrOnly = false;
        logEdit = 0;
    }
    void setValue(int p)
    {
        value = p;
        if(value > valMax) value = valMax;
        if(value < valMin) value = valMin;
    }
    void setMax(int m)
    {
        valMax = m;
    }
    void setMin(int m)
    {
        valMin = m;
    }
    int getValue()
    {
        return value;
    }
    void addLine(QString str)
    {
        if(!logEdit) return;
        if(logErrOnly) return;
        LastLine = str;
        QColor tc = logEdit->textColor();
        logEdit->setTextColor(QColor("black"));
        logEdit->append("[" + QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss.zzz") + "] " + str);
        logEdit->setTextColor(tc);
    }
    void addError(QString str)
    {
        if(!logEdit) return;
        LastError = str;
        QColor tc = logEdit->textColor();
        logEdit->setTextColor(QColor("red"));
        logEdit->append("[" + QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss.zzz") + "] " + str);
        logEdit->setTextColor(tc);
        logEdit->repaint();
    }
    void setLogErrOnly(bool le)
    {
        logErrOnly = le;
    }
    QString lastLine()
    {
        return LastLine;
    }
    QString lastError()
    {
        return LastError;
    }
    void setLogEdit(QTextEdit* e)
    {
        logEdit = e;
    }

private:
    bool logErrOnly;
    int value, valMax, valMin;
    QTextEdit* logEdit;
    QString LastLine, LastError;
};

void Utils_EXPORT MakeFileAndDirList(QString path, QStringList& dirs, QStringList& files, bool absPath = true);
bool Utils_EXPORT CopyFolder(QString src, QString dst, bool overWrite, CopyFolderProgress* p = 0, bool procEvents = false);
bool Utils_EXPORT CopyFilesFromList(QString src, QString dst, QStringList list, CopyFolderProgress* p = 0, bool procEvents = false);
bool Utils_EXPORT LoadStrListFromFile(QString fn, QStringList& tmp);
bool Utils_EXPORT FolderIsEmpty(QString path);


#endif // FILEUTILS_H
