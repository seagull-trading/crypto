#include <QDebug>
#include <QObject>
#include <QTextCodec>
#include <QDesktopWidget>
#include "utils.h"
#include "dialogmessage.h"
#include "ui_dialogmessage.h"

bool IsEnabledTimeMessages = true;
QHash<QString,QString> StyledMessagesStyles;

void EnableTimeMessages(bool v)
{
    IsEnabledTimeMessages = v;
}

//#include "GUIElements/include/windowpositionmanager.h"
// вернет 0 - окно закрыли, 1,2,... номер нажатой кнопки (1 - Ok, 2 - Cancel)
int ShowMessageDialog(int page, QString tit, QString msg, QColor c, QFont f)
{
    DialogMessage d;
    d.setWindowFlags(d.windowFlags() | Qt::WindowStaysOnTopHint);
    d.setPage(page);
    d.setTitle(tit);
    d.setText(msg);
    d.setFont(f);
    if(c.isValid())
    {
        QPalette p;
        p.setColor(QPalette::Window, c);
        d.setPalette(p);
    }
    //window_position::RestoreSize(&d);
    d.exec();
    return d.buttonNum;
}


void LoadStyledMessages(QString path)
{
    QStringList files;
    makeFileList(path, files);
    StyledMessagesStyles.clear();
    for(auto file : files)
    {
        QString name = GetFileName(file,true);
        QString stylesheet = LoadStringFromFile(path + "/" + file);
        StyledMessagesStyles[name] = stylesheet;
    }
}

int ShowStyledMessage(int page, QString name, QString tit, QString msg)
{
    DialogMessage d;
    d.setWindowFlags(d.windowFlags() | Qt::WindowStaysOnTopHint);
    d.setPage(page);
    d.setTitle(tit);
    d.setText(msg);
    if(StyledMessagesStyles.contains(name))
        d.setStyleSheet(StyledMessagesStyles[name]);
    d.exec();
    return d.buttonNum;
}

int ShowStyledOK(QString name, QString tit, QString msg)
{
    return ShowStyledMessage(0, name, tit, msg);
}

int ShowStyledOK(QString name, QString msg)
{
    return ShowStyledOK(name, QObject::tr("Сообщение"), msg);
}

void ShowTimeMessage(QString str, int timeout, QSize sz, QString title, QString sst)
{
    if(!IsEnabledTimeMessages) return;
    DialogMessage d;
    if(title.length())
    {
        d.setWindowTitle(title);
    }else{
        d.setWindowFlags(d.windowFlags() | Qt::FramelessWindowHint | Qt::SubWindow);
    }
    if(!sst.isEmpty())
        d.setStyleSheet(sst);
    d.hideButtons();
    d.setText(str);
    d.setTimeout(timeout);
    d.setMinimumSize(sz);
    d.setMaximumSize(sz);
    QRect screenGeometry = QApplication::desktop()->screenGeometry();
    int x = (screenGeometry.width() - sz.width()) / 2;
    int y = (screenGeometry.height() - sz.height()) / 2;
    d.setGeometry(QRect(x,y,sz.width(),sz.height()));
    d.exec();
}

void ShowStandartTimeMessage(QString str, int timeout)
{
    ShowTimeMessage(str, timeout, QSize(300,120), QString(),
                    ".QFrame { border: 2px solid gray; font-size: 23; }");

}

int ShowOK(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(0,tit,msg,col,f);
}

int ShowOK(QString msg, QColor col, QFont f)
{
    return ShowOK("", msg,col,f);
}

int ShowERR(QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(0,QObject::tr("Ошибка"), msg, col, f);
}
// если msg="" то никаких сообщений не выведет и вернёт -1
int ShowERROR(QString msg, QColor col, QFont f)
{
    if(msg.isEmpty()) return -1;
    return ShowMessageDialog(0,QObject::tr("Ошибка"), msg, col, f);
}


int ShowOKC(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(1,tit,msg,col,f);
}

int ShowSave(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(2,tit,msg,col,f);
}

int ShowYN(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(3,tit,msg,col,f);
}

int ShowPRINT(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(4,tit,msg,col,f);
}

int ShowYNC(QString tit, QString msg, QColor col, QFont f)
{
    return ShowMessageDialog(6,tit,msg,col,f);
}


int ShowUSRDLG(QString tit, QString msg, QStringList btn, QColor col, QFont f)
{
    DialogMessage d;
    d.setPage(5);
    d.setTitle(tit);
    d.setText(msg);
    d.setFont(f);
    if(col.isValid())
    {
        QPalette p;
        p.setColor(QPalette::Window, col);
        d.setPalette(p);
    }
    for(int i = 0; i < btn.count(); i++)
        d.setBtnUsr(i+1,btn[i]);
    d.exec();
    return d.buttonNum;
}

//------------------------------------------------------------------------------
DialogMessage::DialogMessage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogMessage)
{
    ui->setupUi(this);
    timeOut = -1;
    //setObjectName("DialogMessage");
    //window_position::SetEventFilter(this);
    buttonNum = 0;
    ui->btnUsr1->setVisible(false);
    ui->btnUsr2->setVisible(false);
    ui->btnUsr3->setVisible(false);
    ui->btnUsr4->setVisible(false);
}

DialogMessage::~DialogMessage()
{
    delete ui;
}

void DialogMessage::setPage(int page)
{
    ui->stackedWidget->setCurrentIndex(page);
}

void DialogMessage::setTitle(QString tit)
{
    if(!tit.isEmpty())
        setWindowTitle(tit);
}

void DialogMessage::setText(QString msg)
{
    ui->label->setText(msg);
}

void DialogMessage::setFont(QFont font)
{
    if(!font.key().isEmpty())
    {
        ui->stackedWidget->setFont(font);
        ui->label->setFont(font);
    }
}

void DialogMessage::on_pushButton_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_pushButton_2_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_pushButton_3_clicked()
{
    buttonNum = 2;
    close();
}

void DialogMessage::on_pushButton_5_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_pushButton_4_clicked()
{
    buttonNum = 2;
    close();
}

void DialogMessage::on_pushButton_6_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_pushButton_7_clicked()
{
    buttonNum = 2;
    close();
}

void DialogMessage::on_pushButton_9_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_pushButton_8_clicked()
{
    buttonNum = 2;
    close();
}


void DialogMessage::setSize(int w, int h, bool toCenter)
{
    // задать ширину и высоту
    QRect rect = geometry();
    if(w > -1) rect.setWidth(w);
    if(h > -1) rect.setHeight(h);
    setGeometry(rect);
    // сместить в центр экрана
    if(toCenter)
    {
        w = rect.width();
        h = rect.height();
        QRect desktopRect = QApplication::desktop()->availableGeometry(this);
        QPoint center = desktopRect.center();
        move(center.x()-w*0.5, center.y()-h*0.5);
    }
}

void DialogMessage::setBtnUsr(int num, QString txt)
{
    switch(num)
    {
    case 1: ui->btnUsr1->setText(txt); ui->btnUsr1->show(); break;
    case 2: ui->btnUsr2->setText(txt); ui->btnUsr2->show(); break;
    case 3: ui->btnUsr3->setText(txt); ui->btnUsr3->show(); break;
    case 4: ui->btnUsr4->setText(txt); ui->btnUsr4->show(); break;
    }
}

void DialogMessage::setTimeout(int ms)
{
    timerClose.setInterval(ms);
    connect(&timerClose,SIGNAL(timeout()), this, SLOT(timeCloseUpdate()));
    timerClose.start();
}

void DialogMessage::hideButtons()
{
    ui->stackedWidget->hide();
    ui->stackedWidget->setMaximumHeight(0);
}

void DialogMessage::on_btnUsr1_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_btnUsr2_clicked()
{
    buttonNum = 2;
    close();
}

void DialogMessage::on_btnUsr3_clicked()
{
    buttonNum = 3;
    close();
}

void DialogMessage::on_btnUsr4_clicked()
{
    buttonNum = 4;
    close();
}

void DialogMessage::timeCloseUpdate()
{
    buttonNum = -1;
    close();
}

void DialogMessage::on_btnYes_clicked()
{
    buttonNum = 1;
    close();
}

void DialogMessage::on_btnNo_clicked()
{
    buttonNum = 2;
    close();
}

void DialogMessage::on_btnCancel_clicked()
{
    buttonNum = 3;
    close();
}
