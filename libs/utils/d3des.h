#ifndef D3DES_H
#define D3DES_H
#include <QByteArray>
#include <QString>
#include "utils_global.h"
//----------------------------------------------------------------------------
// key[8] - ключ
// src[8] - строка
// dst[8] - результат
// Mode = 0 - шифровать, 1 - дешифровать
//----------------------------------------------------------------------------
void Utils_EXPORT des_crypt(unsigned char* key, unsigned char* src, unsigned char* dst, int Mode);

// строка ---> закодированный набор (key8 - ключ, 8 символов LAT)
QString Utils_EXPORT DesEncode(QString str, QString key8 = "sec12ret");

// закодированный набор ---> строка (key8 - ключ, 8 символов LAT)
QString Utils_EXPORT DesDecode(QString str, QString key8 = "sec12ret");


#endif // D3DES_H
