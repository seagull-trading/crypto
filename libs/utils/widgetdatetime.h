#ifndef WIDGETDATETIME_H
#define WIDGETDATETIME_H

#include <QDate>
#include <QWidget>
#include <QLineEdit>
#include <QWheelEvent>
#include <QPushButton>
#include "utils_global.h"

class WidgetCalendar;
namespace Ui {
class WidgetDateTime;
}

class  Utils_EXPORT WidgetDateTime : public QWidget
{
    Q_OBJECT

public:
    enum btnDefaultTypes { setDefaultValue, setCurrent, setSignal };

    explicit WidgetDateTime(QWidget *parent = 0);
    void clear();
    QDate getDate();
    QDateTime getDateTime();
    QDateTime getUTC();
    QDateTime getDefault();
    void setDateTime(const QDateTime&);
    ~WidgetDateTime();
    QLineEdit* lineEdit();
    void setFormat(QString frm, QString editMask);
    void setFocus();
    void setDefault(QDateTime dv);
    void setTimeShift(double ts);
    void setDefaultType(btnDefaultTypes t);
    QPushButton* btnCalendar();
    QPushButton* btnDefault();
    QPushButton* btnClear();
    QPushButton* btnNext();
    QPushButton* btnPrev();
    QPushButton* btnUsr();
    void refresh();

    void showBtnCalendar(bool shw = true);
    void showBtnDefault(bool shw = true);
    void showBtnClear(bool shw = true);
    void showBtnNextPrev(bool shw = true);
    void showBtnUsr(bool shw = true);

    int getMininumWidth();
    void autoResize(int delta = 20);

private slots:
    void dateTimeIsChanged();
    void on_btnCalendar_clicked();
    void on_btnClear_clicked();
    void on_lineEdit_textChanged(const QString &arg1);
    void on_btnDefault_clicked();
    void on_btnPrev_clicked();
    void on_btnNext_clicked();
    void on_btnUsr_clicked();

protected:
    void wheelEvent(QWheelEvent *);
private:
    btnDefaultTypes defType;
    double timeShift;  // интервал смещения в часах
    QString format;
    QDateTime defVal;
    WidgetCalendar* Calendar;
    Ui::WidgetDateTime *ui;

    QDateTime stringToDateTime(QString str, QString format);
    QString dateTimeToString(QDateTime d, QString format);
signals:
    void isChanged(const QDateTime& d);
    void onDefaultPressed();
    void btnUsrPressed();

};

#endif // WIDGETDATETIME_H
