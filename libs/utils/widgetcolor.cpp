#include <QDebug>
#include <QColorDialog>
#include "widgetcolor.h"
#include "ui_widgetcolor.h"

WidgetColor::WidgetColor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetColor)
{
    ui->setupUi(this);
    setColor(Qt::white);
}

void WidgetColor::setColor(QColor c)
{
    Color = c;
    ui->pushButton->setStyleSheet("QPushButton {  background-color: " + c.name() + ";  border-style: solid; }");
    ui->pushButton->update();
}

QColor WidgetColor::getColor()
{
    return Color;
}

WidgetColor::~WidgetColor()
{
    delete ui;
}

void WidgetColor::on_pushButton_clicked()
{
    QColor c = QColorDialog::getColor(Color,0,tr("Выбор цвета"), QColorDialog::ShowAlphaChannel);
    if(c.isValid())
    {
        setColor(c);
        emit colorChanged();
    }
}


