#include "dialogwait.h"
#include "ui_dialogwait.h"

DialogWait::DialogWait(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogWait)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowStaysOnTopHint);
    setWindowTitle(tr("Ожидание..."));
    onClose = 0;
    ui->btnCancel->hide();
}

void DialogWait::showProgress(int val, int maxval)
{
    ui->progressBar->setValue(val);
    ui->progressBar->setMaximum(maxval);
}

void DialogWait::setText(QString str)
{
    ui->label->setText(str);
}

void DialogWait::showCancelButton()
{
    ui->btnCancel->show();
}

QLabel *DialogWait::label()
{
    return ui->label;
}

DialogWait::~DialogWait()
{
    delete ui;
}

void DialogWait::closeEvent(QCloseEvent *)
{
    onClose++;
}

void DialogWait::on_btnCancel_clicked()
{
    onClose++;
    close();
}
