#include <QDebug>
#include <QObject>
#include <QTextCodec>
#include <QApplication>
#include "fileutils.h"

//CopyFolderProgress cpFolderProgress;

void MakeFileAndDirList(QString path, QStringList& dirs, QStringList& files, bool absPath)
{
    QDir dir(path);
    dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList fileList = dir.entryList();
    for (int i = 0; i < fileList.count(); i++)
        files << path + "/" + fileList[i];

    dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList dirList = dir.entryList();
    for (int i = 0; i < dirList.size(); i++)
    {
        QString newPath = dir.absolutePath() + "/" + dirList.at(i);
        dirs << newPath;
        MakeFileAndDirList(newPath,dirs,files,true);
    }
    if(!absPath) // удалить path из dirs и files
    {
        for(int i = 0; i < dirs.count(); i++)
            dirs[i] = dirs[i].remove(0,path.length() + 1);
        for(int i = 0; i < files.count(); i++)
            files[i] = files[i].remove(0,path.length()+1);
    }
}

// Копировать одну папку в другую (даже можно в себя!)
bool CopyFolder(QString src, QString dst, bool overWrite, CopyFolderProgress* p, bool procEvents)
{
    QStringList srcDirs, srcFiles, dstDirs, dstFiles;
    MakeFileAndDirList(src, srcDirs, srcFiles, false);
    MakeFileAndDirList(src, dstDirs, dstFiles, false);
    if(!srcDirs.count() && !srcFiles.count())
    {
        if(p) p->addError(QObject::tr("Исходная папка не существует или пуста: ") + src);
        return false;
    }
    if(!QDir(dst).exists())
    {
        if(!QDir().mkdir(dst))
        {
            if(p) p->addError(QObject::tr("Невозможно создать папку: ") + dst);
            return false;
        }
    }
    // создать папки
    for(int i = 0; i < srcDirs.count(); i++)
    {
        QString d = dst + "/" + srcDirs[i];
        if(!QDir(d).exists())
        {
            if(p) p->addLine(QObject::tr("   создать ") + d);
            if(!QDir().mkdir(d))
            {
                if(p) p->addError(QObject::tr("Невозможно создать папку: ") + d);
                return false;
            }
        }
    }
    // копировать файлы
    for(int i = 0; i < srcFiles.count(); i++)
    {
        QString fn = dst + "/" + srcFiles[i];
        if(procEvents) QApplication::processEvents();
        if(overWrite)
        {
            if(QFile::exists(fn))
            {
                if(p) p->addLine("удалить файл " + fn);
                if(!QFile::remove(fn))
                {
                    if(p) p->addError(QObject::tr("Невозможно удалить файл: ") + fn);
                    return false;
                }
            }
            if(p) p->addLine(fn);
            if(!QFile::copy(src + "/" + srcFiles[i], fn))
            {
                if(p)
                {
                    p->addError(QObject::tr("Невозможно скопировать файл: "));
                    p->addError(QObject::tr("   из ") + src + "/" + srcFiles[i]);
                    p->addError(QObject::tr("    в ") + fn);
                }
                return false;
            }
        }else{ // overwrite = false
            if(!QFile::exists(fn))
            {
                if(p) p->addLine(fn);
                if(!QFile::copy(src + "/" + srcFiles[i], fn))
                {
                    if(p)
                    {
                        p->addError(QObject::tr("Невозможно скопировать файл: "));
                        p->addError(QObject::tr("   из ") + src + "/" + srcFiles[i]);
                        p->addError(QObject::tr("    в ") + fn);
                    }
                    return false;
                }
            }
        }
    }
    return true;
}
// Скопировать папку с файлами src в dst
// 1. Скопировать файлы/папки только если их нет
// 2. Заменить только файлы указанные в списке(list)
// P.S.
// Пути указываются с прямыми слешами (/) имена относительно папки src
// структура папок воссоздаётся как в dst
// при установленнов cpFolderProgress->setActive(true) величина cpFolderProgress->getValue() от 0 до 200
bool CopyFilesFromList(QString src, QString dst, QStringList list, CopyFolderProgress* p, bool procEvents)
{
    if(!CopyFolder(src,dst,false, p, procEvents)) return false;
    for(int i = 0; i < list.count(); i++)
    {
        QString src_fn = src + "/" + list[i];
        QString dst_fn = dst + "/" + list[i];
        if(p) p->addLine(dst_fn);
        if(QFile::exists(dst_fn))
        {
            if(!QFile().remove(dst_fn))
            {
                if(p) p->addError(QObject::tr("Ошибка: невозможно удалить файл ") + dst_fn);
                return false;
            }
        }
        if(!QFile::copy(src_fn, dst_fn))
        {
            if(p) p->addError(QObject::tr("Ошибка: невозможно скопировать файл ") + src_fn + QObject::tr(" в папку ") + dst);
            return false;
        }
        if(procEvents) QApplication::processEvents();
    }
    return true;
}

bool LoadStrListFromFile(QString fn, QStringList& tmp)
{
    QFile file(fn);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    tmp.clear();
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readLine().trimmed();
        tmp.append(line);
    }
    return true;
}

bool FolderIsEmpty(QString path)
{
    QDir d(path);
    return !d.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot).count();
}
