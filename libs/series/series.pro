QT       += core gui network sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$qtLibraryTarget(series)

TEMPLATE = lib
DEFINES+= Series_LIBRARY

DESTDIR = ../../output
LIBS += -L../../output
INCLUDEPATH += ../../libs/utils

CONFIG(debug,debug|release) {
    LIBS += -lutilsd
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    LIBS += -lutils
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}

HEADERS += \
    candles.h \
    candlesloader.h \
    series.h \
    series_global.h \
    timeseries.h

SOURCES += \
    candles.cpp \
    candlesloader.cpp \
    series.cpp \
    timeseries.cpp

