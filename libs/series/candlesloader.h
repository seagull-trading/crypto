#ifndef CANDLESLOADER_H
#define CANDLESLOADER_H

#include <QObject>
#include <QSqlDatabase>
#include "series_global.h"

class Series_EXPORT CandlesLoader : public QObject
{
    Q_OBJECT

public:
    CandlesLoader(QSqlDatabase);
    // name = "eth_usdt" => грузятся минутки
    bool load(QString inst1, QString inst2);

private:
    QSqlDatabase dataBase;
};
#endif // CANDLESLOADER_H
