#include <QDir>
#include <QFile>
#include <QtMath>
#include <QTextStream>
#include "utils.h"
#include "candles.h"

QDateTime Candle::time()
{
    return QDateTime::fromTime_t(tm, Qt::UTC);
}

QString Candle::timeStr()
{
    return DateTimeToString(time());
}

double Candle::durationSecs()
{
    return double(duration_ms) / 1000;
}

double Candle::durationMinutes()
{
    return double(duration_ms) / 60000;
}

Candles::Candles()
{    
}

void Candles::clear()
{
    mCandles.clear();
}

QStringList Candles::all()
{
    QStringList file_list, result;
    makeFileList(fileFolder(), file_list);
    for(auto key : file_list)
        result << RemoveFileExt(key);
    return result;
}

bool Candles::load(QString inst1, QString inst2)
{
    clear();
    QString fn = fileName(inst1,inst2);
    QFile file(fn);
    if(!file.open(QIODevice::ReadOnly)) return false;
    QDataStream stream(&file);
    QString file_type;
    stream >> file_type;
    if(file_type != "candles")
    {
        qDeb << "Invalid file" << fn << "--> wrong file type";
        return false;
    }
    QString s1, s2;
    stream >> s1;
    stream >> s2;
    if(s1 != inst1 || s2 != inst2)
    {
        qDeb << "Invalid file" << fn << "--> wrong instId";
        return false;
    }
    quint32 sz = 0;
    stream >> sz;
    for(int i = 0; i < int(sz); i++)
    {
        Candle d;
        stream >> d.tm
                >> d.open >> d.high >> d.low >> d.close
                >> d.vol >> d.volCcy;
        mCandles << d;
    }
    file.close();
    return true;

}

bool Candles::save(QString inst1, QString inst2)
{
    QString fn = fileName(inst1,inst2);
    QFile file(fn);
    if(!file.open(QIODevice::WriteOnly)) return false;
    QDataStream stream(&file);
    QString file_type = "candles";
    stream << file_type;
    stream << inst1;
    stream << inst2;
    quint32 sz = mCandles.count();
    stream << sz;
    for(int i = 0; i < mCandles.count(); i++)
    {
        auto d = mCandles[i];
        stream << d.tm
               << d.open << d.high << d.low << d.close
               << d.vol << d.volCcy;
    }
    file.close();
    return true;
}

QVector<Candle> Candles::getCandles(int pos, int len, int period_sec, int n)
{
    QVector<Candle> result;
    int ind1 = pos, ind2 = pos + len;
    if(period_sec <= 0)
    {
        // Вернёт массив как есть
        for(int i = ind1; i <= ind2; i++)
        {
            result << mCandles[i];
            if(n > 0 && result.count() >= n) break;
        }
        return result;
    }
    // "Усреднять" по периодам
    int i = ind1;
    quint64 last_tm = time(ind1);
    while(i < ind2)
    {
        int i0 = i;
        Candle cand;
        bool is_find = false;
        while(i < ind2 && time(i) < last_tm + period_sec)
        {
            is_find = true;
            auto rec = mCandles[i];
            if(i == i0)
            {
                cand.tm   = rec.tm;
                cand.open = value(Open, i);
                cand.high = rec.high;
                cand.low  = rec.low;
            }else{
                if(rec.high > cand.high) cand.high = rec.high;
                if(rec.low  < cand.low)  cand.low  = rec.low;
            }
            cand.close  = rec.close;
            cand.vol   += rec.vol;
            cand.volCcy+= rec.volCcy;
            cand.duration_ms = 1000*period_sec;
            i++;
        }
        last_tm = time(i);
        if(is_find) result << cand;
        if(n > 0 && result.count() >= n) break;
    }
    return result;
}

QVector<Candle> Candles::getCandles(QString t1, QString t2, int period_sec, int n)
{
    int ind1, ind2;
    if(!getInterval(t1,t2,ind1,ind2)) return QVector<Candle>();
    return getCandles(ind1,ind2-ind1,period_sec,n);
}

double Candles::value(Candles::ValType vt, int index)
{
    return Candles::candleValue(mCandles[index], vt);
}

QVector<double> Candles::getValues(Candles::ValType vt, int pos, int len, int period_sec, int n)
{
    QVector<double> result;
    auto list = getCandles(pos,len,period_sec, n);
    for(auto it : list)
        result << Candles::candleValue(it,vt);
    return result;
}

QVector<double> Candles::getValues(Candles::ValType vt, QString t1, QString t2, int period_sec, int n)
{
    int ind1, ind2;
    if(!getInterval(t1,t2,ind1,ind2)) return QVector<double>();
    return getValues(vt,ind1,ind2-ind1,period_sec,n);
}

Series Candles::getSeries(Candles::ValType vt, QString t1, QString t2, int period_sec, int n)
{
    return Series(getValues(vt,t1,t2,period_sec,n));
}

Series Candles::getSeries(Candles::ValType vt, int pos, int len, int period_sec, int n)
{
    return Series(getValues(vt,pos,len,period_sec,n));
}

TimeSeries Candles::getTimeSeries(Candles::ValType vt, QString t1, QString t2, int period_sec, int n)
{
    int ind1, ind2;
    if(!getInterval(t1,t2,ind1,ind2)) return TimeSeries();
    return getTimeSeries(vt, ind1, ind2, period_sec, n);
}

TimeSeries Candles::getTimeSeries(Candles::ValType vt, int pos, int len, int period_sec, int n)
{
    TimeSeries ts;
    auto list = getCandles(pos,len,period_sec,n);
    for(auto it : list) ts.add(it.tm, candleValue(it, vt));
    return ts;
}

int Candles::count()
{
    return mCandles.count();
}

int Candles::find(quint64 tm)
{
    auto it = std::lower_bound(
                mCandles.begin(), mCandles.end(), tm,
                [](const Candle& s, quint64 tm) { return s.tm < tm; });
    return it != mCandles.end() ? std::distance(mCandles.begin(), it) : -1;
}

int Candles::findTime(QString t1)
{
    return find(DateTimeFromString(t1).toTime_t());
}

Candle Candles::candle(int index)
{
    return mCandles[index];
}

Candle Candles::candle(QString tm)
{
    int ind = find(DateTimeFromString(tm).toTime_t());
    return ind == -1 ? Candle() : mCandles[ind];
}

double Candles::candleValue(Candle it, Candles::ValType vt)
{
    switch (vt) {
    case Time:   return it.tm;
    case Open:   return it.open;
    case High:   return it.high;
    case Low:    return it.low;
    case Close:  return it.close;
    case Vol:    return it.vol;
    case VolCcy: return it.volCcy;
    }
    return 0;
}

void Candles::add(Candle d)
{
    mCandles << d;
}

void Candles::recalc()
{
    // сортировка
    std::sort(mCandles.begin(),
              mCandles.end(),
              [](const Candle& a, const Candle& b)
    {
        return a.tm < b.tm;
    });
}

QString Candles::fileFolder()
{
    return QDir::currentPath() + "/data/candles";
}

QString Candles::fileName(QString inst1, QString inst2)
{
    return fileFolder() + "/" + inst1 + "_" + inst2 + ".bin";
}

quint64 Candles::time(int index)
{
    return mCandles[index].tm;
}

double Candles::value(Candles::ValType vt, QString tm, double def)
{
    int ind = find(DateTimeFromString(tm).toTime_t());
    return ind == -1 ? def : value(vt, ind);
}

bool Candles::getInterval(QString t1, QString t2, int& a, int& b)
{
    if(mCandles.isEmpty()) return false;
    quint64 tm1 = t1.isEmpty() ? mCandles.first().tm: DateTimeFromString(t1).toTime_t();
    quint64 tm2 = t2.isEmpty() ? mCandles.last().tm: DateTimeFromString(t2).toTime_t();
    a = find(tm1);
    b = find(tm2);
    if(a == -1 || b == -1) return false;
    if(b < a) qSwap(a,b);
    return true;
}

double Candles::average(Candles::ValType vt, QString t1, QString t2)
{
    int a, b;
    if(!getInterval(t1,t2,a,b)) return 0;
    return average(vt, a, b - a + 1);
}

double Candles::average(Candles::ValType vt, int index, int len)
{
    double s = 0;
    for(int i = index; i < index + len; i++)
        s += value(vt, i);
    return s / len;
}

int Candles::findMax(Candles::ValType vt, int index, int len)
{
    int ind = -1;
    double m = value(vt,index);
    for(int i = index + 1; i < index + len; i++)
    {
        double v = value(vt,i);
        if(v > m) ind = i;
    }
    return ind;
}

int Candles::findMin(Candles::ValType vt, int index, int len)
{
    int ind = -1;
    double m = value(vt,index);
    for(int i = index + 1; i < index + len; i++)
    {
        double v = value(vt,i);
        if(v < m) ind = i;
    }
    return ind;
}

double Candles::getMax(Candles::ValType vt, int index, int len)
{
    int ind = findMax(vt,index,len);
    return ind == -1 ? 0 : value(vt, ind);
}

double Candles::getMin(Candles::ValType vt, int index, int len)
{
    int ind = findMin(vt,index,len);
    return ind == -1 ? 0 : value(vt, ind);
}

double Candles::stdDev(Candles::ValType vt, QString t1, QString t2)
{
    int a, b;
    if(!getInterval(t1,t2,a,b)) return 0;
    return stdDev(vt, a, b - a + 1);
}

double Candles::stdDev(Candles::ValType vt, int index, int len)
{
    double m = 0;
    for(int i = index; i < index + len; i++)
        m += value(vt, i);
    m /= len;
    double v = 0;
    for(int i = index; i < index + len; i++)
        v += pow(value(vt, i) - m, 2);
    return sqrt(v / len);
}

int Candles::bound(Candles::ValType vt, QString t1, double val1, double val2, int& secs, int secs_max)
{
    secs = 0;
    if(mCandles.isEmpty()) return -1;
    quint64 tm = t1.isEmpty() ? mCandles.first().tm: DateTimeFromString(t1).toTime_t();
    int ind = find(tm);
    if(ind == -1) return -1;
    quint64 tm_end = mCandles[ind].tm + secs_max;
    double v1 = qMin(val1,val2), v2 = qMax(val1,val2);
    for(int i = ind; i < mCandles.count(); i++)
    {
        double v = value(vt, i);
        if(mCandles[i].tm > tm_end) break;
        if(v < v1 || v > v2)
        {
            secs = mCandles[i].tm - mCandles[ind].tm;
            return i;
        }
    }
    return -1;
}

int Candles::boundUp(Candles::ValType vt, QString t1, double val, int &secs, int secs_max)
{
    secs = 0;
    if(mCandles.isEmpty()) return -1;
    quint64 tm = t1.isEmpty() ? mCandles.first().tm: DateTimeFromString(t1).toTime_t();
    int ind = find(tm);
    if(ind == -1) return -1;
    quint64 tm_end = mCandles[ind].tm + secs_max;
    for(int i = ind; i < mCandles.count(); i++)
    {
        double v = value(vt, i);
        if(mCandles[i].tm > tm_end) break;
        if(v > val)
        {
            secs = mCandles[i].tm - mCandles[ind].tm;
            return i;
        }
    }
    return -1;
}

int Candles::boundDown(Candles::ValType vt, QString t1, double val, int &secs, int secs_max)
{
    secs = 0;
    if(mCandles.isEmpty()) return -1;
    quint64 tm = t1.isEmpty() ? mCandles.first().tm: DateTimeFromString(t1).toTime_t();
    int ind = find(tm);
    if(ind == -1) return -1;
    quint64 tm_end = mCandles[ind].tm + secs_max;
    for(int i = ind; i < mCandles.count(); i++)
    {
        double v = value(vt, i);
        if(mCandles[i].tm > tm_end) break;
        if(v < val)
        {
            secs = mCandles[i].tm - mCandles[ind].tm;
            return i;
        }
    }
    return -1;
}

QVector<SeriesPath> Candles::priceDeviation(QString t1, QString t2, QString period,
                                            double delta_price, QString opt)
{
    QVector<SeriesPath> result;
    int period_sec = TimeSeries::strToIntervalSec(period.trimmed());
    if(period_sec < 1) return result;
    int ind1 = find(DateTimeFromString(t1).toTime_t());
    int ind2 = find(DateTimeFromString(t2).toTime_t());
    if(ind1 == -1 || ind2 == -1) return result;
    int i = ind1;
    quint64 last_tm = time(ind1);
    while(i < ind2)
    {
        // поиск high и low за период period_sec
        // high = max(high(i)), low = min(low(i))
        int i0 = i;
        double v_high = 0, v_low = 0;
        int v_high_index = 0, v_low_index = 0;
        while(i < ind2 && time(i) < last_tm + period_sec)
        {
            double vh = mCandles[i].high;
            double vl = mCandles[i].low;
            if(i == i0)
            {
                v_high = vh;
                v_high_index = i;

                v_low  = vl;
                v_low_index = i;
            }else{
                if(vh > v_high)
                {
                    v_high = vh;
                    v_high_index = i;
                }
                if(vl < v_low)
                {
                    v_low  = vl;
                    v_low_index = i;
                }
            }
            i++;
        }
        last_tm = time(i);

        // если high - low за период больше/меньше чем delta_price
        // то добавить его в результат
        if(v_high - v_low > delta_price)
        {
            if(opt == "compress")
            {
                // сжимаем интервал до экстемума
                if(v_high_index != v_low_index)
                {
                    i0 = qMin(v_high_index,v_low_index);
                    i  = qMax(v_high_index,v_low_index);
                }
            }
            SeriesPath p;
            QVariantHash inf;
            p.pos  = i0;
            p.len  = i - i0;
            inf["high"] = v_high;
            inf["high_index"] = v_high_index;
            inf["low"]  = v_low;
            inf["low_index"] = v_low_index;
            p.info = inf;
            result << p;
        }else{
            //i = i0 + 1;
        }
    }
    return result;
}

//TimeSeries Candles::SMA(ValType vt, QString t1, QString t2, int period_sec, int cnt)
//{
//    TimeSeries ts;
//    QVector<Candle> list = getCandles(t1, t2, period_sec*cnt);
//    int n = list.count();
//    if(!n) return ts;
//    double sma = 0;
//    double v0 = candleValue(list[0],vt);
//    for(int i = 1; i < list.count(); i++)
//    {
//        double v = candleValue(list[i],vt);
//        double delta = (v - v0) / n;
//        sma+= delta;
//        ts.add(list[i].tm, sma);
//    }
//    return ts;
//}

double Candles::indexRsi(QString t2, int m, int period_sec, double def)
{
    QDateTime tm2 = DateTimeFromString(t2);
    if(!tm2.isValid()) return def;
    QDateTime tm1 = tm2.addSecs(-(m+1)*period_sec);
    QString t1 = DateTimeToString(tm1);
    auto list = getCandles(t1,t2,period_sec);
    int n = list.count();
    if(n < m) return def; // данных недостаточно
    double sum_green = 0, sum_red = 0;
    for(int i = 0; i < m; i++)
    {
        int ind = i + (n - m);
        double d = list[ind].close - list[ind].open;
        if(d > 0) sum_green+= qAbs(d);
        else if(d < 0) sum_red+= qAbs(d);
    }
    if(!sum_red) return 100;
    double rs = sum_green / sum_red;
    return double(100) - double(100)/(rs + 1);
}
