#ifndef TIMESERIES_H
#define TIMESERIES_H
#include <QPair>
#include <QVector>
#include "series_global.h"

struct Series_EXPORT TimeSeriesRecord
{
    quint64 tm;
    double value;
};

// Временная последовательность (t,x)
class Series_EXPORT TimeSeries
{
public:
    TimeSeries();
    void clear();
    void add(quint64 tm, double val);
    void add(QString tm, double val);
    void sort();

    int count();
    double value(int index);
    quint64 time(int index);

    // "14" -> 14, "2m" -> 120, "1D" -> 86400
    static int strToIntervalSec(QString str);

private:
    QVector<TimeSeriesRecord> mData;
};

#endif // TIMESERIES_H
