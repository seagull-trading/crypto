#ifndef SERIES_GLOBAL_H
#define SERIES_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(Series_LIBRARY)
#  define Series_EXPORT Q_DECL_EXPORT
#else
#  define Series_EXPORT Q_DECL_IMPORT
#endif
#endif
