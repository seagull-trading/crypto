#include <QDir>
#include <QFile>
#include <QtMath>
#include <QSqlQuery>
#include <QByteArray>
#include "utils.h"
#include "series.h"

Series& operator << (Series& s, const SeriesDataRecord& val)
{
    s.add(val);
    return s;
}

Series& operator << (Series& s, const SeriesData& array)
{
    s.add(array);
    return s;
}

Series::Series()
{
}

Series::Series(QVector<SeriesDataRecord> a)
{
    add(a);
}

void Series::clear()
{
    mData.clear();
}

bool Series::load(QString name)
{
    clear();
    mData = loadArrayFromFile(fileName(name));
    return !mData.isEmpty();
}

bool Series::save(QString name)
{
    return saveArrayToFile(fileName(name),mData);
}

SeriesData Series::loadArrayFromFile(QString fn)
{
    SeriesData d;
    QFile file(fn);
    if(!file.open(QIODevice::ReadOnly))
        return SeriesData();
    QDataStream stream(&file);
    stream >> d;
    file.close();
    return d;
}

bool Series::saveArrayToFile(QString fn, SeriesData d)
{
    QFile file(fn);
    if(!file.open(QIODevice::WriteOnly))
        return false;
    QDataStream stream(&file);
    stream << d;
    file.close();
    return true;
}

int Series::count()
{
    return mData.count();
}

SeriesDataRecord Series::value(int index)
{
    return mData[index];
}

void Series::set(int index, SeriesDataRecord v)
{
    mData[index] = v;
}

void Series::setAll(SeriesData d)
{
    mData = d;
}

void Series::add(SeriesDataRecord v)
{
    mData << v;
}

void Series::add(SeriesData v)
{
    mData += v;
}

void Series::remove(int pos, int len)
{
    mData.remove(pos,len);
}

void Series::fill(int pos, int len, SeriesDataRecord v)
{
    int n = qMin(pos+len,mData.count());
    for(int i = pos; i < n; i++)
        mData[i] = v;
}

void Series::fillAll(SeriesDataRecord v)
{
    fill(0,mData.count(),v);
}

double Series::summ(int pos, int len)
{
    if(pos < 0) pos = 0;
    if(len < 0) len = mData.count();
    int n = qMin(pos+len,mData.count());
    double result = 0;
    for(int i = pos; i < n; i++)
        result+= mData[i];
    return result;
}

double Series::average(int pos, int len)
{
    if(pos < 0) pos = 0;
    if(len < 0) len = mData.count();
    int n = qMin(pos+len,mData.count());
    double result = 0;
    for(int i = pos; i < n; i++)
        result+= mData[i];
    return (n - pos) ? result / (n - pos)  : 0;
}

double Series::stdDev(int pos, int len)
{
    if(pos < 0) pos = 0;
    if(len < 0) len = mData.count();
    int n = qMin(pos+len,mData.count());
    if(n - pos <= 0) return 0;
    double result = 0;
    double a = average(pos, len);
    for(int i = pos; i < n; i++)
        result+= pow(mData[i] - a,2);
    return sqrt(result / (n - pos));
}

double Series::maxVal(int pos, int len)
{
    if(pos < 0) pos = 0;
    if(len < 0) len = mData.count();
    int n = qMin(pos+len,mData.count());
    if(n - pos <= 0) return 0;
    double result = mData[pos];
    for(int i = pos + 1; i < n; i++)
    {
        double v = mData[i];
        if(v > result) result = v;
    }
    return result;
}

double Series::minVal(int pos, int len)
{
    if(pos < 0) pos = 0;
    if(len < 0) len = mData.count();
    int n = qMin(pos+len,mData.count());
    if(n - pos <= 0) return 0;
    double result = mData[pos];
    for(int i = pos + 1; i < n; i++)
    {
        double v = mData[i];
        if(v < result) result = v;
    }
    return result;
}
//
// Пример (тест):
//
//    Series x;
//    for(int i = 0; i < 10; i++)
//        x << 11.23 + 0.333*i + 0.12345*i*i;
//    QVector<double> a;
//    x.leastSquares(a,2);
//
// Вернёт: a[0] = 11.23; a[1] = 0.333; a[2] = 0.12345
//
bool Series::leastSquares(QVector<double>& result, int m, int pos, int len)
{
    QVector<double> x, y, a, b;
    QVector< QVector<double> > sums;
    result.clear();
    a.fill(0, m+1);
    b.fill(0, m+1);
    for(int i = 0; i < m + 1; i++)
    {
        QVector<double> tmp;
        tmp.fill(0,m+1);
        sums << tmp;
    }
    int n = 0;
    if(len == -1) len = mData.count() - pos + 1;
    for(int i = pos; i < pos + len; i++)
    {
        if(i >= mData.count()) break;
        x << n;
        y << mData[i];
        n++;
    }
    for(int i = 0; i < m + 1; i++)
        for(int j = 0; j < m + 1; j++)
            for(int k = 0; k < n; k++)
                sums[i][j] += pow(x[k], i+j);

    for(int i = 0; i < m + 1; i++)
        for(int k = 0; k < n; k++)
            b[i] += pow(x[k], i) * y[k];

    //check if there are 0 on main diagonal and exchange rows in that case
    for(int i = 0; i < m + 1; i++)
    {
        if( sums[i][i] != 0 ) continue;
        for(int j = 0; j < m + 1; j++)
        {
            if(j==i) continue;
            if(sums[j][i] !=0 && sums[i][j]!=0)
            {
                for(int k = 0; k < m + 1; k++)
                {
                    double tmp = sums[j][k];
                    sums[j][k] = sums[i][k];
                    sums[i][k] = tmp;
                }
                double tmp = b[j];
                b[j] = b[i];
                b[i] = tmp;
                break;
            }
        }
    }
    //process rows
    for(int k = 0; k < m + 1; k++)
    {
        for(int i = k + 1; i < m + 1; i++)
        {
            if(sums[k][k] == 0)  return false; // Решение не существует
            double q = sums[i][k] / sums[k][k];
            for(int j = k; j < m + 1; j++)
                sums[i][j] -= q * sums[k][j];
            b[i] -= q*b[k];
        }
    }
    for(int i = m; i >= 0; i--)
    {
        double s = 0;
        for(int j = i; j < m + 1; j++)
            s+= sums[i][j]*a[j];
        a[i] = (b[i] - s) / sums[i][i];
    }
    // результат
    for(int i = 0; i < a.count(); i++)
        result << a[i];
    return true;
}

SeriesData Series::mid(int pos, int len)
{
    return mData.mid(pos, len);
}

QString Series::fileName(QString name)
{
    return QDir::currentPath() + "/data/local/" + name + ".bin";
}


