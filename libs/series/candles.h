#ifndef CANDLES_H
#define CANDLES_H

#include <QString>
#include <QVector>
#include <QDateTime>
#include "series_global.h"
#include "series.h"
#include "timeseries.h"

struct Series_EXPORT Candle
{
    double open = 0, high = 0, low = 0, close = 0;
    double vol = 0, volCcy = 0;
    quint64 tm = 0;
    int duration_ms = 0; // длительность в мс
    QDateTime time();
    QString timeStr();
    double durationSecs();
    double durationMinutes();
};

class Series_EXPORT Candles
{
public:
    enum ValType
    {
        Time,
        Open,
        High,
        Low,
        Close,
        Vol,
        VolCcy
    };


    Candles();
    void clear();
    static QStringList all();
    bool load(QString inst1, QString inst2);
    bool save(QString inst1, QString inst2);

    int count();
    Candle candle(int index);
    Candle candle(QString tm);

    static double candleValue(Candle it, ValType vt);

    // Получить массив свечей
    QVector<Candle> getCandles(int pos, int len, int period_sec, int n = 0);
    QVector<Candle> getCandles(QString t1, QString t2, int period_sec, int n = 0);

    // Получить массив значений (open,high,low,..)
    QVector<double> getValues(ValType vt, int pos, int len, int period_sec, int n = 0);
    QVector<double> getValues(ValType vt, QString t1, QString t2, int period_sec, int n = 0);

    // Получить последовательность значений
    Series getSeries(ValType vt, QString t1, QString t2, int period_sec, int n = 0);
    Series getSeries(ValType vt, int pos, int len, int period_sec, int n = 0);

    // Получить временную последовательность
    TimeSeries getTimeSeries(ValType vt, QString t1, QString t2, int period_sec, int n = 0);
    TimeSeries getTimeSeries(ValType vt, int pos, int len, int period_sec, int n = 0);


    // поиск первого элемента с значением tm >= t, скорость O(log(N))
    int find(quint64 t);
    int findTime(QString t1);

    // добавить (после добавления надо вызвать recalc для сортировки)
    void add(Candle);
    void recalc();

    // время по индексу
    quint64 time(int index);

    // значение open/high/low/close ...
    double value(ValType vt, int index);

    // значение open/high/low/close ... по времени tm = "2021-05-12 12:30"
    double value(ValType vt, QString tm, double def = 0);

    // вернет индекс начала и окончания [a,b]
    bool getInterval(QString t1, QString t2, int &a, int &b);

    // среднее значение за период с t1 по t2
    double average(ValType vt, QString t1 = "", QString t2 = "");
    double average(ValType vt, int index, int len);

    // максимум / минимум
    int findMax(ValType vt, int index, int len);
    int findMin(ValType vt, int index, int len);

    double getMax(ValType vt, int index, int len);
    double getMin(ValType vt, int index, int len);

    // среднее квадратичное отклонение за период с t1 по t2
    double stdDev(ValType vt, QString t1 = "", QString t2 = "");
    double stdDev(ValType vt, int index, int len);

    // вернёт индекс, когда величина выйдет за границы [val1, val2]
    // или -1 если не выходит никогда (secs - кол-во секунд)
    int bound(ValType vt, QString t1, double val1, double val2, int& secs, int secs_max);

    // вернёт индекс, когда величина превысит val
    // или -1 если не выходит никогда (secs - кол-во секунд)
    int boundUp(ValType vt, QString t1, double val, int& secs, int secs_max);

    // вернёт индекс, когда величина станет ниже val
    // или -1 если не выходит никогда (secs - кол-во секунд)
    int boundDown(ValType vt, QString t1, double val, int& secs, int secs_max);

    // Найти области где цена измененяется на величину
    // больше  чем delta_price за короткое время period_sec
    // Если opt = "compress" то сжимаем интервалы, так, чтобы начинался
    // и заканчивался на экстремуме
    QVector<SeriesPath> priceDeviation(QString t1, QString t2, QString period,
                                       double delta_price, QString opt = "compress");

    // Simple moving average (среднее значение)
    //TimeSeries SMA(ValType vt, QString t1, QString t2, int period_sec, int cnt);

    // Relative Strength Index (RSI)
    // диапазон: [0, 100]
    // Вернёт:
    //   70 и выше - актив перекуплен (ожидается падение)
    //   30 и ниже - актив перепродан (ожидается рост)
    // count = кол-во периодов (14 хорошее)
    //  для дневных свечей: period_sec = 24*3600, если count=14 то смотрим 14 суток назад от t2
    double indexRsi(QString t2, int count, int period_sec, double def = -1);

private:
    QString mInst1, mInst2;
    QVector<Candle> mCandles;
    QString fileName(QString inst1, QString inst2);
    static QString fileFolder();
};


#endif // CANDLES_H
