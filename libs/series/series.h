#ifndef SERIES_H
#define SERIES_H
#include <QVector>
#include <QString>
#include <QDateTime>
#include <QVariantHash>
#include "series_global.h"

typedef double SeriesDataRecord;
typedef QVector<SeriesDataRecord> SeriesData;


struct Series_EXPORT SeriesPath
{
    int pos = 0;
    int len = 0;
    QVariantHash info;
};

class Series_EXPORT Series
{
public:
    Series();
    Series(QVector<SeriesDataRecord>);

    // очистить
    void clear();

    bool load(QString name);
    bool save(QString name);

    //SeriesData get(QString t1, QString t2 = "", )

    // кол-во элементов
    int count();

    // элемент номер index = 0 .. count() - 1
    SeriesDataRecord value(int index);

    // задать значение
    void set(int index, SeriesDataRecord v);

    // задать все значения
    void setAll(SeriesData);

    // добавить в конец 1 элемент
    void add(SeriesDataRecord v);

    // добавить в конец массив
    void add(SeriesData v);

    // удалить кусок
    void remove(int pos, int len);

    // задать значение для всех элементов от pos до pos+len-1
    void fill(int pos, int len, SeriesDataRecord v);

    // задать все значения равные заданному
    void fillAll(SeriesDataRecord v);

    // сумма элементов
    double summ(int pos = 0, int len = -1);

    // среднее значение (мат. ожидание)
    double average(int pos = 0, int len = -1);

    // среднеквадратическое отклонение
    double stdDev(int pos = 0, int len = -1);

    // максимум
    double maxVal(int pos = 0, int len = -1);

    // минимум
    double minVal(int pos = 0, int len = -1);

    // метод наименьших квадратов вернёт коэфициенты y = a[0] + a[1]*x + a[2]*x^2 + ... a[m]*x^m
    // m - степень многочлена (a.count() = m + 1)
    bool leastSquares(QVector<double>& a, int m = 1, int pos = 0, int len = -1);

    // кусок данных
    SeriesData mid(int pos, int len = -1);


private:
    SeriesData mData;

    QString fileName(QString name);
    SeriesData loadArrayFromFile(QString fn);
    bool saveArrayToFile(QString fn, SeriesData d);


};

Series& Series_EXPORT operator << (Series& s, const SeriesDataRecord&);
Series& Series_EXPORT operator << (Series& s, const SeriesData&);


#endif // SERIES_H
