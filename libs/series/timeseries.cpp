#include "utils.h"
#include "timeseries.h"

TimeSeries::TimeSeries()
{

}

void TimeSeries::clear()
{
    mData.clear();
}

void TimeSeries::add(quint64 tm, double val)
{
    TimeSeriesRecord rec;
    rec.tm = tm;
    rec.value = val;
    mData << rec;
}

void TimeSeries::add(QString tm, double val)
{
    QDateTime t = DateTimeFromString(tm);
    if(!t.isValid()) return;
    add(t.toTime_t(), val);
}

int TimeSeries::count()
{
    return mData.count();
}

double TimeSeries::value(int index)
{
    return mData[index].value;
}

quint64 TimeSeries::time(int index)
{
    return mData[index].tm;
}

int TimeSeries::strToIntervalSec(QString str)
{
    bool ok = false;
    if(str.isEmpty()) return 0;
    int sec = str.toInt(&ok);
    if(ok) return sec;
    int mult = 1;
    QString ch = str.mid(str.length() - 1, 1).toLower();
    if(ch == "m") mult = 60;
    else if(ch == "h") mult = 3600;
    else if(ch == "d") mult = 86400;
    return mult*str.mid(0,str.length() - 1).toInt();
}

void TimeSeries::sort()
{
    // сортировать по времени
    std::sort(mData.begin(),
              mData.end(),
              [](const TimeSeriesRecord& a, const TimeSeriesRecord& b)
    {
        return a.tm < b.tm;
    });

}
