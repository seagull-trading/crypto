#include <QDir>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QDateTime>
#include "candles.h"
#include "candlesloader.h"

CandlesLoader::CandlesLoader(QSqlDatabase db)
{
    dataBase = db;
}

bool CandlesLoader::load(QString inst1, QString inst2)
{
    qDebug() << Q_FUNC_INFO << "Load" << inst1 << inst2;
    QSqlQuery q(dataBase);
    if(!q.exec("select * from " + inst1 + "_" + inst2 + "_1m" + " order by tm"))
        return false;
    QVector<double> open, high, low, close, vol, volCcy, tm;
    Candles candles;
    while(q.next())
    {
        Candle rec;
        rec.open    = q.value("open").toDouble();
        rec.high    = q.value("high").toDouble();
        rec.low     = q.value("low").toDouble();
        rec.close   = q.value("close").toDouble();
        rec.vol     = q.value("vol").toDouble();
        rec.volCcy  = q.value("volccy").toDouble();
        rec.tm      = q.value("tm").toDateTime().toTime_t();
        candles.add(rec);
    }
    candles.recalc();
    candles.save(inst1, inst2);
    return true;

}
