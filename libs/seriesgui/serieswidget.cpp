#include "serieswidget.h"
#include "ui_serieswidget.h"

SeriesWidget::SeriesWidget(QString title, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SeriesWidget)
{
    ui->setupUi(this);
    setWindowTitle(title.isEmpty() ? tr("График") : title);
    ui->graph->setDateAxis(false);
    ui->graph->drawDayNight = ui->graph->isDateAxis();
    mPens.clear();
    mPens["open"]  = QPen(Qt::red);
    mPens["high"]  = QPen(Qt::blue, 3);
    mPens["low"]   = QPen(Qt::darkCyan,3);
    mPens["close"] = QPen(Qt::darkGreen);

}

void SeriesWidget::clearTimeSeries()
{
    mTimeSeries.clear();
    recalc();
}

void SeriesWidget::setTimeSeries(Series s)
{
    mTimeSeries = s;
    recalc();
}

void SeriesWidget::setSeries(QString name, Series s, bool resc)
{
    mSeries[name] = s;
    recalc();
    if(resc) ui->graph->myRescaleAxes();
}

void SeriesWidget::removeSeries(QString name)
{
    mSeries.remove(name);
    recalc();
}

void SeriesWidget::setCandles(QString name, Candles c, bool resc)
{
    mCandles[name] = c;
    recalc();
    if(resc) ui->graph->myRescaleAxes();
}

void SeriesWidget::removeCandles(QString name)
{
    mCandles.remove(name);
    recalc();
}

void SeriesWidget::setPen(QString name, QPen pen)
{
    mPens[name] = pen;
}

void SeriesWidget::removePen(QString name)
{
    mPens.remove(name);
}

FinPlotWidget *SeriesWidget::plot()
{
    return ui->graph;
}

void SeriesWidget::setPos(int x, int y)
{
    int w = width();
    int h = height();
    setGeometry(x,y,w,h);
}

SeriesWidget::~SeriesWidget()
{
    delete ui;
}

// name="eth-usdt", what="open"
QPen SeriesWidget::penForCandles(QString name, QString what)
{
    return mPens.contains(name + "." + what) ? mPens[name + "." + what] : mPens[what];
}

void SeriesWidget::recalc()
{
    int index = 0;
    int n = mTimeSeries.count();
    ui->graph->setDateAxis(mTimeSeries.count() || mCandles.count());
    ui->graph->drawDayNight = ui->graph->isDateAxis();
    ui->graph->clearGraphs();
    for(auto name : mSeries.keys())
    {
        QPen pen = mPens.contains(name) ? mPens[name] : QPen(Qt::black);
        for(int i = 0; i < mSeries[name].count(); i++)
        {
            double x = (i < n) ? mTimeSeries.value(i) : i;
            double y = mSeries[name].value(i);
            ui->graph->addValue(index, x, y);
        }
        ui->graph->setPen(index, pen);
        index++;
    }
    for(auto name : mCandles.keys())
    {
        int index_open  = index;
        int index_high  = index+1;
        int index_low   = index+2;
        int index_close = index+3;
        ui->graph->setPen(index_open , penForCandles(name, "open"));
        ui->graph->setPen(index_high , penForCandles(name, "high"));
        ui->graph->setPen(index_low  , penForCandles(name, "low"));
        ui->graph->setPen(index_close, penForCandles(name, "close"));
        for(int i = 0; i < mCandles[name].count(); i++)
        {
            auto rec = mCandles[name].candle(i);
            double x = rec.tm;
            ui->graph->addValue(index_open , x, rec.open);
            ui->graph->addValue(index_high , x, rec.high);
            ui->graph->addValue(index_low  , x, rec.low);
            ui->graph->addValue(index_close, x, rec.close);
        }

        index+=4;
    }

}
