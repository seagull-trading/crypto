#include "utils.h"
#include "finplotwidget.h"
#include "candleswidget.h"
#include "ui_candleswidget.h"

CandlesWidget::CandlesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CandlesWidget)
{
    ui->setupUi(this);
    ui->cmd->hide();
    mPens.clear();
    mPens["open"]  = QPen(Qt::red);
    mPens["high"]  = QPen(Qt::blue, 3);
    mPens["low"]   = QPen(Qt::darkCyan,3);
    mPens["close"] = QPen(Qt::darkGreen);
}

void CandlesWidget::addCandles(Candles* candles)
{
    int index = ui->graph->graphCount();
    for(int i = 0; i < candles->count(); i++)
    {
        auto rec = candles->candle(i);
        double x = rec.tm;
        ui->graph->addValue(index  , x, rec.open);
        ui->graph->addValue(index+1, x, rec.high);
        ui->graph->addValue(index+2, x, rec.low);
        ui->graph->addValue(index+3, x, rec.close);
    }
    ui->graph->setPen(index  , mPens["open"]);  ui->graph->graph(index  )->setProperty("graph_type","open");
    ui->graph->setPen(index+1, mPens["high"]);  ui->graph->graph(index+1)->setProperty("graph_type","high");
    ui->graph->setPen(index+2, mPens["low"]);   ui->graph->graph(index+2)->setProperty("graph_type","low");
    ui->graph->setPen(index+3, mPens["close"]); ui->graph->graph(index+3)->setProperty("graph_type","close");
    ui->graph->myRescaleAxes();
}

void CandlesWidget::addSeries(TimeSeries* ts, QPen pen, bool resc)
{
    int index = ui->graph->graphCount();
    for(int i = 0; i < ts->count(); i++)
        ui->graph->addValue(index, ts->time(i), ts->value(i));
    ui->graph->setPen(index,pen);
    if(resc) ui->graph->myRescaleAxes();
}

//void CandlesWidget::addBars(TimeSeries *s, int width_sec, QPen p, QBrush b)
//{
//}

FinPlotWidget *CandlesWidget::graph()
{
    return ui->graph;
}

CandlesWidget::~CandlesWidget()
{
    delete ui;
}

void CandlesWidget::setPos(int x, int y)
{
    int w = width(), h = height();
    setGeometry(QRect(x,y,w,h));
}

void CandlesWidget::refreshGraphs()
{
    for(int i = 0; i < ui->graph->graphCount(); i++)
    {
        auto g = ui->graph->graph(i);
        QString tp = g->property("graph_type").toString();
        if(tp == "open")  g->setVisible(ui->open->isChecked());
        if(tp == "high")  g->setVisible(ui->high->isChecked());
        if(tp == "low")   g->setVisible(ui->low->isChecked());
        if(tp == "close") g->setVisible(ui->close->isChecked());
    }
    ui->graph->replot();
}

void CandlesWidget::on_open_clicked(bool)
{
    refreshGraphs();
}

void CandlesWidget::on_high_clicked(bool)
{
    refreshGraphs();
}

void CandlesWidget::on_low_clicked(bool)
{
    refreshGraphs();
}

void CandlesWidget::on_close_clicked(bool)
{
    refreshGraphs();
}


void CandlesWidget::on_menu_clicked()
{
    ui->cmd->setVisible(!ui->cmd->isVisible());
}

void CandlesWidget::on_cmd_returnPressed()
{
    QStringList tmp = ui->cmd->text().split("-");
    if(tmp.count() == 2)
    {
        quint64 t1 = DateTimeFromString(tmp[0].trimmed()).toTime_t();
        quint64 t2 = DateTimeFromString(tmp[1].trimmed()).toTime_t();
        if(t1 && t2)
        {
            ui->graph->xAxis->setRange(t1,t2);
            ui->graph->replot();
            ui->cmd->clear();
            return;
        }
    }
}
