#ifndef CANDLESWIDGET_H
#define CANDLESWIDGET_H

#include <QWidget>
#include <QPainter>
#include "candles.h"
#include "timeseries.h"
#include "series_global.h"

namespace Ui {
class CandlesWidget;
}

class FinPlotWidget;
class Series_EXPORT CandlesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CandlesWidget(QWidget *parent = nullptr);

    void clear();
    // нарисовать свечи
    void addCandles(Candles*);

    // нарисовать временной ряд
    void addSeries(TimeSeries* s, QPen p = QPen(Qt::black), bool resc = false);

    // нарисовать прямоугольники длинной width_sec секунд
    //void addBars(TimeSeries* s, int width_sec, QPen p = QPen(Qt::black), QBrush b = QBrush(Qt::yellow));

    FinPlotWidget* graph();

    ~CandlesWidget();

    void setPos(int x, int y);
private slots:
    void on_open_clicked(bool);
    void on_high_clicked(bool);
    void on_low_clicked(bool);
    void on_close_clicked(bool);
    void on_menu_clicked();
    void on_cmd_returnPressed();

private:
    Ui::CandlesWidget *ui;
    QHash<QString, QPen> mPens;
    void refreshGraphs();
};

#endif // CANDLESWIDGET_H
