QT       += core gui network sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$qtLibraryTarget(seriesgui)

TEMPLATE = lib
DEFINES+= Series_LIBRARY

DESTDIR = ../../output
LIBS += -L../../output
INCLUDEPATH += ../../libs/utils
INCLUDEPATH += ../../libs/fplot
INCLUDEPATH += ../../libs/series

CONFIG(debug,debug|release) {
    LIBS += -lutilsd -lfplotd -lseriesd
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    LIBS += -lutils -lfplot -lseries
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}

HEADERS += \
    candleswidget.h \
    series_gui.h \
    serieswidget.h \
    seriesgui_global.h

SOURCES += \
    candleswidget.cpp \
    series_gui.cpp \
    serieswidget.cpp

FORMS += \
    candleswidget.ui \
    serieswidget.ui

