#ifndef SERIESWIDGET_H
#define SERIESWIDGET_H
#include <QHash>
#include <QWidget>
#include <QPainter>
#include "series.h"
#include "candles.h"
#include "series_global.h"

namespace Ui {
class SeriesWidget;
}

class FinPlotWidget;
class Series_EXPORT SeriesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SeriesWidget(QString titl = "", QWidget *parent = nullptr);

    void clearTimeSeries();
    void setTimeSeries(Series s);

    void setSeries(QString name, Series s, bool resc = true);
    void removeSeries(QString name);

    void setCandles(QString name, Candles c, bool resc = true);
    void removeCandles(QString name);

    void setPen(QString name, QPen pen);
    void removePen(QString name);

    FinPlotWidget* plot();
    void setPos(int x, int y);

    ~SeriesWidget();

private:
    Ui::SeriesWidget *ui;
    Series mTimeSeries;
    QHash<QString, Series> mSeries;
    QHash<QString, Candles> mCandles;
    QHash<QString, QPen> mPens;
    void recalc();
    QPen penForCandles(QString name, QString what);
};

#endif // SERIESWIDGET_H
