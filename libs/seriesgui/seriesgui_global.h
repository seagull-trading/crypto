#ifndef SERIESGUI_GLOBAL_H
#define SERIESGUI_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(SeriesGui_LIBRARY)
#  define SeriesGui_EXPORT Q_DECL_EXPORT
#else
#  define SeriesGui_EXPORT Q_DECL_IMPORT
#endif
#endif
