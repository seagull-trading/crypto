#include "okextime.h"

OkexTime::OkexTime()
{
    mTime.setTimeSpec(Qt::UTC);
}

OkexTime::OkexTime(QDateTime tm)
{
    mTime = QDateTime(tm.date(), tm.time(), Qt::UTC);
}

OkexTime::OkexTime(quint64 ts)
{
    mTime.setTimeSpec(Qt::UTC);
    fromTimeStamp(ts);
}

QDateTime OkexTime::dateTime()
{
    return mTime;
}

uint OkexTime::toTime_t()
{
    return mTime.toTime_t();
}

QString OkexTime::toString()
{
    return mTime.toString("yyyy-MM-dd") + "T" +
            mTime.toString("hh:mm:ss.zzz") + "Z";
}

QString OkexTime::toStr(QString format)
{
    return mTime.toString(format);
}

quint64 OkexTime::timeStamp()
{
    return quint64(mTime.toTime_t())*1000;
}

void OkexTime::fromTimeStamp(quint64 ts)
{
    mTime = mTime.fromTime_t(ts/1000, Qt::UTC);
}

void OkexTime::addSecs(int delta)
{
    mTime = mTime.addSecs(delta);
}

void OkexTime::addHours(int delta)
{
    mTime = mTime.addSecs(3600*delta);
}

void OkexTime::addDays(int delta)
{
    mTime = mTime.addDays(delta);
}
