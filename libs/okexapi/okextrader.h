#ifndef OKEXTRADER_H
#define OKEXTRADER_H
#include <QUrl>
#include <QDebug>
#include <QUrlQuery>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QLoggingCategory>
#include <QNetworkAccessManager>
#include <QMessageAuthenticationCode>

#include "okextime.h"
#include "okexapi.h"
#include "okexerror.h"

class OkexApi_EXPORT OkexTrader : public QObject
{
    Q_OBJECT

public:
    explicit OkexTrader(QObject* parent = nullptr);

    static QStringList barSizeList();

    // Вернёт true если предыдущий запрос всё ещё выполняется
    bool static requestProgress();

    // задать таймаут при http запросе в мс
    void setRequestTimeout(int);
    int requestTimeout();

    void setTest(QString);
    void setLogFile(QString);
    void clearTest();

    // Вернёт:
    // 0 - недопустимая пара или ошибка запроса
    // 1 - если запрос истории выполнять через /api/v5/market/history-candles (getCandlesHistory)
    // 2 - если запрос истории выполнять через /api/v5/market/history-candles (getCandles)
    OkexHistoryType checkCandlesHistory(QString instId);

    /* Запросить баланс */
    QHash<QString,OkexBalance> getBalance(QStringList act = QStringList());
    OkexBalance getBalance(QString act);

    /* Запросить свечи (недавние)
       GET /api/v5/market/candles
       insId   = BTC-USD
       barSize = 1m/3m/5m/15m/30m/1H/2H/4H/6H/12H/1D/1W/1M/3M/6M/1Y*/
    OkexCandleList getCandles(QString instId, qint64 t1, qint64 t2, QString barSize = "1D", int limit = 100);

    /* Запросить свечи (длинную историю)
       GET /api/v5/market/history-candles
       insId   = BTC-USD
       barSize = 1m/3m/5m/15m/30m/1H/2H/4H/6H/12H/1D/1W/1M/3M/6M/1Y
    */
    OkexCandleList getCandlesHistory(QString instId, qint64 t1, qint64 t2, QString barSize = "1D", int limit = 100);

    /* Запросить незавершённые ордера
       GET /api/v5/trade/orders-pending
       Вернёт true - запрос выполнился успешно, false - ошибка
       В list запишет незавершённые ордера*/
    bool getOrderList(QString instId, OkexOrderList& list);

    /* Выставить пачку ордеров
       POST /api/v5/trade/batch-orders
       insId   = BTC-USD
       list    = список ордеров
       Вернёт:
          true  - если все ордеры установлены успешно
          false - если хотя бы один не установлен или другие ошибки
       В list запишется:
          ordId, sCode = 0, sMsg = ""  - если ордер установлен то ordId непустой и sCode = 0
          sCode > 0, sMsg = ошибка     - если ордер не установлен*/
    bool batchOrders(QString instId, OkexOrderList& list);

    /* Снять пачку ордеров
       POST /api/v5/trade/cancel-batch-orders
       В list нужно только заполненное поле: ordId*/
    bool cancelBatchOrders(QString instId, OkexOrderList& list);

    /* Все пары инструментов
      instType = SPOT,MARGIN,SWAP,FUTURES или OPTION */
    bool getInstruments(QVector<OkexInstrument> &result, QString instType = "SPOT");
    QStringList getInstrumentList(QString instType = "SPOT");

    OkexError errors();
    QString lastResult();

    void addLog(QString);
signals:
    void onSend(QString);
    void onReceive(QString);

private:
    bool networkReplySuccess = false;
    QString networkReplyResult;
    int mRequestTimeout = 5000; // сколько ждать ответа от сайта в мс
    QString testFileName; // если не пустой то запросы идут в него и всё!
    QString mLogFileName;
    QHash<QString, OkexHistoryType> candlesHistType;
    OkexError mErrors;
    QString mLastJsonResult;
    void initCandlesHistType();
    bool runRequest(QString& result, QString method, QString path, QString body = "");
    bool getCandlesJson(QString& result, QString instId, qint64 before, qint64 after, QString barSize = "1D", int limit = 100);
    bool getCandlesHistoryJson(QString& result, QString instId, qint64 before, qint64 after, QString barSize = "1D", int limit = 100);
    bool getBalanceJson(QString& result, QStringList act = QStringList());
    bool getTickersJson(QString& result, QString instType, QString act1, QString act2 = "USDT");
    bool batchOrdersJson(QString& result, QString body);
    bool cancelBatchOrdersJson(QString &result, QString body);
    OkexCandleList candleListFromJson(QString json);
    bool getOrdersListJson(QString &result, QString instId = "");
    bool getInstrumentsJson(QString& result, QString instType);
    bool writeInfoToTestFile(QString method, QString path, QString body);
private slots:
    void networkReplyFinished(QNetworkReply*);
};

#endif // OKEXTRADER_H
