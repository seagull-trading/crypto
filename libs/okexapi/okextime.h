#ifndef OKEXTIME_H
#define OKEXTIME_H
#include <QDebug>
#include <QDateTime>
#include "okexapi_global.h"
#define qDeb qDebug() << Q_FUNC_INFO

class OkexApi_EXPORT OkexTime
{
public:
    OkexTime();
    OkexTime(QDateTime);
    OkexTime(quint64);

    QDateTime dateTime();
    uint toTime_t();
    QString toString();
    QString toStr(QString format = "dd.MM.yyyy hh:mm:ss");
    quint64 timeStamp();
    void fromTimeStamp(quint64);

    void addSecs(int);
    void addHours(int);
    void addDays(int);

private:
    QDateTime mTime;
};


#endif // OKEXTIME_H
