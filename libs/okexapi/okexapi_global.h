#ifndef OKEX_GLOBAL_H
#define OKEX_GLOBAL_H
#include <QtCore/qglobal.h>
#if defined(OkexApi_LIBRARY)
#  define OkexApi_EXPORT Q_DECL_EXPORT
#else
#  define OkexApi_EXPORT Q_DECL_IMPORT
#endif
#endif
