#ifndef OKEXROBOT_H
#define OKEXROBOT_H
#include <QMap>
#include <QObject>
#include <QVariant>
#include <QVector>
#include <QStringList>
#include "okexapi.h"


struct OkexRobotLogMessage
{
    enum MessageType{
        Error = 0,
        Message = 1,
        Detail = 2
    };
    MessageType type;
    QString message;
    OkexRobotLogMessage(){}
    OkexRobotLogMessage(MessageType tp, QString msg) { type = tp; message = msg;}
    QString toString()
    {
        QString str;
        switch (type) {
        case Error:   str = "ERR: "; break;
        case Message: str = "MSG: "; break;
        case Detail:  str = "DET: "; break;
        }
        str.append(message);
        return str;
    };
};

typedef QVector<OkexRobotLogMessage> OkexRobotLogList;

// результат работы робота
struct OkexRobotResult
{
    bool success = false;  // успешно отработал или нет
    QString error;
    OkexOrderList orders;  // список ордеров
};

class OkexApi_EXPORT OkexRobot : public QObject
{
    Q_OBJECT
public:
    explicit OkexRobot(QObject *parent = nullptr);

    // Задать пару и начальное кол-во (баланс)
    void setCryptoPair(OkexCryptoPair cp);
    OkexCryptoPair cryptoPair();
    void setBalance(QPair<double,double>);
    void addToBalance(double,double);
    QPair<double,double> balance();

    // Задать комиссию
    void setFee(double fee_taker, double fee_maker);

    // Задать свечи для анализа
    void setCandles(OkexCandleList*);
    int candlesCount();
    OkexCandle candle(int);
    int findCandle(QDateTime tm);

    // Рассчитать ордера для свечей candle(index)
    virtual OkexRobotResult calcOrders(int index = -1) = 0;

    // Выполнить ордера у которых closed = false для свечи candle(index)
    // Пересчитать баланс(в mPair) с учётом комиссии за срабатывание ордера
    // Вернёт изменение баланса для двух монет на данном шаге
    QPair<double, double> runOrders(int index, OkexOrderList& list);

    // Записать список ордеров с строку для отладки
    QStringList ordersToString(OkexOrderList);

    // Проверить наличие и правильность входных переменных
    virtual bool checkBindVals() = 0;

    // Работа с входными переменными
    void clearBindValues();
    void bindValue(QString name, QVariant val);
    QVariant value(QString name, QVariant def = QVariant());
    double valDouble(QString name, double def = 0);
    QString valString(QString name, QString def = "");
    int valInt(QString name, int def = 0);
    QStringList bindValueNames();
    bool bindValueContains(QString);

    // Для записи лога
    void clearLog();
    void addLog(QString);
    void addError(QString);
    void addDetail(QString);
    void addLogBindings(OkexRobotLogMessage::MessageType tp = OkexRobotLogMessage::Message,
                        QString title = ""); // запись в лог параметров бота
    OkexRobotLogList getLog();
    QStringList getLogList();
    QStringList getErrors();
    bool testBindVals(QStringList names);

private:
    OkexCandleList* candles = nullptr;
    OkexRobotLogList logList; // логи
    QMap<QString, QVariant> bindValues;
    double mFeeTaker = 0.01, mFeeMaker = 0.008;
    OkexCryptoPair mPair;

signals:
    void zeroBalance();

};

#endif // OKEXROBOT_H
