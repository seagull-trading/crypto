#include <QDebug>
#include "okindexrobot.h"
#define qDeb qDebug() << Q_FUNC_INFO

OkIndexRobot::OkIndexRobot(QObject* parent)
    : OkexRobot(parent)
{
}

bool OkIndexRobot::calcRsiIndex(int start, int len, double& val)
{
    double sum_green = 0, sum_red = 0;
    int size = candlesCount();
    if(start < 0 || start >= size - len) return false;
    for(int i = start; i < start+len; i++)
    {
        auto c = candle(i);
        double d = c.close - c.open;
        if(d > 0) sum_green+= qAbs(d);
        else if(d < 0) sum_red+= qAbs(d);
    }
    if(!sum_red) return 100;
    double rs = sum_green / sum_red;
    val = double(100) - double(100)/(rs + 1);
    return true;
}

bool OkIndexRobot::checkBindVals()
{
    return true;
}

OkexRobotResult OkIndexRobot::calcOrders(int index)
{
    OkexRobotResult result;
    OkexOrderList orders;
    result.success = true;

    int len = 14;
    if(index < len) return result;
    double rsi = 0;
    if(!calcRsiIndex(index,len,rsi)) return result;

    OkexCandle cand = candle(index);
    auto bal = balance();
    if(rsi > 85)
    {
        double price = cand.close;
        double amount = 0.1 * bal.second / price;
        orders << OkexOrder(BuyOrder, price, amount);
    }else if(rsi < 25)
    {
        double price = cand.close;
        double amount = 0.1 * bal.first;
        orders << OkexOrder(SellOrder, price, amount);
    }
    result.orders = orders;
    return result;
}

