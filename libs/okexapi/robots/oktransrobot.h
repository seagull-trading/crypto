#ifndef OKTRANSROBOT_H
#define OKTRANSROBOT_H

#include "okexrobot.h"


class OkexApi_EXPORT OkTransRobot  : public OkexRobot
{
public:
    OkTransRobot(QObject* parent = nullptr);

    OkexRobotResult calcOrders(int index = -1);
    bool checkBindVals();
    static double multiplierMin();
private:
    OkexCryptoPair makeStep(OkexCryptoPair pair, double mult, bool step_up, double& price, double& amount);
};

#endif // OKTRANSROBOT_H
