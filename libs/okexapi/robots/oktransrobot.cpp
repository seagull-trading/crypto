#include <QtMath>
#include "oktransrobot.h"

OkTransRobot::OkTransRobot(QObject* parent) : OkexRobot(parent)
{
}

bool OkTransRobot::checkBindVals()
{
    if(!testBindVals({"count","multiplier"}))
        return false;
    int count = valInt("count");
    if(count < 1 || count > 1000)
    {
        addError(tr("Параметр count должен быть от 1 до 1000"));
        return false;
    }
    double mult = valDouble("multiplier");
    if(mult < multiplierMin())
    {
        addError(tr("Параметр multiplier должен быть не менее %1")
                 .arg(QString::number(multiplierMin(),'f')));
        return false;
    }
    double shoulder = valDouble("shoulder", 1);
    if(shoulder <= 0)
    {
        addError(tr("Параметр shoulder должен быть больше нуля"));
        return false;
    }
    return true;
}

double OkTransRobot::multiplierMin()
{
    return 1.0001;
}

// BTC: 0.00375458 USDT: 135.82
OkexRobotResult OkTransRobot::calcOrders(int)
{
    clearLog();
    OkexRobotResult result;
    if(!checkBindVals()) return result;
    int count = valInt("count", 10);
    double multiplier = valDouble("multiplier", 1.07);
    OkexCryptoPair pair_new, pair = cryptoPair();
    if(pair.baseCrypto.amount <= 0 || pair.mainCrypto.amount <= 0)
    {
        if(pair.baseCrypto.amount <= 0)
            addError(tr("Баланс %1 исчерпан (%2)").arg(pair.baseCrypto.code).arg(pair.baseCrypto.amount));
        if(pair.mainCrypto.amount <= 0)
            addError(tr("Баланс %1 исчерпан (%2)").arg(pair.mainCrypto.code).arg(pair.mainCrypto.amount));
        return result;
    }
    double shoulder = valDouble("shoulder", 1);
    pair.mainCrypto.amount*= shoulder;
    pair.baseCrypto.amount*= shoulder;
    for(int i = 0; i < count; i++)
    {
        double price = 0, amount = 0;
        auto p = makeStep(i ? pair_new : pair, multiplier, false, price, amount);
        result.orders << OkexOrder(BuyOrder, price, amount);
        pair_new = p;
    }
    for(int i = 0; i < count; i++)
    {
        double price = 0, amount = 0;
        auto p = makeStep(i ? pair_new : pair, multiplier, true, price, amount);
        result.orders << OkexOrder(SellOrder, price, amount);
        pair_new = p;
    }
    result.success = true;
    return result;
}

OkexCryptoPair OkTransRobot::makeStep(OkexCryptoPair pair, double mult, bool step_up,
                                      double& order_price, double& order_amount)
{
    double price = pair.baseCrypto.amount / pair.mainCrypto.amount;
    double new_price = double(step_up ? price*mult : price / mult);
    double new_balance = pair.mainCrypto.amount * new_price + pair.baseCrypto.amount;
    double base_crypto_delta = new_balance / 2.0 - pair.baseCrypto.amount;
    double main_crypto_delta = -base_crypto_delta / new_price;
    OkexCryptoPair result = pair;
    result.mainCrypto.amount += main_crypto_delta;
    result.baseCrypto.amount += base_crypto_delta;
    order_price = new_price;
    order_amount = qAbs(main_crypto_delta);
    return result;
}



