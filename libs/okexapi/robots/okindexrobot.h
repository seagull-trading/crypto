#ifndef OKINDEXROBOT_H
#define OKINDEXROBOT_H

#include "okexrobot.h"

class OkexApi_EXPORT OkIndexRobot : public OkexRobot
{
public:
    OkIndexRobot(QObject* parent = nullptr);
    OkexRobotResult calcOrders(int index = -1);
    bool checkBindVals();
private:
    bool calcRsiIndex(int pos, int len, double& val);
};

#endif // OKINDEXROBOT_H
