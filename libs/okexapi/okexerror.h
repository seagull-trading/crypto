#ifndef OKEXERROR_H
#define OKEXERROR_H

#include <QString>
#include <QDateTime>
#include <QVector>
#include "okexapi_global.h"

struct OkexErrorRecord
{
    QString text;
    QDateTime time;
};

class OkexApi_EXPORT OkexError
{
public:
    OkexError();
    OkexErrorRecord errorAt(int);
    QString errorTextAt(int, bool datetime = true);

    OkexErrorRecord lastError();
    QString lastErrorText(bool datetime = true);

    void clear();
    void addError(QString);
    int count();

private:
    int queueCount = 100;
    QVector<OkexErrorRecord> mErrorList;

};

#endif // OKEXERROR_H
