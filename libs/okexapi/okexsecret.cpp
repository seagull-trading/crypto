#include <QDir>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include "okexsecret.h"

OkexSecret::OkexSecret()
{
    makeTable();
    load();
}

QString OkexSecret::accessKey()
{
    return mAccessKey;
}

void OkexSecret::setAccessKey(QString key)
{
    mAccessKey = key;
}

QString OkexSecret::secretKey()
{
    return mSecretKey;
}

void OkexSecret::setSecretKey(QString key)
{
    mSecretKey = key;
}

QString OkexSecret::password()
{
    return mPassword;
}

void OkexSecret::setPassword(QString key)
{
    mPassword = key;
}

void OkexSecret::load()
{
    QString fileName = QDir::currentPath() + "/okexkey.txt";
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << Q_FUNC_INFO << "Can't open file" << fileName;
        return;
    }
    QStringList tmp;
    while (!file.atEnd())
        tmp << file.readLine();
    file.close();

    if(tmp.count() < 3)
    {
        qDebug() << Q_FUNC_INFO << "Error at file" << fileName;
        return;
    }
    mAccessKey = decode(tmp[0].trimmed());
    mSecretKey = decode(tmp[1].trimmed());
    mPassword  = decode(tmp[2].trimmed());
}

void OkexSecret::save()
{
    QString fileName = QDir::currentPath() + "/okexkey.txt";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << Q_FUNC_INFO << "Can't open file" << fileName;
        return;
    }
    QTextStream out(&file);
    out << encode(mAccessKey) << "\n";
    out << encode(mSecretKey) << "\n";
    out << encode(mPassword) << "\n";
    file.close();
}

void OkexSecret::makeTable()
{
    table.clear();
    table["0"] = "2";    table["1"] = "0";    table["2"] = "3";    table["3"] = "9";
    table["4"] = "1";    table["5"] = "8";    table["6"] = "4";    table["7"] = "7";
    table["8"] = "6";    table["9"] = "5";
    table["a"] = "a";    table["b"] = "z";    table["c"] = "b";    table["d"] = "y";
    table["e"] = "c";    table["f"] = "x";    table["g"] = "d";    table["h"] = "e";
    table["i"] = "w";    table["j"] = "f";    table["k"] = "u";    table["l"] = "g";
    table["m"] = "v";    table["n"] = "t";    table["o"] = "h";    table["p"] = "i";
    table["q"] = "j";    table["r"] = "s";    table["s"] = "r";    table["t"] = "k";
    table["v"] = "l";    table["u"] = "m";    table["w"] = "n";    table["x"] = "o";
    table["y"] = "p";    table["z"] = "q";
    table["A"] = "G";    table["H"] = "N";    table["O"] = "T";    table["V"] = "Z";
    table["B"] = "F";    table["I"] = "M";    table["P"] = "S";    table["U"] = "Y";
    table["C"] = "E";    table["J"] = "L";    table["Q"] = "R";    table["W"] = "V";
    table["D"] = "D";    table["K"] = "K";    table["R"] = "Q";    table["X"] = "U";
    table["E"] = "C";    table["L"] = "H";    table["S"] = "P";    table["Y"] = "W";
    table["F"] = "B";    table["M"] = "I";    table["T"] = "O";    table["Z"] = "X";
    table["G"] = "A";    table["N"] = "J";
    tableInv.clear();
    for(auto key : table.keys())
        tableInv[ table[key] ] = key;
}

// зашифровать строку
QString OkexSecret::encode(QString str)
{
    QString result;
    for(int i = 0; i < str.length(); i++)
        result += table.contains(str.mid(i,1)) ? table[str.mid(i,1)] : str.mid(i,1);
    return result;
}

// расшифровать строку
QString OkexSecret::decode(QString str)
{
    QString result;
    for(int i = 0; i < str.length(); i++)
        result += tableInv.contains(str.mid(i,1)) ? tableInv[str.mid(i,1)] : str.mid(i,1);
    return result;
}
