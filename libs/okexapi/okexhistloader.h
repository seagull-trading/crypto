#ifndef OKEXHISTLOADER_H
#define OKEXHISTLOADER_H
#include <QHash>
#include <QTimer>
#include <QObject>
#include <QDateTime>
#include "okextime.h"
#include "okextrader.h"
#include "okexapi_global.h"
class OkexApi_EXPORT OkexHistLoader : public QObject
{
    Q_OBJECT
public:
    explicit OkexHistLoader(QObject *parent = nullptr);
    ~OkexHistLoader();

    bool start(QString instId, QString barsize, QDateTime from,
               QDateTime to = QDateTime::currentDateTimeUtc(),
               int interval = 500);
    void setFileName(QString);
    QString fileName();

    bool isStarted();
    void stop();
    QString lastError();

    // прогресс загрузки
    int progressMaximun();
    int progressValue();
    quint64 currentTime();

    static OkexCandleList loadFromCsv(QString fn);
    static bool saveToCsv(QString fn, OkexCandleList list);
    static int barSizeToMinute(QString barsize);

private slots:
    void timerUpdate();

signals:
    void finished();
    void progress();

private:
    QTimer timer;
    QString mFileName;
    OkexTrader* trader = nullptr;
    QString mInstId = "BTC-USDT";
    QString mBarsize = "15m";
    quint64 mStartTime, mStopTime, mCurrentTime, mStepTime;
    quint64 mLastSavedTs = 0;
    QString mLastError;

    bool createFile();
    void addToFile(OkexCandleList);

};

#endif // OKEXHISTLOADER_H
