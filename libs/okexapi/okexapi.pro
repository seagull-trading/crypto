QT       += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$qtLibraryTarget(okexapi)

TEMPLATE = lib
DEFINES+= OkexApi_LIBRARY
INCLUDEPATH += robots
DESTDIR = ../../output
LIBS += -L../../output

CONFIG(debug,debug|release) {
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}

HEADERS += \
    okexapi.h \
    okexapi_global.h \
    okexerror.h \
    okexhistloader.h \
    okexrobot.h \
    okexrobottester.h \
    okexsecret.h \
    okextime.h \
    okextrader.h \
    robots/oktransrobot.h

SOURCES += \
    okexapi.cpp \
    okexerror.cpp \
    okexhistloader.cpp \
    okexrobot.cpp \
    okexrobottester.cpp \
    okexsecret.cpp \
    okextime.cpp \
    okextrader.cpp \
    robots/oktransrobot.cpp
