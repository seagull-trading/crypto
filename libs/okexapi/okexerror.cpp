#include "okexerror.h"

OkexError::OkexError()
{    
}

OkexErrorRecord OkexError::lastError()
{
    return mErrorList.isEmpty() ? OkexErrorRecord() : mErrorList.last();
}

OkexErrorRecord OkexError::errorAt(int ind)
{
    return mErrorList[ind];
}

QString OkexError::errorTextAt(int ind, bool datetime)
{
    auto r = errorAt(ind);
    QString result = r.text;
    if(datetime)
        result.prepend("\n[" + r.time.toString("dd.MM.yyyy hh:mm:ss.zzz") + "]");
    return result;
}

QString OkexError::lastErrorText(bool datetime)
{
    if(mErrorList.isEmpty()) return QString();
    return errorTextAt(mErrorList.count() - 1, datetime);
}

void OkexError::clear()
{
    mErrorList.clear();
}

void OkexError::addError(QString err)
{
    OkexErrorRecord rec;
    rec.text = err;
    rec.time = QDateTime::currentDateTimeUtc();
    mErrorList << rec;
    while(mErrorList.count() > queueCount)
        mErrorList.removeFirst();
}

int OkexError::count()
{
    return mErrorList.count();
}
