#include "okexapi.h"
#include "okextime.h"


OkexCandleList OkexJoinCandles(const OkexCandleList& a, const OkexCandleList& b)
{
    OkexCandleList result = a + b;
    std::sort(result.begin(), result.end(),
              [] ( const OkexCandle& v1, const OkexCandle& v2 ) {
        return v1.ts < v2.ts;
    });
    result.erase(std::unique(result.begin(), result.end(),
                             [](const OkexCandle& v1, const  OkexCandle& v2){
        return v1.ts == v2.ts; }), result.end());
    return result;
}

QString OkexCandle::helpCsv()
{
    return QString("#time;timestamp;open;high;low;close;vol;volCcy");
}

QString OkexCandle::toCsvString()
{
    QStringList tmp;
    tmp << OkexTime(ts).toString();
    tmp << QString::number(ts);
    tmp << QString::number(open,'f',4);
    tmp << QString::number(high,'f',4);
    tmp << QString::number(low,'f',4);
    tmp << QString::number(close,'f',4);
    tmp << QString::number(vol,'f',4);
    tmp << QString::number(volCcy,'f',4);
    return tmp.join(";");
}

bool OkexCandle::fromCsvString(QString str)
{
    QStringList tmp = str.split(";");
    if(tmp.count() != 8) return false;
    ts     = tmp[1].trimmed().toLongLong();
    open   = tmp[2].trimmed().toDouble();
    high   = tmp[3].trimmed().toDouble();
    low    = tmp[4].trimmed().toDouble();
    close  = tmp[5].trimmed().toDouble();
    vol    = tmp[6].trimmed().toDouble();
    volCcy = tmp[7].trimmed().toDouble();
    return true;
}

QString OkexCandle::toString(QString format)
{
    QString str = format;
    str = str.replace("%t", OkexTime(ts).toStr());
    str = str.replace("%l", QString::number(low,'f'));
    str = str.replace("%h", QString::number(high,'f'));
    str = str.replace("%o", QString::number(open,'f'));
    str = str.replace("%c", QString::number(close,'f'));
    return str;
}

// 0-32 символа (буквы-цифры)
// e94ef2326f514f4ca2fa9e5bcc769a07
// 4fb658f9a7e243ea82be7ce4c7e32c11
QString OkexOrder::makeId()
{
    QString result;
    QString uuid = QUuid::createUuid().toString();
    QRegExp rx("([A-Za-z0-9]+)");
    int pos = 0;
    while((pos = rx.indexIn(uuid, pos)) != -1) {
        result+= rx.cap(1);
        pos += rx.matchedLength();
    }
    return result.mid(0,32);
}
