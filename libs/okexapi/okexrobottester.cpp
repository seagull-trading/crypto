#include "okextime.h"
#include "okexrobottester.h"

OkexRobotTester::OkexRobotTester(QObject *parent) : QObject(parent)
{
}

void OkexRobotTester::setRobot(OkexRobot* r)
{
    mRobot = r;
}

OkexRobot *OkexRobotTester::robot()
{
    return mRobot;
}

int OkexRobotTester::currentIndex()
{
    return mCurrentIndex;
}

QDateTime OkexRobotTester::currentDateTime()
{
    return (mCurrentIndex >= 0 &&
            mCurrentIndex < mRobot->candlesCount()) ? OkexTime(mRobot->candle(mCurrentIndex).ts).dateTime()
                                                    : QDateTime();
}

QString OkexRobotTester::lastError()
{
    return mLastError;
}

int OkexRobotTester::calcOpenedOrders(OkexOrderList d)
{
    int n = 0;
    for(int i = 0; i < d.count(); i++)
        if(d[i].closeIndex < 0) n++;
    return n;
}

int OkexRobotTester::calcClosedOrders(OkexOrderList d)
{
    int n = 0;
    for(int i = 0; i < d.count(); i++)
        if(d[i].closeIndex >= 0) n++;
    return n;
}

void OkexRobotTester::setStartIndex(int index)
{
    mStartIndex = index;
    mCurrentIndex = index;
}

bool OkexRobotTester::setStartDateTime(QDateTime tm)
{
    int index = mRobot->findCandle(tm);
    if(index == -1) index = 0;
    setStartIndex(index);
    return true;
}

void OkexRobotTester::setStopIndex(int index)
{
    mStopIndex = index;
}

bool OkexRobotTester::setStopDateTime(QDateTime tm)
{
    int index = mRobot->findCandle(tm);
    if(index == -1) index = mRobot->candlesCount() - 1;
    setStopIndex(index);
    return true;
}

bool OkexRobotTester::next()
{
    mLastError.clear();
    mDelta = qMakePair(0,0);
    if(!mRobot)
    {
        mLastError = tr("Не инициализированн");
        return false;
    }
    if(!mOrders.count() || calcClosedOrders(mOrders) >= 0)
    {
        auto tmp = mRobot->calcOrders(mCurrentIndex);
        if(!tmp.success)
        {
            mLastError = tr("Ошибки робота:") + "\n" + mRobot->getErrors().join("\n");
            return false;
        }
        mOrders = tmp.orders;
    }
    mDelta = mRobot->runOrders(mCurrentIndex, mOrders);
    mCurrentIndex++;
    int max_index = (mStopIndex > 0) ? mStopIndex+1 : mRobot->candlesCount();
    return (mCurrentIndex < max_index);
}

void OkexRobotTester::run(int index_from, int index_to)
{
    setStartIndex(index_from);
    int n = qMin(index_to, mRobot->candlesCount());
    while(currentIndex() < n)
        if(!next()) break;
}

void OkexRobotTester::runAll()
{
    run(0, mRobot->candlesCount());
}

OkexOrderList OkexRobotTester::currentOrders()
{
    return mOrders;
}

QPair<double, double> OkexRobotTester::lastProfit()
{
    return mDelta;
}


