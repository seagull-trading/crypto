#include <QFile>
#include <QTextStream>
#include "okextrader.h"
#include "okexhistloader.h"

OkexHistLoader::OkexHistLoader(QObject *parent) : QObject(parent)
{
    trader = new OkexTrader();
    connect(&timer, &QTimer::timeout, this, &OkexHistLoader::timerUpdate);
    mStartTime = OkexTime(QDateTime::currentDateTimeUtc().addDays(-365)).timeStamp();
    mStopTime  = OkexTime(QDateTime::currentDateTimeUtc()).timeStamp();
    mCurrentTime = OkexTime(QDateTime::currentDateTimeUtc().addDays(-365)).timeStamp();

}

OkexHistLoader::~OkexHistLoader()
{
    delete trader;
}

int OkexHistLoader::barSizeToMinute(QString barsize)
{
    QString s = barsize.trimmed();
    if(s == "1m" ) return 1;
    if(s == "3m" ) return 3;
    if(s == "5m" ) return 5;
    if(s == "15m") return 15;
    if(s == "30m") return 30;
    if(s == "1H" ) return 60;
    if(s == "2H" ) return 120;
    if(s == "4H" ) return 240;
    if(s == "6H" ) return 360;
    if(s == "12H") return 720;
    if(s == "1D" ) return 1440;
    if(s == "1W" ) return 1440*7;
    if(s == "1M" ) return 1440*30;
    if(s == "3M" ) return 1440*90;
    if(s == "6M" ) return 1440*180;
    if(s == "1Y" ) return 1440*365;
    return 0;
}

bool OkexHistLoader::createFile()
{
    if(mFileName.isEmpty()) return true;
    QFile file(mFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    QTextStream out(&file);
    out << OkexCandle::helpCsv() << "\n";
    file.close();
    return true;
}

void OkexHistLoader::addToFile(OkexCandleList list)
{
    if(mFileName.isEmpty()) return;
    QFile file(mFileName);
    if (!file.open(QIODevice::Append | QIODevice::Text))
        return;
    QTextStream out(&file);
    for(int i = 0; i < list.count(); i++)
    {
        if(list[i].ts <= mLastSavedTs) continue;
        out << list[i].toCsvString() << "\n";
        mLastSavedTs = list[i].ts;
    }
    file.close();
}

OkexCandleList OkexHistLoader::loadFromCsv(QString fn)
{
    int error_line = -1;
    QFile file(fn);
    if (!file.open(QIODevice::ReadOnly))
        return OkexCandleList();
    OkexCandleList result;
    QTextStream in(&file);
    int ind = 0;
    while(!in.atEnd())
    {
        QString line = in.readLine().trimmed();
        if(line.isEmpty() || line.mid(0,1) == "#") continue;
        OkexCandle cand;
        if(!cand.fromCsvString(line))
        {
            error_line = ind;
            continue;
        }
        result << cand;
        ind++;
    }
    if(error_line != -1)
        qDeb << "Error in file" << fn << "at line" << error_line;
    return result;
}

bool OkexHistLoader::saveToCsv(QString fn, OkexCandleList list)
{
    QFile file(fn);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    QTextStream out(&file);
    out << OkexCandle::helpCsv() << "\n";
    for(auto cand : list)
        out << cand.toCsvString() << "\n";
    file.close();
    return true;
}

bool OkexHistLoader::start(QString instId, QString barsize, QDateTime from, QDateTime to, int interval)
{
    mLastError.clear();
    mLastSavedTs = 0;
    int mbs = barSizeToMinute(mBarsize);
    if(mbs < 1)
    {
        mLastError = tr("Неверный barsize = %1").arg(barsize);
        return false;
    }
    if(!mFileName.isEmpty())
    {
        if(!createFile())
        {
            mLastError = tr("Ошибка при создании файла %1").arg(mFileName);
            return false;
        }
    }

    // максимально 100 записей но берём c перекрытием,
    // чтобы случайно не потерять на стыках
    quint64 maxiter = 90;
    mInstId      = instId;
    mBarsize     = barsize;
    mStartTime   = OkexTime(qMin(from,to)).timeStamp();
    mStopTime    = OkexTime(qMax(from,to)).timeStamp();
    mCurrentTime = mStartTime;
    mStepTime    = quint64(mbs*60000*maxiter);
    if(mStepTime < 1)
    {
        mLastError = tr("Ошибка при создании файла %1").arg(mFileName);
        return false;

    }
    timer.setInterval(interval);
    timer.start();
    return true;
}

void OkexHistLoader::setFileName(QString fn)
{
    mFileName = fn;
}

QString OkexHistLoader::fileName()
{
    return mFileName;
}

bool OkexHistLoader::isStarted()
{
    return timer.isActive();
}

void OkexHistLoader::stop()
{
    timer.stop();
}

QString OkexHistLoader::lastError()
{
    return mLastError;
}

int OkexHistLoader::progressMaximun()
{
    return quint64(mStopTime - mStartTime)/mStepTime;
}

int OkexHistLoader::progressValue()
{
    return quint64(mCurrentTime - mStartTime)/mStepTime;
}

quint64 OkexHistLoader::currentTime()
{
    return mCurrentTime;
}

void OkexHistLoader::timerUpdate()
{
    timer.stop();
    OkexCandleList list = trader->getCandlesHistory(mInstId,
                                                    mCurrentTime,
                                                    mCurrentTime + mStepTime,
                                                    mBarsize);
    if(list.isEmpty())
    {
        mLastError = trader->errors().lastErrorText();
        emit finished();
        return;
    }
    // сортировать по времени
    std::sort(list.begin(),
              list.end(),
              [](const OkexCandle& a, const OkexCandle& b)
    {
        return a.ts < b.ts;
    });

    // list.first().ts = минимальное время
    // list.last().ts  = максимальное время
    mCurrentTime = list.last().ts;
    //    qDeb << OkexTime(list.first().ts).toStr()
    //         << OkexTime(list.last().ts).toStr()
    //         << list.count();
    addToFile(list);
    timer.start();
    emit progress();
}

