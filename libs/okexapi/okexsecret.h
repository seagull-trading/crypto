#ifndef OKEXSECRET_H
#define OKEXSECRET_H
#include <QHash>
#include <QString>
#include "okexapi_global.h"

class OkexApi_EXPORT OkexSecret
{
public:
    OkexSecret();

    QString accessKey();
    void setAccessKey(QString);

    QString secretKey();
    void setSecretKey(QString);

    QString password();
    void setPassword(QString);

    // зашифровать строку
    QString encode(QString);

    // расшифровать строку
    QString decode(QString);

    void load();
    void save();
private:
    QString mAccessKey, mSecretKey, mPassword;
    QHash<QString,QString> table, tableInv;
    void makeTable();
};

#endif // OKEXSECRET_H
