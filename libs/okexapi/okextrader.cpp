#include <QFile>
#include <QTimer>
#include <QTextStream>
#include "okexsecret.h"
#include "okextrader.h"

#define qDeb qDebug() << Q_FUNC_INFO

bool OkexTraderRequestProgress = false; // true если предыдущий http запрос ещё выполняется


OkexTrader::OkexTrader(QObject *parent) : QObject(parent)
{
}

void OkexTrader::setTest(QString fileName)
{
    testFileName = fileName;
}

void OkexTrader::setLogFile(QString fn)
{
    mLogFileName = fn;
}

void OkexTrader::clearTest()
{
    testFileName.clear();
}

OkexError OkexTrader::errors()
{
    return mErrors;
}

QString OkexTrader::lastResult()
{
    return mLastJsonResult;
}

void OkexTrader::addLog(QString str)
{
    if(mLogFileName.isEmpty()) return;
    QFile file(mLogFileName);
    if(!file.open(QIODevice::Append | QIODevice::Text))
        return;
    QTextStream out(&file);
    out << QString("\n[" + QDateTime::currentDateTimeUtc()
                   .toString("dd.MM.yyyy hh:mm:ss.zzz") + "] " + str);
    file.close();
}

void OkexTrader::initCandlesHistType()
{
    candlesHistType.clear();
    // 1 если запрос истории через /api/v5/market/history-candles (период 1 год)
    candlesHistType["BTC-USDT"] = BigHistory;
    candlesHistType["ETH-USDT"] = BigHistory;
    candlesHistType["LTC-USDT"] = BigHistory;
    candlesHistType["ETC-USDT"] = BigHistory;
    candlesHistType["XRP-USDT"] = BigHistory;
    candlesHistType["EOS-USDT"] = BigHistory;
    candlesHistType["BCH-USDT"] = BigHistory;
    candlesHistType["BSV-USDT"] = BigHistory;
    candlesHistType["TRX-USDT"] = BigHistory;
    candlesHistType["BTC-USD"]  = BigHistory;
    candlesHistType["ETH-USD"]  = BigHistory;
    candlesHistType["LTC-USD"]  = BigHistory;
    candlesHistType["ETC-USD"]  = BigHistory;
    candlesHistType["XRP-USD"]  = BigHistory;
    candlesHistType["EOS-USD"]  = BigHistory;
    candlesHistType["BCH-USD"]  = BigHistory;
    candlesHistType["BSV-USD"]  = BigHistory;
    candlesHistType["TRX-USD"]  = BigHistory;
    // 2 если запрос истории через /api/v5/market/candles (период 1440 записей от текущего времени)
    QStringList inst_list = getInstrumentList("SPOT");
    for(auto inst_id : inst_list)
        if(!candlesHistType.contains(inst_id))
            candlesHistType[inst_id] = SmallHistory;
}

bool OkexTrader::writeInfoToTestFile(QString method, QString path, QString body)
{
    QFile file(testFileName);
    if (!file.open(QIODevice::Append | QIODevice::Text))
        return false;
    QTextStream out(&file);
    out << QString("%1 %2").arg(method).arg(path) << "\n";
    out << body << "\n";
    file.close();
    return true;
}

//----------------------------------------------------------------------------------------
// Послать запрос на okex, вернёт false если ошибка коннекта или ответ в result
//   method = GET или POST
//   path   = /api/v5/account/balance
//   body   = пустое для GET и json для POST
//----------------------------------------------------------------------------------------
//bool OkexTrader::runRequest(QString& result, QString method, QString path, QString body)
//{
//    addLog(tr("*** Run request %1 %2\n%3 ***").arg(method).arg(path).arg(body));
//    if(testFileName.length())
//    {
//        bool ok = writeInfoToTestFile(method,path,body);
//        emit onSend(body);
//        return ok;
//    }
//    QEventLoop loop;
//    OkexSecret secret;
//    QNetworkReply* reply = nullptr;
//    QNetworkAccessManager* manager = new QNetworkAccessManager();
//    QString timestamp = OkexTime(QDateTime::currentDateTimeUtc()).toString();
//    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
//    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
//    QNetworkRequest request = QNetworkRequest(QUrl("https://www.okex.com" + path));
//    QString str = timestamp + method + path + body; // 2021-06-02T14:10:06.832ZGET/api/v5/account/balance
//    QByteArray signhash = QMessageAuthenticationCode::hash(str.toUtf8(),
//                                                           secret.secretKey().toUtf8(),
//                                                           QCryptographicHash::Sha256);
//    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
//    //request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, QVariant(true));
//    request.setRawHeader("ACCEPT", "application/json");
//    // OK-ACCESS-KEY         API ключ
//    // OK-ACCESS-SIGN        Хэш (SHA256) для строки (timestamp + method + path + body)
//    // OK-ACCESS-TIMESTAMP   Время в формате: 2020-12-08T09:08:57.715Z
//    // OK-ACCESS-PASSPHRASE  Пароль для ключа
//    request.setRawHeader("OK-ACCESS-KEY",        secret.accessKey().toUtf8());
//    request.setRawHeader("OK-ACCESS-SIGN",       signhash.toBase64());
//    request.setRawHeader("OK-ACCESS-TIMESTAMP",  timestamp.toUtf8());
//    request.setRawHeader("OK-ACCESS-PASSPHRASE", secret.password().toUtf8());

//    emit onSend(method + " " + path + "\n" + body);

//    addLog(tr("Send request..."));
//    if(method == "GET")
//        reply = manager->get(request);
//    else
//        reply = manager->post(request, body.toUtf8());
//    bool ok = false;
//    mLastJsonResult.clear();
//    if(reply)
//    {
//        addLog(tr("Send request: wait reply"));
//        QObject::connect(reply, &QNetworkReply::finished, &loop, [&reply, &result, &loop]()
//        {
//            if(reply->error() == QNetworkReply::NoError)
//                result = reply->readAll();
//            loop.quit();
//        });
//        loop.exec();
//        addLog(tr("Send request: loop finished"));
//        ok = (reply->error() == QNetworkReply::NoError) ? true : false;
//        addLog(tr("Send request: result %1").arg(ok ? "true" : "false"));
//        if(ok)
//        {
//            addLog(tr("Send request: parse json"));
//            QJsonDocument doc = QJsonDocument::fromJson(result.toUtf8());
//            QJsonObject obj = doc.object();
//            if(obj.contains("code") && obj["code"].toString().toInt())
//            {
//                ok = false;
//                result = tr("Error %1: %2").arg(obj["code"].toString())
//                        .arg(obj["msg"].toString());
//                addLog(tr("Send request: parse json return error\n%1").arg(result));
//            }
//        }else{
//            result = reply->errorString() + " (code=" + QString::number(reply->error()) + ")";
//            addLog(tr("Send request error: %1").arg(result));
//        }
//        //reply->deleteLater();
//        mLastJsonResult = result;
//        addLog(tr("Emit onReceive"));
//        emit onReceive(result);

//    }else{
//        result = tr("Error %1: %2").arg(-1)
//                .arg(tr("GET/POST return 0"));
//    }
//    delete manager;
//    addLog("Run request exit");
//    return ok;
//}
bool OkexTrader::runRequest(QString& result, QString method, QString path, QString body)
{
    addLog(tr("%1 %2").arg(method).arg(path));
    if(OkexTraderRequestProgress)
    {
        addLog(tr("ERROR: Предыдущий запрос ещё выполняется!"));
        return false;
    }
    if(body.length()) addLog("\n" + body);
    if(testFileName.length())
    {
        bool ok = writeInfoToTestFile(method,path,body);
        emit onSend(body);
        return ok;
    }
    QEventLoop loop;
    OkexSecret secret;
    QNetworkAccessManager* manager = new QNetworkAccessManager();
    QString timestamp = OkexTime(QDateTime::currentDateTimeUtc()).toString();
    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    QNetworkRequest request = QNetworkRequest(QUrl("https://www.okex.com" + path));
    QString str = timestamp + method + path + body; // 2021-06-02T14:10:06.832ZGET/api/v5/account/balance
    QByteArray signhash = QMessageAuthenticationCode::hash(str.toUtf8(),
                                                           secret.secretKey().toUtf8(),
                                                           QCryptographicHash::Sha256);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    //request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, QVariant(true));
    request.setRawHeader("ACCEPT", "application/json");
    // OK-ACCESS-KEY         API ключ
    // OK-ACCESS-SIGN        Хэш (SHA256) для строки (timestamp + method + path + body)
    // OK-ACCESS-TIMESTAMP   Время в формате: 2020-12-08T09:08:57.715Z
    // OK-ACCESS-PASSPHRASE  Пароль для ключа
    request.setRawHeader("OK-ACCESS-KEY",        secret.accessKey().toUtf8());
    request.setRawHeader("OK-ACCESS-SIGN",       signhash.toBase64());
    request.setRawHeader("OK-ACCESS-TIMESTAMP",  timestamp.toUtf8());
    request.setRawHeader("OK-ACCESS-PASSPHRASE", secret.password().toUtf8());

    emit onSend(method + " " + path + "\n" + body);

    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(networkReplyFinished(QNetworkReply*)));
    connect(manager, SIGNAL(finished(QNetworkReply*)),&loop, SLOT(quit()));
    QTimer timer;
    OkexTraderRequestProgress = true;
    timer.setSingleShot(true); // один раз
    timer.start(mRequestTimeout);         // ждём ответа не более 3х секунд
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    if(method == "GET")
        manager->get(request);
    else
        manager->post(request, body.toUtf8());
    loop.exec(QEventLoop::ExcludeUserInputEvents);
    bool success = timer.isActive();
    if(success) timer.stop();
    result = networkReplyResult;
    mLastJsonResult = networkReplyResult;
    delete manager;
    if(!success) addLog(tr("ERROR: таймаут запроса"));
    emit onReceive(result);
    OkexTraderRequestProgress = false;
    return networkReplySuccess;
}

void OkexTrader::networkReplyFinished(QNetworkReply* reply)
{
    mLastJsonResult.clear();
    networkReplyResult.clear();
    networkReplySuccess = (reply->error() == QNetworkReply::NoError) ? true : false;
    if(!networkReplySuccess)
    {
        networkReplyResult = reply->errorString() + " (code=" + QString::number(reply->error()) + ")";
        addLog(networkReplyResult);
        return;
    }
    networkReplyResult = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(networkReplyResult.toUtf8());
    QJsonObject obj = doc.object();
    if(obj.contains("code") && obj["code"].toString().toInt())
    {
        networkReplySuccess = false;
        networkReplyResult = tr("Error %1: %2").arg(obj["code"].toString())
                .arg(obj["msg"].toString());
        addLog(tr("ERROR: /Json/ %1").arg(networkReplyResult));
    }
}

// act = "BTC","ETH"
bool OkexTrader::getBalanceJson(QString& result, QStringList act)
{
    QString path = "/api/v5/account/balance";
    if(!act.isEmpty())
    {
        QUrlQuery q;
        q.addQueryItem("ccy", act.join(","));
        path += "?" + q.toString();
    }
    return runRequest(result, "GET", path);
}

//{"code":"0","data":[{"alias":"","baseCcy":"BTC","category":"1","ctMult":"","ctType":"","ctVal":"","ctValCcy":"","expTime":"","instId":"BTC-USDT","instType":"SPOT","lever":"10","listTime":"1630468997000","lotSz":"0.00000001","minSz":"0.00001","optType":"","quoteCcy":"USDT","settleCcy":"","state":"live","stk":"","tickSz":"0.1","uly":""},{"alias":"","baseCcy":"ETH","category":"1","ctMult":"","ctType":"","ctVal":"","ctValCcy":"","expTime":"","instId":"ETH-USDT","instType":"SPOT","lever":"10","listTime":"1548133413000","lotSz":"0.000001","minSz":"0.001","optType":"","quoteCcy":"USDT","settleCcy":"","state":"live","stk":"","tickSz":"0.01","uly":""},
bool OkexTrader::getInstrumentsJson(QString& result, QString instType)
{
    QString path = "/api/v5/public/instruments";
    QUrlQuery q;
    q.addQueryItem("instType", instType);
    path += "?" + q.toString();
    return runRequest(result, "GET", path);
}

bool OkexTrader::getTickersJson(QString &result, QString instType, QString act1, QString act2)
{
    QUrlQuery q;
    QString path = "/api/v5/market/tickers";
    q.addQueryItem("instType", instType);  // SPOT,SWAP,FUTURES,OPTION
    q.addQueryItem("uly", act1 + "-" + act2);
    path += "?" + q.toString();
    return runRequest(result, "GET", path);
}

// https://www.okex.com/api/v5/market/candles?instId=ETH-BTC
// t1 такое, что не более 1440 значений от текущего времени
bool OkexTrader::getCandlesJson(QString &result, QString instId, qint64 t1, qint64 t2,
                                QString barsize, int limit)
{
    QUrlQuery q;
    QString path = "/api/v5/market/candles";
    q.addQueryItem("instId", instId);
    if(t1 && t2)
    {
        q.addQueryItem("before", QString::number(qMin(t1,t2)));
        q.addQueryItem("after", QString::number(qMax(t1,t2)));
    }
    if(!barsize.isEmpty()) q.addQueryItem("bar", barsize);
    if(limit > 0 && limit <= 100)
        q.addQueryItem("limit", QString::number(limit));
    path += "?" + q.toString();
    return runRequest(result, "GET", path);
}

bool OkexTrader::getCandlesHistoryJson(QString &result, QString instId, qint64 t1, qint64 t2,
                                       QString barsize, int limit)
{
    QUrlQuery q;
    QString path = "/api/v5/market/history-candles";
    q.addQueryItem("instId", instId);
    if(t1 && t2)
    {
        q.addQueryItem("before", QString::number(qMin(t1,t2)));
        q.addQueryItem("after", QString::number(qMax(t1,t2)));
    }
    if(!barsize.isEmpty()) q.addQueryItem("bar", barsize);
    if(limit > 0 && limit <= 100)
        q.addQueryItem("limit", QString::number(limit));
    path += "?" + q.toString();
    return runRequest(result, "GET", path);
}

bool OkexTrader::batchOrdersJson(QString &result, QString body)
{
    QString path = "/api/v5/trade/batch-orders";
    return runRequest(result, "POST", path, body);
}

bool OkexTrader::cancelBatchOrdersJson(QString &result, QString body)
{
    QString path = "/api/v5/trade/cancel-batch-orders";
    return runRequest(result, "POST", path, body);
}

bool OkexTrader::getOrdersListJson(QString &result, QString instId)
{
    QString path = "/api/v5/trade/orders-pending";
    if(!instId.isEmpty())
    {
        QUrlQuery q;
        q.addQueryItem("instId", instId);
        path += "?" + q.toString();
    }
    return runRequest(result, "GET", path);
}

OkexCandleList OkexTrader::candleListFromJson(QString json)
{
    OkexCandleList result;
    QJsonDocument doc = QJsonDocument::fromJson(json.toLocal8Bit());
    QJsonObject obj = doc.object();
    QJsonArray  data = obj["data"].toArray();
    for(int i = 0; i < data.count(); i++)
    {
        QJsonArray rec = data[i].toArray();
        OkexCandle cand;
        cand.ts     = rec.at(0).toString().toLongLong();
        cand.open   = rec.at(1).toString().toDouble();
        cand.high   = rec.at(2).toString().toDouble();
        cand.low    = rec.at(3).toString().toDouble();
        cand.close  = rec.at(4).toString().toDouble();
        cand.vol    = rec.at(5).toString().toDouble();
        cand.volCcy = rec.at(6).toString().toDouble();
        result << cand;
    }
    std::sort(result.begin(), result.end(), [](const OkexCandle& a, const OkexCandle& b){ return a.ts < b.ts; });
    return result;
}


// act = "BTC", "ETH" ...
QHash<QString,OkexBalance> OkexTrader::getBalance(QStringList act)
{
    QString json;
    QHash<QString,OkexBalance> result;
    if(!getBalanceJson(json, act))
    {
        mErrors.addError(json);
        return result;
    }
    QJsonDocument doc = QJsonDocument::fromJson(json.toLocal8Bit());
    QJsonObject obj = doc.object();
    QJsonArray data = obj["data"].toArray();
    for(int i = 0; i < data.count(); i++)
    {
        QJsonObject tmp = data.at(i).toObject();
        QJsonArray details = tmp["details"].toArray();
        for(int j = 0; j < details.count(); j++)
        {
            OkexBalance rec;
            QJsonObject obj = details.at(j).toObject();
            rec.isValid       = true;
            rec.ccy           = obj["ccy"].toString();         // USDT
            rec.uTime         = obj["uTime"].toString().toLongLong();
            rec.availBal      = obj["availBal"].toString().toDouble();
            rec.availEq       = obj["availEq"].toString().toDouble();
            rec.cashBal       = obj["cashBal"].toString().toDouble();
            rec.crossLiab     = obj["crossLiab"].toString().toDouble();
            rec.disEq         = obj["disEq"].toString().toDouble();
            rec.eq            = obj["eq"].toString().toDouble();
            rec.eqUsd         = obj["eqUsd"].toString().toDouble();
            rec.frozenBal     = obj["frozenBal"].toString().toDouble();
            rec.interest      = obj["interest"].toString().toDouble();
            rec.isoEq         = obj["isoEq"].toString().toDouble();
            rec.isoLiab       = obj["isoLiab"].toString().toDouble();
            rec.liab          = obj["liab"].toString().toDouble();
            rec.maxLoan       = obj["maxLoan"].toString().toDouble();
            rec.mgnRatio      = obj["mgnRatio"].toString().toDouble();
            rec.notionalLever = obj["notionalLever"].toString().toDouble();
            rec.ordFrozen     = obj["ordFrozen"].toString().toDouble();
            rec.twap          = obj["twap"].toString().toDouble();
            rec.upl           = obj["upl"].toString().toDouble();
            rec.uplLiab       = obj["uplLiab"].toString().toDouble();
            result[rec.ccy] = rec;
        }
    }
    return result;
}

OkexBalance OkexTrader::getBalance(QString act)
{
    QStringList tmp;
    tmp << act;
    auto list = getBalance(tmp);
    return list.contains(act) ? list[act] : OkexBalance();
}


OkexCandleList OkexTrader::getCandles(QString instId, qint64 t1, qint64 t2, QString barSize, int limit)
{
    QString json;
    if(!getCandlesJson(json, instId, t1, t2, barSize, limit))
    {
        mErrors.addError(json);
        return OkexCandleList();
    }
    return candleListFromJson(json);

}

OkexCandleList OkexTrader::getCandlesHistory(QString instId, qint64 t1, qint64 t2, QString barSize, int limit)
{
    QString json;
    OkexCandleList result;
    if(!getCandlesHistoryJson(json, instId, t1, t2, barSize, limit))
    {
        mErrors.addError(json);
        return OkexCandleList();
    }
    return candleListFromJson(json);
}

OkexHistoryType OkexTrader::checkCandlesHistory(QString instId)
{
    if(candlesHistType.isEmpty()) initCandlesHistType();
    return candlesHistType.contains(instId) ? candlesHistType[instId] : NoHistory;
}

bool OkexTrader::batchOrders(QString instId, OkexOrderList &list)
{
    mLastJsonResult.clear();
    QJsonDocument req;
    QJsonArray req_arr;
    QStringList errors;
    for(int i = 0; i < list.count(); i++)
    {
        QJsonObject obj;
        auto ord = list.at(i);
        if(list.at(i).clOrdId.isEmpty())
            list[i].clOrdId = OkexOrder::makeId();
        double price = list.at(i).price;
        double amount = list.at(i).amount;
        if(price <= 0 || amount <= 0)
        {
            errors << tr("Недопустимые параметры ордера %1 (price/amount)").arg(i);
            continue;
        }
        obj["instId"]  = instId;
        obj["sz"]      = QString::number(amount,'f',6);
        obj["px"]      = QString::number(price,'f',6);
        obj["side"]    = QString(list.at(i).type == BuyOrder ? "buy" : "sell");
        obj["tdMode"]  = list.at(i).tdMode;
        obj["clOrdId"] = list.at(i).clOrdId;
        obj["ordType"] = list.at(i).ordType;
        req_arr.append(obj);
    }
    if(req_arr.count() < 1)
    {
        mErrors.addError(tr("Нет ни одного правильного ордера.\n") + errors.join("\n"));
        return false;
    }
    req.setArray(req_arr);
    QString json;
    if(!batchOrdersJson(json, req.toJson()))
        return false;
    // парсинг результата
    QJsonDocument resp = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject obj = resp.object();
    QJsonArray data = obj["data"].toArray();
    int success_count = 0;
    for(int i = 0; i < data.count(); i++)
    {
        QJsonObject rec = data.at(i).toObject();
        QString clOrdId = rec["clOrdId"].toString();
        int ind = -1;
        for(int j = 0; j < list.count(); j++)
        {
            if(list.at(j).clOrdId == clOrdId)
            {
                ind = j;
                break;
            }
        }
        if(ind == -1)
        {
            errors << tr("В ответе не найден ордер с clOrdId = %1").arg(clOrdId);
            continue;
        }
        list[i].ordId = rec["ordId"].toString();
        list[i].sCode = rec["sCode"].toString().toInt();
        list[i].sMsg  = rec["sMsg"].toString();
        if(list[i].sCode == 0 && list[i].sMsg.isEmpty())
            success_count++;
    }
    if(!errors.isEmpty())
        mErrors.addError(errors.join("\n"));

    return (errors.isEmpty() && success_count > 0 && success_count == list.count());
}

bool OkexTrader::cancelBatchOrders(QString instId, OkexOrderList &list)
{
    QJsonDocument req;
    QJsonArray req_arr;
    QStringList errors;
    mLastJsonResult.clear();
    for(int i = 0; i < list.count(); i++)
    {
        QJsonObject obj;
        auto ord = list.at(i);
        if(list.at(i).ordId.isEmpty()) continue;
        obj["instId"] = instId;
        obj["ordId"]  = ord.ordId;
        req_arr.append(obj);
    }
    if(req_arr.count() < 1)
    {
        mErrors.addError(tr("Нет ни одного правильного ордера.\n") + errors.join("\n"));
        return false;
    }
    req.setArray(req_arr);
    QString json;
    if(!cancelBatchOrdersJson(json, req.toJson()))
        return false;
    // парсинг результата
    QJsonDocument resp = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject obj = resp.object();
    QJsonArray data = obj["data"].toArray();
    int success_count = 0;
    for(int i = 0; i < data.count(); i++)
    {
        int ind = -1;
        QJsonObject rec = data.at(i).toObject();
        QString ordId = rec["ordId"].toString();
        for(int j = 0; j < list.count(); j++)
        {
            if(list[j].ordId == ordId)
            {
                ind = j;
                break;
            }
        }
        if(ind == -1)
        {
            errors << tr("В ответе не найден ордер с ordId = %1").arg(ordId);
            continue;
        }
        list[ind].sCode = rec["sCode"].toString().toInt();
        list[ind].sMsg  = rec["sMsg"].toString();
        list[ind].canceled = true;
        if(list[ind].sCode == 0 && list[ind].sMsg.isEmpty())
            success_count++;
    }
    if(!errors.isEmpty())
    {
        mErrors.addError(errors.join("\n") + "\n" + json);
    }
    return (errors.isEmpty() && success_count > 0 && success_count == list.count());
}

bool OkexTrader::getInstruments(QVector<OkexInstrument>& result, QString instType)
{
    QString json;
    result.clear();
    if(!getInstrumentsJson(json, instType))
    {
        mErrors.addError(json);
        return false;
    }
    QJsonDocument doc = QJsonDocument::fromJson(json.toLocal8Bit());
    QJsonObject obj = doc.object();
    QJsonArray data = obj["data"].toArray();
    for(int i = 0; i < data.count(); i++)
    {
        QJsonObject tmp = data.at(i).toObject();
        OkexInstrument rec;
        rec.alias    = tmp["alias"].toString();
        rec.baseCcy  = tmp["baseCcy"].toString();
        rec.category = tmp["category"].toString();
        rec.ctMult   = tmp["ctMult"].toString();
        rec.ctType   = tmp["ctType"].toString();
        rec.ctValCcy = tmp["ctValCcy"].toString();
        rec.expTime  = tmp["expTime"].toString();
        rec.instId   = tmp["instId"].toString();
        rec.instType = tmp["instType"].toString();
        rec.lever    = tmp["lever"].toString();
        rec.listTime = tmp["listTime"].toString();
        rec.lotSz    = tmp["lotSz"].toString();
        rec.minSz    = tmp["minSz"].toString();
        rec.optType  = tmp["optType"].toString();
        rec.quoteCcy = tmp["quoteCcy"].toString();
        rec.settleCcy= tmp["settleCcy"].toString();
        rec.state    = tmp["state"].toString();
        rec.tickSz   = tmp["tickSz"].toString();
        rec.uly      = tmp["uly"].toString();
        result << rec;
    }
    return data.count();
}

QStringList OkexTrader::getInstrumentList(QString instType)
{
    QStringList result;
    QVector<OkexInstrument> inst_list;
    if(getInstruments(inst_list, instType))
    {
        for(auto inst : inst_list)
            if(!result.contains(inst.instId))
                result << inst.instId;
    }
    return result;
}

bool OkexTrader::getOrderList(QString instId, OkexOrderList &list)
{
    list.clear();
    mLastJsonResult.clear();
    QString json;
    if(!getOrdersListJson(json, instId))
        return false;
    // парсинг результата
    QJsonDocument resp = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject obj = resp.object();
    QJsonArray data = obj["data"].toArray();
    if(data.isEmpty()) return true;
    for(int i = 0; i < data.count(); i++)
    {
        OkexOrder ord;
        QJsonObject rec = data.at(i).toObject();
        ord.canceled = false;
        ord.ordId   = rec["ordId"].toString();
        ord.clOrdId = rec["clOrdId"].toString();
        ord.price   = rec["px"].toString().toDouble();
        ord.amount  = rec["sz"].toString().toDouble();
        ord.ordType = rec["ordType"].toString();
        ord.tdMode  = rec["tdMode"].toString();
        QString side = rec["side"].toString().toLower().trimmed();
        if(side == "buy") ord.type = BuyOrder;
        else if(side == "sell") ord.type = SellOrder;
        list << ord;
    }
    return true;
}


QStringList OkexTrader::barSizeList()
{
    return QStringList {
        "1m" ,"3m" ,"5m" ,"15m","30m"
        ,"1H","2H","4H","6H","12H"
        ,"1D","1W","1M","3M","6M","1Y"
    };
}

bool OkexTrader::requestProgress()
{
    return OkexTraderRequestProgress;
}

void OkexTrader::setRequestTimeout(int ms)
{
    if(ms < 50) ms = 50;
    mRequestTimeout = ms;
}

int OkexTrader::requestTimeout()
{
    return mRequestTimeout;
}
