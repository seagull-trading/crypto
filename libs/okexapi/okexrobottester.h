#ifndef OKEXROBOTTESTER_H
#define OKEXROBOTTESTER_H

#include <QObject>
#include "okexrobot.h"
#include "okexapi_global.h"

class OkexRobot;
class OkexApi_EXPORT OkexRobotTester : public QObject
{
    Q_OBJECT
public:
    explicit OkexRobotTester(QObject *parent = nullptr);
    void setRobot(OkexRobot*);

    // задать стартовый индекс для расчёта (время начала)
    void setStartIndex(int index);
    bool setStartDateTime(QDateTime);
    void setStopIndex(int index);
    bool setStopDateTime(QDateTime);

    // проверить сработали ли ордера на для свечей на текущем шаге(время)
    // и посчитать прибыль по каждому, и добавить её к балансу в поле amount (для каждого ордера)
    // вернёт false если после расчёта index вышел за границы (или ошибка, тогда isError() вернёт true)
    bool next();

    // тоже что и next() только в диапазоне
    void run(int index_from, int index_to);
    void runAll();

    OkexOrderList currentOrders();
    QPair<double, double> lastProfit();

    OkexRobot* robot();
    int currentIndex();
    QDateTime currentDateTime();
    QString lastError();


    static int calcOpenedOrders(OkexOrderList);
    static int calcClosedOrders(OkexOrderList);
private:
    OkexRobot* mRobot = nullptr;
    int mStartIndex = 0, mCurrentIndex = 0, mStopIndex = -1;
    QPair<double, double> mDelta;
    OkexOrderList mOrders;
    QString mLastError;

};

#endif // OKEXROBOTTESTER_H
