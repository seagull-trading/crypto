#ifndef OKEXAPI_H
#define OKEXAPI_H
#include <QUuid>
#include <QString>
#include <QtGlobal>
#include <QDateTime>
#include "okexapi_global.h"

enum OkexOrderType
{
    BuyOrder = 1,
    SellOrder = 2,
};

enum OkexHistoryType
{
    NoHistory = 0,
    BigHistory = 1,
    SmallHistory = 2
};

// На сколько дней хранится история на серверах okex
// для запроса /api/v5/market/history-candles (BigHistory)
#define OKEX_HIST_BIG_DAYS       365

// На сколько записей хранится история на серверах okex
// для запроса /api/v5/market/candles (SmallHistory)
#define OKEX_HIST_SMALL_RECORDS  1400

struct OkexApi_EXPORT OkexCandle
{
    quint64 ts     = 0;  // Время (UTC), в миллисекундах с 1970 года
    double  open   = 0;  // Открытие
    double  high   = 0;  // Максимум
    double  low    = 0;  // Минимум
    double  close  = 0;  // Закрытие
    double  vol    = 0;  // String Trading volume, with a unit of contact.
    double  volCcy = 0;  // String Trading volume, with a unit of currency.
    OkexCandle(){}
    static QString helpCsv();
    QString toCsvString();
    bool fromCsvString(QString);
    QString toString(QString format = "UTC: %t\nlow: %l\nhigh: %h\nopen: %o\nclose: %c");
};
typedef QVector<OkexCandle> OkexCandleList;
// объединить свечи, отсортировав по возростанию времени и удалив лишние
OkexCandleList OkexApi_EXPORT OkexJoinCandles(const OkexCandleList& a, const OkexCandleList& b);

struct OkexApi_EXPORT OkexCrypto
{
    QString code;        // тип инструмента BTC, USDT, ETH, ...
    double amount = 0;   // кол-во
    OkexCrypto(){}
    OkexCrypto(QString c, double d) { code = c; amount = d; }
};

struct OkexApi_EXPORT OkexOrder
{
    OkexOrderType type;         // покупка или продажа (side)
    double price = 0;           // цена покупки или продажи (px)
    double amount = 0;          // кол-во монет (sz)

    QString clOrdId;            // задаётся клиентом (clOrdId) буквы-цифры длинной до 32 символом
    QString ordId;              // возвращает okex после фактической установки ордера
    QString tdMode = "cash";    // cash/cross/isolated (cash - если без плеча)
    QString ordType = "limit";  // limit - ожидаем срабатывания, market - покупка по рынку
    int sCode = -1;             // -1 - не устанавливался, 0 - успешшно установлен, >0 - ошибка установки (в sMsg)
    QString sMsg;
    bool canceled = false;      // выставляется, если ордер отменён

    int closeIndex = -1;        // индекс свечи на которой сработал ордер (отладка)
    OkexOrder(){}
    OkexOrder(OkexOrderType tp){ type = tp; }
    OkexOrder(OkexOrderType tp, double p, double a){ type = tp; price = p; amount = a; }
    static QString makeId();
    QString typeStr() { return QString(type == BuyOrder ? "buy" : "sell"); }
};
typedef QVector<OkexOrder> OkexOrderList;


struct OkexApi_EXPORT OkexCryptoPair
{
    OkexCrypto mainCrypto; // основная валюта
    OkexCrypto baseCrypto; // валюта в которой измеряется цена (базовая)
    OkexCryptoPair(){}
    OkexCryptoPair(OkexCrypto c1, OkexCrypto c2)
    { mainCrypto = c1; baseCrypto = c2; }
};

struct OkexApi_EXPORT OkexBalance
{
    bool isValid = false; // false - ошибка получения баланса, true - баланс получен
    QString ccy;          // USDT
    quint64 uTime = 0;    // время
    double availBal = 0;  // доступный баланс (без учёта ордеров)
    double availEq  = 0;
    double cashBal  = 0;  // сколько всего
    double crossLiab = 0;
    double disEq = 0;
    double eq = 0;
    double eqUsd = 0;
    double frozenBal = 0;
    double interest = 0;
    double isoEq = 0;
    double isoLiab  = 0;
    double liab = 0;
    double maxLoan = 0;
    double mgnRatio = 0;
    double notionalLever = 0;
    double ordFrozen = 0;
    double twap = 0;
    double upl  = 0;
    double uplLiab = 0;
};

struct OkexApi_EXPORT OkexInstrument
{
    QString instId;    // Instrument ID
    QString instType;  // Instrument type (SPOT/MARGIN/SWAP/FUTURES/OPTION)
    QString minSz;     // Minimum order size
    QString alias;     // this_week,next_week,quarter,next_quarter
    QString baseCcy;   // Base currency. Only applicable to SPOT.
    QString category;  // Fee schedule
    QString ctMult;    // Contract multiplier (Only FUTURES/SWAP/OPTION)
    QString ctType;    // Contract value (Only FUTURES/SWAP/OPTION)
    QString ctValCcy;  // Contract value currency (Only FUTURES/SWAP/OPTION)
    QString expTime;   // Expiry time (Only FUTURES/SWAP/OPTION)
    QString lever;     // Max Leverage
    QString listTime;  // Listing time
    QString lotSz;     // Lot size
    QString optType;
    QString quoteCcy;
    QString settleCcy;
    QString state;       // live
    QString tickSz;
    QString uly;
};

#endif // OKEXAPI_H
