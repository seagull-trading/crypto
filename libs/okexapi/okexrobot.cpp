#include <QDateTime>
#include "okextime.h"
#include "okexrobot.h"

OkexRobot::OkexRobot(QObject *parent) : QObject(parent)
{
}

int OkexRobot::candlesCount()
{
    return candles ? candles->count() : 0;
}

OkexCandle OkexRobot::candle(int i)
{
    return candles ? candles->at(i) : OkexCandle();
}

int OkexRobot::findCandle(QDateTime tm)
{
    for(int i = 0; i < candles->count(); i++)
        if(OkexTime(candles->at(i).ts).dateTime() > tm)
            return i;
    return -1;
}

void OkexRobot::setCryptoPair(OkexCryptoPair cp)
{
    mPair = cp;
}

OkexCryptoPair OkexRobot::cryptoPair()
{
    return mPair;
}

void OkexRobot::setBalance(QPair<double,double> v)
{
    mPair.mainCrypto.amount = v.first;
    mPair.baseCrypto.amount = v.second;
}

void OkexRobot::addToBalance(double first, double second)
{
    mPair.mainCrypto.amount += first;
    mPair.baseCrypto.amount += second;
}

QPair<double, double> OkexRobot::balance()
{
    return qMakePair(mPair.mainCrypto.amount, mPair.baseCrypto.amount);
}

void OkexRobot::setFee(double fee_taker, double fee_maker)
{
    mFeeTaker = fee_taker;
    mFeeMaker = fee_maker;
}

void OkexRobot::setCandles(OkexCandleList* d)
{
    candles = d;
}

void OkexRobot::clearBindValues()
{
    bindValues.clear();
}

void OkexRobot::bindValue(QString name, QVariant val)
{
    bindValues[name] = val;
}

QVariant OkexRobot::value(QString name, QVariant def)
{
    return bindValues.contains(name) ? bindValues[name] : def;
}

double OkexRobot::valDouble(QString name, double def)
{
    return value(name,def).toDouble();
}

QString OkexRobot::valString(QString name, QString def)
{
    return value(name,def).toString();
}

int OkexRobot::valInt(QString name, int def)
{
    return value(name,def).toInt();
}

QStringList OkexRobot::bindValueNames()
{
    return bindValues.keys();
}

bool OkexRobot::bindValueContains(QString name)
{
    return bindValues.keys().contains(name);
}

void OkexRobot::clearLog()
{
    logList.clear();
}

void OkexRobot::addLog(QString msg)
{
    logList << OkexRobotLogMessage(OkexRobotLogMessage::Message, msg);
}

void OkexRobot::addError(QString err)
{
    logList << OkexRobotLogMessage(OkexRobotLogMessage::Error, err);
}

void OkexRobot::addDetail(QString det)
{
    logList << OkexRobotLogMessage(OkexRobotLogMessage::Error, det);
}

OkexRobotLogList OkexRobot::getLog()
{
    return logList;
}

QStringList OkexRobot::getLogList()
{
    QStringList tmp;
    for(auto rec : logList)
        tmp << rec.toString();
    return tmp;
}

QStringList OkexRobot::getErrors()
{
    QStringList tmp;
    for(auto rec : logList)
        if(rec.type == OkexRobotLogMessage::Error)
            tmp << rec.toString();
    return tmp;
}

bool OkexRobot::testBindVals(QStringList names)
{
    bool ok = true;
    for(auto name : names)
        if(!bindValueContains(name))
        {
            addError(tr("Переменная %1 не найдена").arg(name));
            ok = false;
        }
    return ok;
}

void OkexRobot::addLogBindings(OkexRobotLogMessage::MessageType tp, QString title)
{
    if(title.length())
        logList << OkexRobotLogMessage(tp, title);
    for(auto key : bindValues.keys())
    {
        QString val;
        QVariant v = bindValues[key];
        switch (v.type()) {
        case QVariant::String    : val = "\"" + v.toString() + "\"";                     break;
        case QVariant::Bool      : val = v.toBool() ? "true" : "false";                  break;
        case QVariant::Int       : val = QString::number(v.toInt());                     break;
        case QVariant::UInt      : val = QString::number(v.toUInt());                    break;
        case QVariant::LongLong  : val = QString::number(v.toLongLong());                break;
        case QVariant::ULongLong : val = QString::number(v.toULongLong());               break;
        case QVariant::Double    : val = QString::number(v.toDouble());                  break;
        case QVariant::Date      : val = v.toDate().toString("dd.MM.yyyy");              break;
        case QVariant::Time      : val = v.toTime().toString("hh:mm:ss");                break;
        case QVariant::DateTime  : val = v.toDateTime().toString("dd.MM.yyyy hh:mm:ss"); break;
        default:
            val = v.toString();
        }
        logList << OkexRobotLogMessage(tp, key + " = " + val);
    }
}


QStringList OkexRobot::ordersToString(OkexOrderList order_list)
{
    QStringList tmp;
    tmp << "Buy order list:";
    double sum = 0;
    for(auto order : order_list)
    {
        if(order.type != BuyOrder) continue;
        QString str = tr("buy price: %1 amount: %2")
                .arg(QString::number(order.price,'f'))
                .arg(QString::number(order.amount,'f',8));
        int ind = order.closeIndex;
        if(ind >= 0) str+= tr(" - closed %1").arg(OkexTime(candle(ind).ts).toStr());
        tmp << str;
        sum+= order.amount;
    }
    tmp << tr("Summary buy  amount: %1").arg(QString::number(sum,'f',8));
    tmp << "Sell order list:";
    sum = 0;
    for(auto order : order_list)
    {
        if(order.type != SellOrder) continue;
        QString str = tr("sell price: %1 amount: %2")
                .arg(QString::number(order.price,'f'))
                .arg(QString::number(qAbs(order.amount),'f',8));
        int ind = order.closeIndex;
        if(ind >= 0) str+= tr(" - closed %1").arg(OkexTime(candle(ind).ts).toStr());
        tmp << str;
        sum+= qAbs(order.amount);
    }
    tmp << tr("Summary sell amount: %1").arg(QString::number(sum,'f',8));
    return tmp;
}


QPair<double, double> OkexRobot::runOrders(int index, OkexOrderList &list)
{
    QPair<double, double> delta;
    if(index < 0 || index >= candlesCount())
        return qMakePair(0,0);

    auto cand = candle(index);
    for(int i = 0; i < list.count(); i++)
    {
        auto ord = list[i];
        if(ord.closeIndex >= 0) continue;
        if(ord.type == BuyOrder)
        {
            if(ord.price >= cand.low)
            {
                bool is_taker = (ord.price > cand.high);
                double fee = is_taker ? mFeeTaker : mFeeMaker;
                double price = is_taker ? cand.close : ord.price;
                delta.first  += ord.amount;
                delta.second -= ord.amount*price;
                delta.first  -= ord.amount*fee;
                list[i].closeIndex = index;
            }
        }else if(ord.type == SellOrder)
        {
            if(ord.price <= cand.high)
            {
                bool is_taker = (ord.price < cand.low);
                double fee = is_taker ? mFeeTaker : mFeeMaker;
                double price = is_taker ? cand.close : ord.price;
                delta.first  -= ord.amount;
                delta.second += ord.amount*price;
                delta.second -= ord.amount*price*fee;
                //delta.first -= ord.amount*fee; // ??
                list[i].closeIndex = index;
            }
        }
    }
    addToBalance(delta.first, delta.second);
    //qDeb << balance();

    if(balance().first <= 0 || balance().second <= 0)
        emit zeroBalance();
    return delta;
}
