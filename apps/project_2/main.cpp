#include <QDebug>
#include <QTextCodec>
#include <QApplication>
#include "candles.h"
#include "dbutils.h"
#include "utils.h"
#include "items/fplotlineitem.h"
#include "items/fplotpolylineitem.h"
#include "series_gui.h"
#define qDeb qDebug() << Q_FUNC_INFO
#define tr QObject::tr

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    //CreateAllCryptoBinaries();

    // Загрузка свечей
    Candles eth;
    eth.load("eth","usdt");

    // Расчёт параметра (индекс RSI)
    TimeSeries par;
    QString tm1 = "2021-06-01";
    QString tm2 = "2021-11-01";
    quint64 t1 = DateTimeFromString(tm1).toTime_t();
    quint64 t2 = DateTimeFromString(tm2).toTime_t();
    quint64 step = 3600*24;
    for(quint64 t = t1; t < t2; t+= step)
    {
        QString tm = DateTimeToString(QDateTime::fromTime_t(t,Qt::UTC));
        double rsi = eth.indexRsi(tm, 14, step);
        if(rsi < 0) continue;
        par.add(tm, rsi);
    }

    // Расчёт вероятности падения цены на percent процентов при
    // значении параметра (индекс RSI) больше чем 70
    int percent = 1;      // процент изменения цены (3%)
    int period = 3600*24; // сколько секунд ждём падения цены на заданный процент
    int count = 0;        // кол-во раз когда RSI >= 70
    int pcnt = 0;         // кол-во раз когда RSI >= 70 и при этом после было падение цены
    for(int i = 0; i < par.count(); i++)
    {
        bool event = par.value(i) > 70; // если RSI >= 70 то ожидается падение
        if(event)
        {
            int secs = 0;
            QString tm = DateTimeToString(QDateTime::fromTime_t(par.time(i),Qt::UTC));
            double p = eth.value(Candles::Close, tm);  // цена в текущий момент времени
            double p_min = p - double(percent)*p/100;  // цена ниже текущей на заданный процент
            // ind = -1 если цена не упала, >= 0 если цена упала ниже p_min
            int ind = eth.boundDown(Candles::Close, tm, p_min, secs, period);
            pcnt+= (ind == -1) ? 0 : 1;
            count++;
        }
    }
    qDeb << tr("Цена падала при событии: %1 раз из %2 (%3\%)")
            .arg(pcnt).arg(count).arg(pcnt*100/count);

    //CandlesWidget w;
    //w.setWindowTitle("eth-usdt");
    //w.addCandles(&eth);
    //w.show();

    return app.exec();
}
