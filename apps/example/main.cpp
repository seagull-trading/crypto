#include <QDebug>
#include <QScreen>
#include <QTextCodec>
#include <QApplication>
#include "series.h"
#include "candles.h"
#include "timeseries.h"
#include "series_gui.h"
#include "dbutils.h"
#include "utils.h"
#define cout qDebug()

// CreateAllCryptoBinaries() - повторная загрузка в bin файлы

QSqlDatabase dataBase;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    qDeb << "ALL" << Candles::all();
    QStringList pairs;
    pairs << "btc" << "etc" << "ltc" << "eth" << "xrp" << "eos";
    QVector<Candles> candles;
    QVector<CandlesWidget*> windows;
    int x = 5, y = 30;
    auto ssz = app.screenAt(QPoint(0,0))->size();
    ssz.setWidth(ssz.width() - 30);
    ssz.setHeight(ssz.height() - 5);
    for(int i = 0; i < pairs.count(); i++)
    {
        auto pair = pairs[i];
        Candles d;
        d.load(pair,"usdt");
        candles << d;
        CandlesWidget* w = new CandlesWidget();
        w->addCandles(&d);
        w->setWindowTitle(pair);
        w->show();
        w->resize(ssz.width()/3, ssz.height()/2.5);
        w->setPos(x,y);
        if((i+1)%3)
        {
            x+= w->width() + 10;
        }else{
            y+= w->height() + 50;
            x = 5;
        }

    }

    return app.exec();
}
