QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp

HEADERS +=

FORMS +=


INCLUDEPATH += ../../libs/okexapi
INCLUDEPATH += ../../libs/utils
INCLUDEPATH += ../../libs/dbutils
INCLUDEPATH += ../../libs/fplot
INCLUDEPATH += ../../libs/series
INCLUDEPATH += ../../libs/seriesgui

DESTDIR = ../../output
LIBS += -L../../output

CONFIG(debug,debug|release) {
    LIBS += -lutilsd -ldbutilsd -lfplotd -lokexapid -ltelegrambotd -lseriesd -lseriesguid
    COMPILE_PREFIX=debug
}

CONFIG(release,debug|release) {
    LIBS += -lutils -ldbutils -lfplot -lokexapi -ltelegrambot -lseries -lseriesgui
    COMPILE_PREFIX=release
}

MOC_DIR = ../../tmp/build/MOC_$${COMPILE_PREFIX}/$${TARGET}
OBJECTS_DIR = ../../tmp/build/Obj_$${COMPILE_PREFIX}/$${TARGET}
