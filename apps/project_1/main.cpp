#include <QDebug>
#include <QTextCodec>
#include <QApplication>
#include "candles.h"
#include "utils.h"

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    Candles eth;
    eth.load("eth","usdt");

    auto v = eth.candle("2021-05-12 12:00:30");
    qDeb << "ETH open:" << v.open << "USDT" << v.timeStr();

    eth.average(Candles::Close, "2021-05-12 12:00:30", "2021-05-13 12:00:30");


    return 0;
}
