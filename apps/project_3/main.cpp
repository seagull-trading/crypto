#include <QDebug>
#include <QTextCodec>
#include <QApplication>
#include "candles.h"
#include "timeseries.h"
#include "series_gui.h"
#include "utils.h"

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    Candles btc;
    btc.load("btc","usdt");

    TimeSeries m1;
    int len = 60*24;  // среднее за сутки
    for(int i = len; i < btc.count(); i++)
        m1.add(btc.time(i), btc.average(Candles::High, i - len, len));

    CandlesWidget window;
    window.addCandles(&btc);
    window.addSeries(&m1, QPen(Qt::red, 3));
    window.show();

    return app.exec();
}
